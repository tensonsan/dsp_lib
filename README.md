## Digital signal processing and machine learning library for embedded systems

### Features:

* No dynamic allocations.
* Portable.
* C99 compliant.
* Using in-place operations when possible to minimize the RAM usage.
* Using single-precision float (except for linear regression fit) for fast execution on Cortex M4F/M7F.
* Unit tested against existing Python libraries.

### Folder structure:

The [/lib](/lib) folder contains the sources of the library in [/inc](/lib/inc) and [/src](/lib/src) subfolders. Simply copy the relevant sources to your project. The [dsp_lib.c](/lib/dsp_lib.c) and [dsp_lib.h](/lib/dsp_lib.h) sources are used for compiling the library into a shared lib, used for unit tests.

The [/lib/scripts](/lib/scripts) folder contains example Python scripts for training different classifiers as well as scripts for exporting the parameters/weights into C lookup tables. The generated files are put into the [/lib/generated](/lib/generated) folder by default.

The [/tests](/tests) folder contains Python scripts for unit testing the library. The library is compiled into a shared lib, that is accessed from the Python via [ctypes](https://docs.python.org/3/library/ctypes.html). In a unit test, sample input data is provided to the shared lib wrapper and the corresponding Python library (used as a reference) and outputs are compared to validate the C implementation. The output comparison graphs are put to the [/images](/tests/images) subfolder.

### Usage:

Generate Doxygen documentation for the library (Mathjax is used for rendering equations).
```Makefile
make doc
```

Generate lookup tables for unit tests (e.g. filter coefficients, classifier weights, etc).
```Makefile
make lut
```

Compile the library into a shared lib, used in unit tests.
```Makefile
make
```

Run the unit tests. This will generate a folder that contains accuracy comparison graphs for most of the library functions.
```Makefile
make tests
```

Cleanup
```Makefile
make clean
```

### Dependencies:

* Ubuntu 18.04 
* GCC 7.4.0
* Make 4.1
* Doxygen 1.8.13
* Python3 3.6.8 

Python dependencies are listed in [requirements.txt](requirements.txt)

### License:

Copyright 2019 Gasper Korinsek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


