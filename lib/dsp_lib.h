
#ifndef _DSP_LIB_H_
#define _DSP_LIB_H_

#include <stdint.h>

float dsp_lib_mean(float *x, int n);
float dsp_lib_median(float *x, int n);
float dsp_lib_mean_abs(float *x, int n);
float dsp_lib_rms(float *x, int n);
float dsp_lib_relvar(float *x, int n);
float dsp_lib_mad(float *x, int n);
float dsp_lib_entropy(float *x, int n);
void dsp_lib_min(float *x, int n, float *v_min, int *i_min);
void dsp_lib_max(float *x, int n, float *v_max, int *i_max);
void dsp_lib_linreg(int x1, int x2, float *data, int *x, float *y, int n);
void dsp_lib_ma(float *x, float *y, int n, int m);
void dsp_lib_ema(float *x, float *y, int n, float k);

void dsp_lib_rfft(float *x, float *y_re, float *y_im, int n);
void dsp_lib_irfft(float *x, float *y, int n);
int dsp_lib_bin2freq(int bin, int n, int fs);
int dsp_lib_freq2bin(int f, int n, int fs);

void dsp_lib_acorr_direct(float *x, float *r, int n, int m);
void dsp_lib_acorr_fft(float *x, float *r, int n);

void dsp_lib_lpc(float *x, float *a, float *e, int n, int m);
void dsp_lib_lpc_freq(float *x, float *y, int n, int m);
void dsp_lib_lpc_prediction(float *x, float *y, int n, int m);
void dsp_lib_lpcc(float *a, float *c, float e, int m);

void dsp_lib_trifb_lin(int m, int n, float *c);
void dsp_lib_trifb_mel(int m, int n, int fs, float *c);

float dsp_lib_mlp(float *x);

int dsp_lib_decision_tree(float *x);

float dsp_lib_svm_lin(float *x);
float dsp_lib_svm_rbf(float *x);

void dsp_lib_fir_stream(float *x, float *y, int n);
void dsp_lib_fir_buffer(float *x, float *y, int n);
void dsp_lib_fir_stream_fix16(int16_t *x, int16_t *y, int n);
void dsp_lib_fir_buffer_fix16(int16_t *x, int16_t *y, int n);
void dsp_lib_biquad(float *x, float *y, int n);
void dsp_lib_goertzel(float *x, float *y, int f, int fs, int n);

void dsp_lib_cepstrum(float *x, float *y, int n);

void dsp_lib_window_hann(float *x, int n);
void dsp_lib_window_hamming(float *x, int n);
void dsp_lib_window_bartlett(float *x, int n);

void dsp_lib_decimator1(float *x, float *y, int n);
void dsp_lib_decimator2(int16_t *x, int16_t *y, int n);
void dsp_lib_decimator3(float *x, float *y, int n);
void dsp_lib_decimator4(int16_t *x, int16_t *y, int n);

void dsp_lib_dct_direct(float *x, float *y, int n);
void dsp_lib_dct_fft(float *x, float *y, int n);
void dsp_lib_dct_fast(float *x, float *y);

void dsp_lib_lms(float *x, float *y, float *d, float *c, int n, int m, float k);

void dsp_lib_hilbert(float *x, float *y, int n);

void dsp_lib_spline_fit(float *x, float *a, float *b, float *c, float *d, int n);
void dsp_lib_spline_interpolate(float *xt, float *yt, int nt, float *x, float *y, int n);

void dsp_lib_peak_find_all(float *y, int *xmin, int *nmin, int *xmax, int *nmax, int n);
void dsp_lib_peak_find(float *x, int n, int m, int bw, int *px, float *py);

void dsp_lib_dwt_periodic(float *x, float *y, int n);
void dsp_lib_idwt_periodic(float *x, float *y, int n);

float dsp_lib_dtw(float *x, int nx, float *y, int ny);

float dsp_lib_hmm_viterbi(int *o, int T);

void dsp_lib_preemph(float *x, int n, float k);
void dsp_lib_preemph_dynamic(float *x, int n);

void dsp_lib_lfsc(float *x, float *y, int n, int m);
void dsp_lib_lfcc(float *x, float *y, int n, int m);
void dsp_lib_mfsc(float *x, float *y, int n, int m, int fs);
void dsp_lib_mfcc(float *x, float *y, int n, int m, int fs);

void dsp_lib_ssub(float *noise, int n_noise, float *signal, int n_signal, int nfft, float k, float alpha, float beta, int onset, float threshold);

void dsp_lib_edge_detector(float *x, float *y, int n, float threshold, int onset, int n_skip);

void dsp_lib_keras_test(float *x, float *y, int n, int m);

#endif
