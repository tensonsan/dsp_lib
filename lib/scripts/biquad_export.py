#!/usr/bin/python3

'''
Generates C source files with coefficients for the multistage Butterworth biquad filter
'''

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from utils import write_c_array_flt32
import os
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--order', default=16, help='FIR filter order', type=int)
    parser.add_argument('--fs', default=32e3, help='Sampling frequency in Hz', type=int)
    parser.add_argument('--fc', default=500, help='Cutoff frequency in Hz', type=int)
    parser.add_argument('--typ', default='lowpass', choices=['lowpass', 'highpass', 'bandpass', 'bandstop'], help='Filter type')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--plot', action='store_true', help='Display frequency response')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Synthesize the filter
    sos = signal.butter(N=args.order + 1, Wn=2.0 * args.fc / args.fs, btype=args.typ, output='sos')

    # Plot the frequency response if needed
    if args.plot:
        w, h = signal.sosfreqz(sos, worN=1024)
        f = args.fs * w / (2.0 * np.pi)

        plt.semilogx(f, 20.0 * np.log10(np.abs(h)))
        plt.title('Butterworth filter frequency response')
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Amplitude [dB]')
        plt.grid()
        plt.show()

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))

    uprefix = "BIQUAD_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dstages = uprefix + "_STAGES"
    dcoeffs = uprefix + "_COEFFS"

    n_stages = np.shape(sos)[0]
    n_coeffs = np.shape(sos)[1]

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + dstages + " {}\n".format(n_stages))
        f.write("#define " + dcoeffs + " {}\n\n".format(n_coeffs))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        c = np.reshape(sos, np.size(sos))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "[" + dstages + " * " + dcoeffs + "]", c)

if __name__ == "__main__":
    main()