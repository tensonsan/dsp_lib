
import os
import numpy as np
import sys
from utils import write_c_array_flt32

def gru_generate_lut(model, layer_index, output='', suffix='', warning=None):
    # Generates C sources with Gated Recurrent Unit (GRU) layer weights

    units = model.layers[layer_index].get_config()['units']
    inputs = model.layers[layer_index].input_shape[-1]

    # Weights Wz, Wr, Wh
    w = model.layers[layer_index].get_weights()[0]
    kernel_z = w[:, :units]
    kernel_r = w[:, units: units * 2]
    kernel_h = w[:, units * 2:]

    # Weights Uz, Ur, Uh
    w = model.layers[layer_index].get_weights()[1]
    recurrent_kernel_z = w[:, :units]
    recurrent_kernel_r = w[:, units: units * 2]
    recurrent_kernel_h = w[:, units * 2:]

    # Bias bz, br, bh
    w = model.layers[layer_index].get_weights()[2]
    input_bias_z = w[:units]
    input_bias_r = w[units: units * 2]
    input_bias_h = w[units * 2:]

    uprefix = "GRU_LUT_{}_".format(layer_index) + suffix.upper()
    lprefix = uprefix.lower()
    dinputs = uprefix + "_INPUTS"
    dunits = uprefix + "_UNITS"

    # Generate the H file
    with open(os.path.join(output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + dinputs + " {}\n".format(inputs))
        f.write("#define " + dunits + " {}\n\n".format(units))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        # Forward
        wz = kernel_z.T
        wz = np.reshape(wz, np.size(wz))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_wz[" + dinputs + " * " + dunits + "]", wz)

        wr = kernel_r.T
        wr = np.reshape(wr, np.size(wr))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_wr[" + dinputs + " * " + dunits + "]", wr)

        wh = kernel_h.T
        wh = np.reshape(wh, np.size(wh))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_wh[" + dinputs + " * " + dunits + "]", wh)

        # Recurrent
        uz = recurrent_kernel_z.T
        uz = np.reshape(uz, np.size(uz))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_uz[" + dunits + " * " + dunits + "]", uz)

        ur = recurrent_kernel_r.T
        ur = np.reshape(ur, np.size(ur))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_ur[" + dunits + " * " + dunits + "]", ur)

        uh = recurrent_kernel_h.T
        uh = np.reshape(uh, np.size(uh))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_uh[" + dunits + " * " + dunits + "]", uh)

        # Bias
        bz = input_bias_z.T
        bz = np.reshape(bz, np.size(bz))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_bz[" + dunits + "]", bz)

        br = input_bias_r.T
        br = np.reshape(br, np.size(br))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_br[" + dunits + "]", br)

        bh = input_bias_h.T
        bh = np.reshape(bh, np.size(bh))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_bh[" + dunits + "]", bh)

def dense_generate_lut(model, layer_index, output='', suffix='', warning=None):
    # Generates C sources with Dense layer weights

    # Weights W
    w = model.layers[layer_index].get_weights()[0]

    # Bias b 
    b = model.layers[layer_index].get_weights()[1]

    units = model.layers[layer_index].get_config()['units']
    inputs = model.layers[layer_index].input_shape[-1]

    uprefix = "DENSE_LUT_{}_".format(layer_index) + suffix.upper()
    lprefix = uprefix.lower()
    dinputs = uprefix + "_INPUTS"
    dunits = uprefix + "_UNITS"

    # Generate the H file
    with open(os.path.join(output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + dinputs + " {}\n".format(inputs))
        f.write("#define " + dunits + " {}\n\n".format(units))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        # Weights
        w = w.T
        w = np.reshape(w, np.size(w))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_w[" + dinputs + " * " + dunits + "]", w)

        # Bias
        b = b.T
        b = np.reshape(b, np.size(b))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_b[" + dunits + "]", b)
