#!/usr/bin/python3

'''
Generates C source files with coefficients for the FIR (Finite Impulse Response) filter
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import firwin, freqz
from utils import write_c_array_flt32, write_c_array_int32, float_to_int16
import os
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--order', default=32, help='FIR filter order', type=int)
    parser.add_argument('--fs', default=32e3, help='Sampling frequency in Hz', type=int)
    parser.add_argument('--fc', default=4e3, help='Cutoff frequency in Hz', type=int)
    parser.add_argument('--window', default='hann', choices=['hamming', 'hann', 'blackman'], help='Window function')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--fixed', action='store_true', help='Fixed point output')
    parser.add_argument('--plot', action='store_true', help='Display frequency response')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Synthesize the filter
    b = firwin(numtaps=args.order + 1, cutoff=args.fc, window=args.window, fs=args.fs)
    bf = float_to_int16(b)

    # Plot the frequency response if needed
    if args.plot:
        w, h = freqz(b, 1, worN=1024)
        f = args.fs * w / (2.0 * np.pi)
        plt.plot(f, 20.0 * np.log10(np.abs(h)))
        plt.ylabel('Magnitude [dB]')
        plt.xlabel('Frequency [Hz]')
        plt.title('FIR Filter Frequency Response')
        plt.grid()
        plt.show()

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "FIR_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dlen = uprefix + "_LEN"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")
            
        f.write("#define " + dlen + " {}\n\n".format(len(b)))
            
        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        if args.fixed:
            write_c_array_int32(f, "const int16_t " + lprefix + "[" + dlen + "]", bf)
        else:
            write_c_array_flt32(f, "const flt32_t " + lprefix + "[" + dlen + "]", b)

if __name__ == "__main__":
    main()