#!/usr/bin/python3

'''
Generates C source files with coefficients for the two-class Support Vector Machine (SVM) classifier
'''

import joblib
import numpy as np
from utils import write_c_array_flt32
import os
import argparse

def kernel_params(clf):
    # Calculates the gamma kernel parameter
    k = np.zeros(2)
    if 'auto' == clf.gamma:
        k[0] = -1.0 / np.shape(clf.support_vectors_)[1]
    else:
        k[0] = -clf.gamma

    k[1] = clf.coef0

    return k

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', default='svm.pkl', help='Input filename')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Load the pkl file
    clf = joblib.load(args.input)

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "SVM_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dinputs = uprefix + "_INPUTS"
    dvectors = uprefix + "_VECTORS"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        vectors = np.shape(clf.support_vectors_)[0]
        inputs = np.shape(clf.support_vectors_)[1]

        f.write("#define " + dinputs + " {}\n".format(inputs))
        f.write("#define " + dvectors + " {}\n\n".format(vectors))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        sv = np.reshape(clf.support_vectors_, np.size(clf.support_vectors_))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_sv[" + dinputs + " * " + dvectors + "]", sv)

        alpha = clf.dual_coef_[0,:]
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_alpha[" + dvectors + "]", alpha)

        bias = clf.intercept_[0]
        f.write("const flt32_t " + lprefix + "_bias = {}f;\n\n".format(bias))

        write_c_array_flt32(f, "const flt32_t " + lprefix + "_kernel_arg[]", kernel_params(clf))

if __name__ == "__main__":
    main()