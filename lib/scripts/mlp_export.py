#!/usr/bin/python3

'''
Generates C source files with coefficients for the Multilayer Perceptron (MLP) classifier
'''

from sklearn.neural_network import MLPClassifier
import joblib
import numpy as np
from utils import write_c_array_flt32
import os
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', default='mlp.pkl', help='Input filename')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Load the pkl file
    clf = joblib.load(args.input) 

    print(clf.out_activation_) 

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "MLP_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dinputs = uprefix + "_INPUTS"
    dperceptrons = uprefix + "_PERCEPTRONS"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        for i in range(0, clf.n_layers_-1):
            n_inputs = np.shape(clf.coefs_[i])[0]
            n_perceptrons = np.shape(clf.coefs_[i])[1]

            f.write("#define " + dinputs + "{} {}\n".format(i, n_inputs))
            f.write("#define " + dperceptrons + "{} {}\n\n".format(i, n_perceptrons))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        for i in range(0, clf.n_layers_-1): 

            w = np.reshape(clf.coefs_[i].T, np.size(clf.coefs_[i].T))
            write_c_array_flt32(f, "const flt32_t " + lprefix + "_w{}[".format(i) + dinputs + "{}".format(i) + " * " + dperceptrons + "{}]".format(i), w)

            b = np.reshape(clf.intercepts_[i].T, np.size(clf.intercepts_[i].T))
            write_c_array_flt32(f, "const flt32_t " + lprefix + "_b{}[".format(i) + dperceptrons + "{}]".format(i), b)

if __name__ == "__main__":
    main()