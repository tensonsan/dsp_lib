#!/usr/bin/python3

'''
Generates C source files with coefficients for the Fast Fourier Transform (FFT)
'''

from utils import write_c_array_flt32
import numpy as np
import matplotlib.pyplot as plt
import os
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--length', default=1024, help='Maximal FFT length', type=int)
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Generate the lookup table
    n = args.length
    nn = n + n/4 + 4
    y = np.sin(2.0 * np.pi * np.arange(nn) / n)

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "FFT_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + uprefix + "_LEN {}\n\n".format(nn))
        f.write("void " + lprefix + "_sin(flt32_t **ptr);\n")
        f.write("void " + lprefix + "_cos(flt32_t **ptr);\n\n")

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        dlen = uprefix + "_LEN"
        write_c_array_flt32(f, "const flt32_t " + lprefix + "[" + dlen + "*" + dlen + "]", y)

        f.write("void " + lprefix + "_sin(flt32_t **ptr)\n")
        f.write("{\n")
        f.write("   *ptr = (flt32_t *)twiddle_lut;\n")
        f.write("}\n\n")

        f.write("void " + lprefix + "_cos(flt32_t **ptr)\n")
        f.write("{\n")
        f.write("   *ptr = (flt32_t *)" + lprefix + " + {};\n".format(n / 4))
        f.write("}\n")

if __name__ == "__main__":
    main()