#!/usr/bin/python3

'''
Example script that generates a pkl file, that contains the binominal Hidden Markov Model (HMM) classifier.
The classifier from this script is intended for unit tests and can be used as a template for the actual user classifier.
'''

import numpy as np
from hmmlearn import hmm
import joblib
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', default='hmm.pkl', help='Generated pkl file path')
    args = parser.parse_args()

    # Generate HMM classifier matrices manually (for test purpose only)
    n_states = 2

    start_probability = np.array([0.6, 0.4])

    transition_probability = np.array([
    [0.7, 0.3],
    [0.4, 0.6]
    ])

    emission_probability = np.array([
    [0.1, 0.4, 0.5],
    [0.6, 0.3, 0.1]
    ])

    model = hmm.MultinomialHMM(n_components=n_states)
    model.startprob_ = start_probability
    model.transmat_ = transition_probability
    model.emissionprob_ = emission_probability

    # Save the classifier into a pkl file
    joblib.dump(model, args.output)

if __name__ == "__main__":
    main()