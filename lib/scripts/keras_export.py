#!/usr/bin/python3

'''
Generates C source files with coefficients for the Keras-based model.

Note: This script is intended for unit tests and can be used as a template 
for the actual user classifier.

Note: Only Dense and GRU layers are supported.
'''

from keras.models import model_from_json
import numpy as np
import sys
import os
from utils import write_c_array_flt32
from keras_export_utils import *
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', default='model.json', help='Input filename')
    parser.add_argument('--weights', default='model.h5', help='Input filename')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Load the model
    json_file = open(args.model, 'r')
    loaded_model_json = json_file.read()
    json_file.close()

    # Load the weights
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights(args.weights)

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))

    # Generate source files
    gru_generate_lut(model=loaded_model, layer_index=0, output=args.output, suffix=args.suffix, warning=warning)
    gru_generate_lut(model=loaded_model, layer_index=1, output=args.output, suffix=args.suffix, warning=warning)
    dense_generate_lut(model=loaded_model, layer_index=2, output=args.output, suffix=args.suffix, warning=warning)

if __name__ == "__main__":
    main()