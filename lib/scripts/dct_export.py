#!/usr/bin/python3

'''
Generates C source files with coefficients for the DCT (Discrete Cosine Transform) type II
'''

import numpy as np
import matplotlib.pyplot as mp
import os
import argparse
from utils import write_c_array_flt32

def dct_type2_lut(n):
    # Generates a length n DCT type II transform lookup table
    w = np.pi / (2.0 * n)
    lut = np.zeros(n * n)

    for i in range(0, n):
        for j in range(0, n):
            lut[i * n + j] = np.cos((2.0 * j + 1.0) * i * w)

    return lut

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--length', default=16, help='Number of DCT input data samples', type=int)
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    warning="/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))

    n = args.length
    uprefix = "DCT_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + uprefix + "_LEN {}\n".format(n))
        f.write("#define " + uprefix + "_K1 {}\n".format(np.sqrt(2.0 / n)))
        f.write("#define " + uprefix + "_K2 {}\n\n".format(np.sqrt(1.0 / n)))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:
        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        dlen = uprefix + "_LEN"
        write_c_array_flt32(f, "const flt32_t " + lprefix + "[" + dlen + "*" + dlen + "]", dct_type2_lut(n))

if __name__ == "__main__":
    main()
