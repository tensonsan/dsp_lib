#!/usr/bin/python3

'''
Example script that generates a pkl file, that contains the Gaussian Mixture Model (GMM) classifier.
The classifier from this script is intended for unit tests and can be used as a template 
for the actual user classifier.
'''

from sklearn.model_selection import train_test_split
from sklearn import mixture
import numpy as np
import itertools
import joblib
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn import datasets
import argparse

def load_data():
    # Loads the classifier training data
    bc = datasets.load_breast_cancer()

    x = bc.data
    y = bc.target

    # normalization
    x = (x - np.mean(x)) / np.std(x)

    return x, y

def plot_roc(y_score, y_test):
    # Plots the ROC curve
    fpr, tpr, _ = roc_curve(y_test, y_score)
    roc_auc = auc(fpr, tpr)

    plt.figure()
    plt.plot(fpr, tpr, color="darkorange", lw=2, label="ROC curve (area= {})".format(roc_auc))
    plt.plot([0, 1], [0, 1], color="navy", lw=2, linestyle="--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    plt.legend(loc="lower right")
    plt.title("ROC")

def plot_confusion(classes, y_score, y_test):
    # Plots the confusion matrix
    cm = confusion_matrix(y_test, y_score)
    cm = 100 * cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)

    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)
    plt.ylim([-0.5, 1.5])

    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            plt.text(j, i, format(cm[i, j], '.2f'), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True')
    plt.xlabel('Predicted')
    plt.title("Confusion matrix")

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', default='gmm', help='Generated pkl file path')
    parser.add_argument('--plot', action='store_true', help='Plots the ROC curve and the confusion matrix')
    args = parser.parse_args()

    # Prepare the training data
    x, y = load_data()
    x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.75, random_state=0)

    xp = x_train[y_train == 0.0]
    xn = x_train[y_train == 1.0]

    # Train the classifier
    gmm1 = mixture.GaussianMixture(n_components=3, covariance_type='diag', verbose=1, max_iter=200).fit(xp)
    gmm2 = mixture.GaussianMixture(n_components=3, covariance_type='diag', verbose=1, max_iter=200).fit(xn)

    gs1 = gmm1.score_samples(x_test)
    gs2 = gmm2.score_samples(x_test)
    y = np.vstack((gs1, gs2))
    y_score = np.argmax(y, axis=0)

    if args.plot:
        plot_roc(y_score, y_test)
        plot_confusion(["Class 1", "Class 2"], y_score, y_test)
        plt.show()

    # Save the classifier into a pkl file
    joblib.dump(gmm1, args.output + "1.pkl")
    joblib.dump(gmm2, args.output + "2.pkl")

if __name__ == "__main__":
    main()