#!/usr/bin/python3

'''
Example script that generates a json/h5 files, that contain the Keras model.
The classifier from this script is intended for unit tests and can be used as a template 
for the actual user classifier.
'''

from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import GRU
from keras.datasets import imdb
import numpy as np
import argparse

def load_data():
    # Loads the classifier training data
    num_words = 2000
    maxlen = 80

    (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words = num_words)

    # pad the sequences with zeros 
    x_train = pad_sequences(x_train, maxlen = maxlen, padding = 'post')
    x_test = pad_sequences(x_test, maxlen = maxlen, padding = 'post')

    x_train = x_train.reshape(x_train.shape + (1,)).astype(dtype=float)
    x_test = x_test.reshape(x_test.shape + (1,)).astype(dtype=float)

    return x_train, y_train, x_test, y_test

def build_model():
    # Builds the model
    model = Sequential()
    model.add(GRU(32, return_sequences=True, reset_after=False))
    model.add(GRU(16, reset_after=False))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    return model
    
def save_model(model, model_path, weights_path):
    # Saves the model and its weights
    model_json = model.to_json()

    with open(model_path, "w") as json_file:
        json_file.write(model_json)

    model.save_weights(weights_path)

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', default='model.json', help='Generated model file path')
    parser.add_argument('--weights', default='model.h5', help='Generated model weights file path')
    args = parser.parse_args()

    # Train the classifier
    x_train, y_train, x_test, y_test = load_data()

    model = build_model()

    batch_size = 32

    model.fit(x_train, y_train, batch_size=batch_size, epochs=5, validation_data=(x_test, y_test))

    score, acc = model.evaluate(x_test, y_test, batch_size=batch_size)

    print('Test score:', score)
    print('Test accuracy:', acc)

    save_model(model, args.model, args.weights)

if __name__ == "__main__":
    main()