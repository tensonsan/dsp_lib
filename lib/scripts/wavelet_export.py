#!/usr/bin/python3

'''
Generates C source files with coefficients for the Discrete Wavelet Transform (DWT)
'''

import pywt
import argparse
import numpy as np
from utils import write_c_array_flt32
import os

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    wlts = pywt.wavelist(kind='discrete')
    parser.add_argument('--wavelet', choices=wlts, help='Wavelet type. Allowed values are: ' + ', '.join(wlts), metavar='')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    wavelet = pywt.Wavelet(args.wavelet)
    uprefix = "WAVELET_LUT_" + args.suffix.upper() + "_" + args.wavelet.upper()
    lprefix = uprefix.lower()
    dlen = uprefix + "_LEN"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + dlen + " {}\n\n".format(wavelet.rec_len))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        write_c_array_flt32(f, "const flt32_t " + lprefix + "_dec_lo[" + dlen + "]", wavelet.filter_bank[0])
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_dec_hi[" + dlen + "]", wavelet.filter_bank[1])
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_rec_lo[" + dlen + "]", wavelet.filter_bank[2])
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_rec_hi[" + dlen + "]", wavelet.filter_bank[3])

        write_c_array_flt32(f, "const flt32_t " + lprefix + "_dec_lo_inv[" + dlen + "]", wavelet.inverse_filter_bank[0])
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_dec_hi_inv[" + dlen + "]", wavelet.inverse_filter_bank[1])
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_rec_lo_inv[" + dlen + "]", wavelet.inverse_filter_bank[2])
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_rec_hi_inv[" + dlen + "]", wavelet.inverse_filter_bank[3])

if __name__ == "__main__":
    main()