#!/usr/bin/python3

'''
Generates C source files with coefficients for the Decision Tree classifier
'''

import joblib
from sklearn.tree import _tree
import numpy as np
from utils import write_c_array_flt32, write_c_array_int32, write_c_array_int32_touple
import os
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', default='dt.pkl', help='Input pkl file name')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Load the pkl file
    clf = joblib.load(args.input)

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "DT_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dcapacity = uprefix + "_CAPACITY"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n")
        f.write("#include \"decision_tree.h\"\n\n")
            
        f.write("#define " + dcapacity + " {}\n\n".format(len(clf.tree_.value)))
            
        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        threshold = clf.tree_.threshold
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_cut_point[" + dcapacity + "]", threshold)

        feature = clf.tree_.feature
        write_c_array_int32(f, "const int32_t " + lprefix + "_cut_var[" + dcapacity + "]", feature)

        children_left = clf.tree_.children_left
        children_right = clf.tree_.children_right
        write_c_array_int32_touple(f, "const dt_node_t " + lprefix + "_children[" + dcapacity + "]", children_left, children_right)

        dt_class = np.argmax(clf.tree_.value, axis=2)[:,0]
        write_c_array_int32(f, "const int32_t " + lprefix + "_class[" + dcapacity + "]", dt_class)

if __name__ == "__main__":
    main()