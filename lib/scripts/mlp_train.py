#!/usr/bin/python3

'''
Example script that generates a pkl file, that contains the Multilayer Perceptron (MLP) classifier.
The classifier from this script is intended for unit tests and can be used as a template 
for the actual user classifier.
'''

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import joblib
from sklearn.metrics import confusion_matrix
from sklearn import datasets
import argparse

def load_data():
    # Loads the classifier training data
    bc = datasets.load_breast_cancer()

    x = bc.data
    y = bc.target

    # normalization
    x = (x - np.mean(x)) / np.std(x)

    return x, y

def plot_roc(clf, x_test, y_test):
    # Plots the ROC curve
    y_score = clf.predict_proba(x_test)
    fpr, tpr, _ = roc_curve(y_test, y_score[:,1])
    roc_auc = auc(fpr, tpr)

    plt.figure()
    plt.plot(fpr, tpr, color="darkorange", lw=2, label="ROC curve (area= {})".format(roc_auc))
    plt.plot([0, 1], [0, 1], color="navy", lw=2, linestyle="--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    plt.legend(loc="lower right")
    plt.title("ROC")

def plot_confusion(clf, classes, x_test, y_test):
    # Plots the confusion matrix
    y_predict = clf.predict(x_test)
    cm = confusion_matrix(y_test, y_predict)
    cm = 100 * cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)

    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)
    plt.ylim([-0.5, 1.5])

    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            plt.text(j, i, format(cm[i, j], '.2f'), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True')
    plt.xlabel('Predicted')
    plt.title("Confusion matrix")

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', default='gmm', help='Generated pkl file path')
    parser.add_argument('--plot', action='store_true', help='Plots the ROC curve and the confusion matrix')
    args = parser.parse_args()

    # Prepare the training data
    x, y = load_data()
    x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.75, random_state=0)

    # Train the classifier
    clf = MLPClassifier(solver='sgd', activation='logistic', alpha=1e-5, hidden_layer_sizes=(10,), random_state=1, verbose=1, max_iter=10000)
    clf.fit(x_train, y_train)

    print(clf.score(x_test, y_test))

    if args.plot:
        plot_roc(clf, x_test, y_test)
        plot_confusion(clf, ["Class 1", "Class 2"], x_test, y_test)
        plt.show()

    # Save the classifier into a pkl file
    joblib.dump(clf, args.output)

if __name__ == "__main__":
    main()
