#!/usr/bin/python3

'''
Generates C source files with coefficients for the Gaussian Mixture Model (GMM) classifier

Note: Only the Diagonal Covariance Matrix GMM is supported.
'''

import joblib
import numpy as np
from utils import write_c_array_flt32
import os
import argparse

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', default='gmm.pkl', help='Input filename')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Load the pkl file
    clf = joblib.load(args.input)

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "GMM_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dinputs = uprefix + "_INPUTS"
    dmixtures = uprefix + "_MIXTURES"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        mixtures = np.shape(clf.means_)[0]
        inputs = np.shape(clf.means_)[1]

        f.write("#define " + dinputs + " {}\n".format(inputs))
        f.write("#define " + dmixtures + " {}\n\n".format(mixtures))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        covar = np.reshape(clf.covariances_, np.size(clf.covariances_))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_covariances[" + dinputs + " * " + dmixtures + "]", covar)

        mean = np.reshape(clf.means_, np.size(clf.means_))
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_means[" + dinputs + " * " + dmixtures + "]", mean)

        weight = clf.weights_
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_weights[" + dmixtures + "]", weight)

if __name__ == "__main__":
    main()