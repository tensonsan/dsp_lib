#!/usr/bin/python3

'''
Generates C source files with coefficients for the binominal Hidden Markov Model (HMM) classifier
'''

from sklearn.neural_network import MLPClassifier
import joblib
import numpy as np
from utils import write_c_array_flt32
import os
import argparse
import sys

def llog(x):
    # Calculates the natural logarithm in a safe way
    y = []
    for v in x:
        if 0.0 == v:
            y.append(-sys.float_info.max)
        else:
            y.append(np.log(v))

    return y

def main():
    # Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', default='hmm.pkl', help='Input filename')
    parser.add_argument('--suffix', default='', help='Output filename suffix')
    parser.add_argument('--output', default='', help='Output path for generated files')
    args = parser.parse_args()

    # Load the pkl file
    clf = joblib.load(args.input)

    # Transform matrices into log linear arrays
    a = clf.transmat_
    b = clf.emissionprob_
    pi = clf.startprob_

    n, m = np.shape(b)

    a = np.reshape(a, np.size(a))
    b = np.reshape(b, np.size(b))

    a = llog(a)
    b = llog(b)
    pi = llog(pi)

    warning = "/* This file was created automatically by the {} script, DO NOT MODIFY! */".format(os.path.basename(__file__))
    uprefix = "HMM_LUT_" + args.suffix.upper()
    lprefix = uprefix.lower()
    dm = uprefix + "_M"
    dn = uprefix + "_N"

    # Generate the H file
    with open(os.path.join(args.output, lprefix + ".h"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#ifndef _" + uprefix + "_H_\n")
        f.write("#define _" + uprefix + "_H_\n\n")
        f.write("#include \"types.h\"\n\n")

        f.write("#define " + dm + " {}\n".format(m))
        f.write("#define " + dn + " {}\n\n".format(n))

        f.write("#endif\n")

    # Generate the C file
    with open(os.path.join(args.output, lprefix + ".c"), "w") as f:

        f.write("\n" + warning + "\n")
        f.write("\n#include \"" + lprefix + ".h\"\n\n")

        write_c_array_flt32(f, "const flt32_t " + lprefix + "_a[" + dn + " * " + dn + "]", a)
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_b[" + dm + " * " + dn + "]", b)
        write_c_array_flt32(f, "const flt32_t " + lprefix + "_pi[" + dn + "]", pi)

if __name__ == "__main__":
    main()