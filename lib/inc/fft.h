
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Fast Fourier Transform API
 *
 **********************************************************************/

/**
 * @addtogroup FFT Fast Fourier Transform
 * @{
 */

#ifndef _FFT_H_
#define _FFT_H_

#include "types.h"

void fft(cplx_t *x, int32_t type, int32_t n);
void fft_real(flt32_t *x, cplx_t *y, int32_t n);
void ifft_real(cplx_t *x, flt32_t *y, int32_t n);

void fft_magnitude(cplx_t *x, flt32_t *y, int32_t n, flt32_t k);
void fft_power(cplx_t *x, flt32_t *y, int32_t n, flt32_t k);

int32_t fft_bin2freq(int32_t bin, int32_t n, int32_t fs);
int32_t fft_freq2bin(int32_t f, int32_t n, int32_t fs);

#endif

/**
 * @}
 */
