
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Activation Functions API
 *
 **********************************************************************/

/**
 * @addtogroup Activation Activation functions
 * @{
 */

#ifndef _ACTIVATION_H_
#define _ACTIVATION_H_

#include "types.h"

flt32_t act_leaky_relu(flt32_t x, flt32_t a);
flt32_t act_satlins(flt32_t x);
flt32_t act_logsig(flt32_t x);
flt32_t act_tanh(flt32_t x);
flt32_t act_linear(flt32_t x);
void act_softmax(flt32_t *x, int32_t n);
flt32_t act_hard_sigmoid(flt32_t x);

#endif

/**
 * @}
 */
