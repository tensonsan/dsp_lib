
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Edge detector API
 *
 **********************************************************************/

/**
 * @addtogroup EdgeDetector Edge detector
 * @{
 */
 
#ifndef _EDGE_DETECTOR_H_
#define _EDGE_DETECTOR_H_

#include "types.h"

/// Edge detector FSM
typedef enum
{
    ed_idle = 0,    ///< Idle state
    ed_waiting,     ///< Waiting for edge state
    ed_done         ///< Edge detected state
} ed_state_t;

/// Edge detector handle
typedef struct
{
    int32_t start;      ///< Start edge detection
    ed_state_t state;   ///< Edge detector FSM state
    flt32_t max;        ///< Long term input signal maximum
    flt32_t threshold;  ///< max threshold percentage
    int32_t count;      ///< Number of consecutive values below max
    int32_t onset;      ///< Threshold onset
} edge_detector_t;

void ed_init(edge_detector_t *hnd, flt32_t threshold, int32_t onset);
void ed_process(edge_detector_t *hnd, flt32_t x, int32_t active);
ed_state_t ed_state(edge_detector_t *hnd);
flt32_t ed_peak_get(edge_detector_t *hnd);
void ed_rearm(edge_detector_t *hnd);

#endif

/**
 * @}
 */
