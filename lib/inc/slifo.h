
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Sample LIFO API
 *
 **********************************************************************/
 
 /**
  * @addtogroup SLIFO Sample LIFO
  * @{
  */

#ifndef _SLIFO_H_
#define _SLIFO_H_

#include "types.h"

/// Sample LIFO handle
typedef struct
{
    volatile int32_t length;      ///< \p buffer length in samples
    volatile flt32_t *buffer;     ///< Buffer for storing samples
    volatile int32_t index;       ///< Write index
    volatile int32_t index_copy;  ///< Copy of write index 
    volatile uint8_t full;        ///< Buffer full flag (1 = buffer full, 0 = buffer not full)
} slifo_t;

void slifo_init(slifo_t *hnd, flt32_t *buffer, int32_t length);
void slifo_write(slifo_t *hnd, flt32_t val);
int32_t slifo_read(slifo_t *hnd, flt32_t *buffer, int32_t length, int32_t offset);
void slifo_freeze(slifo_t *hnd);
uint8_t slifo_full(slifo_t *hnd);
void slifo_flush(slifo_t *hnd);

/// Sample LIFO handle
typedef struct
{
    volatile int32_t length;      ///< Buffer length in samples
    volatile int16_t *buffer;     ///< Buffer for storing samples
    volatile int32_t index;       ///< Write index
    volatile int32_t index_copy;  ///< Copy of write index 
    volatile uint8_t full;        ///< Buffer full flag (1 = buffer full, 0 = buffer not full)
} slifo16_t;

void slifo16_init(slifo16_t *hnd, int16_t *buffer, int32_t length);
void slifo16_write(slifo16_t *hnd, int16_t val);
int32_t slifo16_read(slifo16_t *hnd, flt32_t *buffer, int32_t length, int32_t offset);
void slifo16_freeze(slifo16_t *hnd);
uint8_t slifo16_full(slifo16_t *hnd);
void slifo16_flush(slifo16_t *hnd);

#endif

/**
 * @}
 */
