
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Spectral Subtraction API
 *
 **********************************************************************/

/**
 * @addtogroup SSUB Spectral Subtraction
 * @{
 */

#ifndef _SPECTRAL_SUBTRACTION_H_
#define _SPECTRAL_SUBTRACTION_H_

#include "types.h"

/// Spectral subtraction handle
typedef struct
{
    int32_t noise_count;      ///< Noise onset counter
    int32_t noise_onset;      ///< Noise onset counter threshold
    flt32_t noise_threshold;  ///< Noise threshold in dB
    flt32_t *noise_mean;      ///< Mean noise magnitude spectrum buffer
    int32_t noise_length;     ///< \p noise_mean length in samples
    flt32_t alpha;            ///< Over-subtraction factor
    flt32_t beta;             ///< Mean noise scale factor
    flt32_t k;                ///< Exponential moving average coefficient for noise frame update
} ssub_t;

void ssub_reset(ssub_t *hnd);
void ssub_init(ssub_t *hnd, flt32_t *buffer, int32_t length, flt32_t alpha, flt32_t beta, flt32_t k, int32_t noise_onset, flt32_t noise_threshold);
void ssub_noise_update(ssub_t *hnd, flt32_t *x);
int32_t ssub_process(ssub_t *hnd, flt32_t *x);
int32_t ssub_vad(ssub_t *hnd, flt32_t *x);

#endif

/**
 * @}
 */
