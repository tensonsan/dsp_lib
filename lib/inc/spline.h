/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Natural Cubic Spline Interpolation API
 *
 **********************************************************************/

/**
 * @addtogroup Spline Natural Cubic Spline Interpolation
 * @{
 */

#ifndef _SPLINE_H_
#define _SPLINE_H_

#include "types.h"

/// Spline handle
typedef struct 
{
    flt32_t *x;     ///< Spline X points
    int32_t n;      ///< Number of spline points - 1
    flt32_t *h;     ///< Difference array
    flt32_t *mu;    ///< Superdiagonal array
    flt32_t *z;     ///< Subdiagonal array

    flt32_t *a;     ///< Spline coefficients
    flt32_t *b;     ///< Spline coefficients
    flt32_t *c;     ///< Spline coefficients
    flt32_t *d;     ///< Spline coefficients
} spline_t;

void spline_init(spline_t *hnd, flt32_t *h, flt32_t *mu, flt32_t *z, flt32_t *b, flt32_t *c, flt32_t *d);
void spline_fit(spline_t *hnd, flt32_t *x, flt32_t *y, int32_t n);
void spline_interpolate(spline_t *hnd, flt32_t *x, flt32_t *y, int32_t n);

#endif

/**
 * @}
 */
