
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Support Vector Machine API
 * 
 **********************************************************************/

/**
 * @addtogroup SVM Support Vector Machine
 * @{
 */

#ifndef _SVM_H_
#define _SVM_H_

#include "types.h"

/// SVM kernel
typedef flt32_t (*svm_kernel_t)(flt32_t *x1, flt32_t *x2, int32_t n, flt32_t *arg);

/// SVM handle
typedef struct
{
    int32_t n;              ///< Length of input vector (feature dimension)
    int32_t m;              ///< Number of support vectors & Lagrangian multipliers
    flt32_t *alpha;         ///< Array of precomputed Lagrangian multipliers
    flt32_t *sv;            ///< Array of precomputed support vectors
    flt32_t b;              ///< Precomputed bias coefficient
    svm_kernel_t kernel;    ///< Kernel
    flt32_t *kernel_arg;    ///< Kernel arguments
} svm_t;

// Kernel functions
flt32_t svm_lin(flt32_t *x1, flt32_t *x2, int32_t n, flt32_t *k);
flt32_t svm_rbf(flt32_t *x1, flt32_t *x2, int32_t n, flt32_t *k);

void svm_normalize(flt32_t *x, int32_t n_inputs, const flt32_t *o, const flt32_t *s);
void svm_init(svm_t *hnd, const flt32_t *alpha, const flt32_t *sv, const flt32_t b, int32_t n, int32_t m, svm_kernel_t kernel, const flt32_t *kernel_arg);
flt32_t svm_classify(svm_t *hnd, flt32_t *x);

#endif

/**
 * @}
 */

