
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Autocorrelation API
 *
 **********************************************************************/

/**
 * @addtogroup ACORR Autocorrelation API
 * @{
 */

#ifndef _ACORR_H_
#define _ACORR_H_

#include "types.h"

void acorr_norm(flt32_t *x, flt32_t *r, int32_t n, int32_t m);
void acorr_fft(flt32_t *x, flt32_t *y, int32_t n);

#endif

/**
 * @}
 */
