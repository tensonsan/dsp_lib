
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Dynamic Time Warping API
 *
 **********************************************************************/

/**
 * @addtogroup DTW Dynamic Time Warping
 * @{
 */

#ifndef _DTW_H_
#define _DTW_H_

#include "types.h"

void dtw_calc(flt32_t *x, flt32_t *y, int32_t nx, int32_t ny, flt32_t *dtw);
flt32_t dtw_backtrace(flt32_t *x, flt32_t *y, int32_t nx, int32_t ny, flt32_t *dtw);

#endif

/**
 * @}
 */
