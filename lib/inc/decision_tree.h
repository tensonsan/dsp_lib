
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Decision Tree Classifier API
 *
 **********************************************************************/

/**
 * @addtogroup DecisionTree Decision Tree
 * @{
 */

#ifndef _DECISION_TREE_H_
#define _DECISION_TREE_H_

#include "types.h"

/// Empty decision tree node
#define DT_NODE_NIL -1

/// Decision tree node
typedef struct
{
    int32_t left;     ///< Left child node
    int32_t right;    ///< Right child node
} dt_node_t;

/// Decision tree handle
typedef struct
{
    int32_t *cut_var;         ///< Array of tree cut variable indices
    flt32_t *cut_point;       ///< Array of tree cut point values
    int32_t *classes;         ///< Output classes
    dt_node_t *children;      ///< Array of decision treee child nodes
    int32_t n;                ///< Index of the deepest child node
} dt_handle_t;

void dt_init(dt_handle_t *hnd, const int32_t *cut_var, const flt32_t *cut_point, const dt_node_t *children, const int32_t *classes, int32_t n);
int32_t dt_process(dt_handle_t *hnd, flt32_t *x);

#endif

/**
 * @}
 */
