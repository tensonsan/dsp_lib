
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Utility functions
 *
 **********************************************************************/

 /**
  * @addtogroup utils Utility functions
  * @{
  */

#ifndef _UTILS_H_
#define _UTILS_H_

#include "types.h"

/// Macro for converting 16-bit ADC value to float
#define AD2FLT32(x) ((flt32_t)((int16_t)(x)) / 32768.0f)  

/// Macro for converting float to 16-bit ADC value
#define FLT322AD(x) ((int16_t)((x) * 32768.0f))

/// Macro for obtaining max value from two values
#ifndef MAX
#define MAX(a, b)   ((a) > (b) ? (a) : (b))
#endif

/// Macro for obtaining min value from two values
#ifndef MIN
#define MIN(a, b)   ((a) < (b) ? (a) : (b))
#endif

/// Macro for swapping two values
#ifndef SWAP
#define SWAP(a, b, t) ((t) = (a), (a) = (b), (b) = (t))
#endif

/// Macro for computing a 2D matrix index in 1D array
#define IDX(x, y, n)    ((x) * (n) + (y))

/// Macro for pi
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

int32_t rand_interval(int32_t min, int32_t max);
void shellsort(flt32_t *x, int32_t n);

#endif

/**
 * @}
 */
