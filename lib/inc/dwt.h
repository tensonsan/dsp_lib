
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Discrete Wavelet Transform API
 *
 **********************************************************************/

/**
 * @addtogroup DWT Discrete Wavelet Transform
 * @{
 */

#ifndef _DWT_H_
#define _DWT_H_

#include "types.h"

void dwt_periodic(flt32_t *x, flt32_t *y, int32_t n, const flt32_t *lo, const flt32_t *hi, int32_t m);
void idwt_periodic(flt32_t *x, flt32_t *y, int32_t n, const flt32_t *lo, const flt32_t *hi, int32_t m);

#endif

/**
 * @}
 */
