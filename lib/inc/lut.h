
// File was generated automatically by script lut_gen.m, do NOT modify!

#ifndef _LUT_H_
#define _LUT_H_

#include "types.h"

#define LUT_LEN 1024

void lut_sin_ptr(flt32_t **ptr);
void lut_cos_ptr(flt32_t **ptr);

#endif
