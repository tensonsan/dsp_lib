
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Linear Predictive Coding API
 *
 **********************************************************************/

/**
 * @addtogroup LPC Linear Predictive Coding
 * @{
 */

#ifndef _LPC_H_
#define _LPC_H_

#include "types.h"

void lpc_levinson_durbin(flt32_t *r, flt32_t *a, flt32_t *e, int32_t m);
void lpc_synthesis(flt32_t *x, flt32_t *r, flt32_t *a, flt32_t *e, int32_t n, int32_t m);
void lpc_freq_response(flt32_t *x, flt32_t *y, flt32_t e, flt32_t *a, int32_t n, int32_t m);
void lpc_prediction(flt32_t *x, flt32_t *y, flt32_t *a, flt32_t e, int32_t n, int32_t m);
void lpc2cep(flt32_t *a, flt32_t *c, flt32_t e, int32_t m);

#endif

/**
 * @}
 */
