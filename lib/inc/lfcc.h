
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   LFCC/LFSC API
 *
 **********************************************************************/

/**
 * @addtogroup LFxC Linear Frequency Cepstral/Spectral Coefficients
 * @{
 */

#ifndef _LFCC_H_
#define _LFCC_H_

#include "types.h"

void lfxc_init(int32_t *centers, int32_t n_centers, int32_t n);
void lfcc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y);
void lfsc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y);

#endif

/**
 * @}
 */

