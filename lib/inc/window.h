
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Signal windowing API
 *
 **********************************************************************/

/**
 * @addtogroup SigWin Signal windowing
 * @{
 */

#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "types.h"

#define WIN_HANN_SCALE      2.0f    ///< Hann window amplitude correction factor
#define WIN_HAMMING_SCALE   1.855f  ///< Hamming window amplitude correction factor
#define WIN_BARTLETT_SCALE  2.0f    ///< Bartlett window amplitude correction factor

void win_hann(flt32_t *x, int32_t n);
void win_hamming(flt32_t *x, int32_t n);
void win_bartlett(flt32_t *x, int32_t n);

#endif

/**
 * @}
 */
