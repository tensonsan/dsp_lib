
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Biquad Filter API
 *
 **********************************************************************/

/**
 * @addtogroup Biquad Biquad Filter
 * @{
 */

#ifndef _BIQUAD_H_
#define _BIQUAD_H_

#include "types.h"

#define BIQUAD_STATES 3 ///< Number of filter states in one stage

/// Biquad filter handle
typedef struct
{
    flt32_t *c; ///< Filter coefficients for all stages
    flt32_t *s; ///< Filter states for all stages
    int32_t nc; ///< Number of filter coefficients in one stage
    int32_t ns; ///< Number of filter stages
} biquad_t;

void biquad_init(biquad_t *hnd, const flt32_t *c, flt32_t *s, int32_t nc, int32_t ns);
flt32_t biquad_calc(biquad_t *hnd, flt32_t x);

#endif

/**
 * @}
 */
