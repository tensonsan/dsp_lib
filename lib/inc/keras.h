
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Keras Neural Network API
 *
 **********************************************************************/

/**
 * @addtogroup Keras Keras Neural Network
 * @{
 */

#ifndef _KERAS_H_
#define _KERAS_H_

#include <stdint.h>
#include "types.h"

void keras_gru(flt32_t *x,
        flt32_t *h,
        const flt32_t *wz,
        const flt32_t *uz,
        const flt32_t *bz,
        const flt32_t *wr,
        const flt32_t *ur,
        const flt32_t *br,
        const flt32_t *wh,
        const flt32_t *uh,
        const flt32_t *bh,
        int32_t n,
        int32_t m);

void keras_gru_reset(flt32_t *h, int32_t m);

void keras_dense(flt32_t *x,
            flt32_t *y,
            const flt32_t *w,
            const flt32_t *b,
            int32_t n,
            int32_t m);

#endif

/**
 * @}
 */
