
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Peak Search API
 *
 **********************************************************************/

 /**
 * @addtogroup PeakSearch Peak Search API
 * @{
 */

#ifndef _PEAK_FIND_H_
#define _PEAK_FIND_H_

#include "types.h"

/// Peak structure
typedef struct
{
    int32_t pos;    ///< Peak position (index in \p x)
    flt32_t val;    ///< Peak value
} peak_t;

int32_t peak_find(flt32_t *x, int32_t imin, int32_t imax);
void peak_find_n(flt32_t *x, int32_t n, int32_t m, int32_t bw, peak_t *peaks);
void peak_find_all(flt32_t *y, int32_t *xmin, int32_t *nmin, int32_t *xmax, int32_t *nmax, int32_t n);

#endif

/**
 * @}
 */
