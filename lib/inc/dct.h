
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Discrete Cosine Transform API
 *
 **********************************************************************/

/**
 * @addtogroup DCT Discrete Cosine Transform
 * @{
 */
 
#ifndef _DCT_H_
#define _DCT_H_

#include "types.h"

void dct_fft(flt32_t *x, flt32_t *y, int32_t n);
void dct_norm(flt32_t *x, flt32_t *y, int32_t n);
void dct_fast(flt32_t *x, flt32_t *y, const flt32_t k1, const flt32_t k2, const flt32_t *w, int32_t n);

#endif

/**
 * @}
 */

