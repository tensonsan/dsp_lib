
#ifndef _TYPES_H_
#define _TYPES_H_

#include <stdint.h>
#include <float.h>

#define FLT32_MAX (FLT_MAX)

typedef float flt32_t;
typedef double flt64_t;

/// Complex number container
typedef struct
{
    flt32_t re; ///< Real part
    flt32_t im; ///< Inaginary part
} cplx_t;

#endif