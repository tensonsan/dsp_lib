
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Cepstrum Transform API
 *
 **********************************************************************/

/**
 * @addtogroup Cepstrum Cepstrum Transform
 * @{
 */

#ifndef _CEPSTRUM_H_
#define _CEPSTRUM_H_

#include "types.h"

flt32_t cepst_bin2quef(int32_t bin, int32_t n, int32_t fs);
void cepstrum(flt32_t *x, flt32_t *y, int32_t n);

#endif

/**
 * @}
 */

