
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Gaussian Mixture Model API
 * 
 **********************************************************************/

/**
 * @addtogroup GMM Gaussian Mixture Model
 * @{
 */

#ifndef _GMM_H_
#define _GMM_H_

#include "types.h"

/// GMM handle
typedef struct
{
    int32_t n;        ///< Length of input vector (dimension of DMVG)
    int32_t m;        ///< Number of DMVG mixtures
    flt32_t *covar;   ///< Array of precomputed GMM covariances
    flt32_t *mean;    ///< Array of precomputed GMM means
    flt32_t *c;       ///< Array of precomputed DMVG coefficients
    flt32_t *w;       ///< Array of precomputed GMM weights (DMVG mixture probabilities)
    flt32_t *logw;    ///< Array of log \p w
} gmm_t;

void gmm_init(gmm_t *hnd, const flt32_t *mean, const flt32_t *covar, const flt32_t *w, flt32_t *logw, flt32_t *c, int32_t n, int32_t m);
flt32_t gmm_calc(gmm_t *hnd, flt32_t *x, flt32_t *post);

#endif

/**
 * @}
 */
