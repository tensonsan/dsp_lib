
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Statistics API
 *
 **********************************************************************/

 /**
 * @addtogroup Stat Statistics
 * @{
 */

#ifndef _STATISTICS_H_
#define _STATISTICS_H_

#include "types.h"

/// Linear regression handle
typedef struct stat_linreg_t
{
    int32_t xm;   ///< Mean X value
    flt32_t ym;   ///< Mean Y value
    flt32_t k;    ///< Regression line slope
} stat_linreg_t;

/// Moving average handle
typedef struct
{
    int32_t length;   ///< Buffer length in samples
    flt32_t *buffer;  ///< Buffer for storing samples
    int32_t index;    ///< Buffer write index
    flt64_t sum;      ///< Moving average sum
} stat_ma_t;

/// Exponential moving average handle
typedef struct
{
    flt32_t y;  ///< Old output value
    flt32_t k;  ///< Coeficient
} stat_ema_t;

flt32_t stat_mean(flt32_t *x, int32_t n);
flt32_t stat_median(flt32_t *x, int32_t n);
flt32_t stat_mean_abs(flt32_t *x, int32_t n);
flt32_t stat_rms(flt32_t *x, int32_t n);
flt32_t stat_relvar(flt32_t *x, int32_t n);
flt32_t stat_mad(flt32_t *x, int32_t n);
void stat_min(flt32_t *x, int32_t n, int32_t *imin);
void stat_max(flt32_t *x, int32_t n, int32_t *imax);
flt32_t stat_entropy(flt32_t *x, int32_t n);

flt32_t stat_linreg_calc(int32_t x, stat_linreg_t *arg);
void stat_linreg_fit(int32_t x1, int32_t x2, flt32_t *y, stat_linreg_t *arg);

void ma_init(stat_ma_t *hnd, flt32_t *buffer, int32_t n);
flt32_t ma_calc(stat_ma_t *hnd, flt32_t val);

void ema_init(stat_ema_t *hnd, flt32_t k);
flt32_t ema_calc(stat_ema_t *hnd, flt32_t x);

#endif

/**
 * @}
 */
