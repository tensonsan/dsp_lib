
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Goertzel filter API
 *
 **********************************************************************/

/**
 * @addtogroup Goertzel Goertzel filter
 * @{
 */

#ifndef _GOERTZEL_H_
#define _GOERTZEL__H_

#include "types.h"

void goertzl(flt32_t *x, cplx_t *y, int32_t f, int32_t fs, int32_t n);

#endif

/**
 * @}
 */
