
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Multilayer Perceptron API
 * 
 **********************************************************************/

/**
 * @addtogroup MLP Multilayer Perceptron
 * @{
 */

#ifndef _ANN_H_
#define _ANN_H_

#include "types.h"

/// Activation function
typedef flt32_t (*ann_function_t)(flt32_t);

void ann_normalize(flt32_t *x, int32_t n);
void ann_denormalize(flt32_t *x, int32_t n);
void ann_normalize_m(flt32_t *x, int32_t n, const flt32_t *xmin, const flt32_t *xmax);

void ann_feedforward(const flt32_t *w, const flt32_t *b, flt32_t *x, flt32_t *y, int32_t n_inputs, int32_t n_neurons, ann_function_t f);

#endif

/**
 * @}
 */
