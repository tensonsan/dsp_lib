
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Hidden Markov Model API
 *
 **********************************************************************/

/**
 * @addtogroup HMM Hidden Markov Model
 * @{
 */

#ifndef _HMM_H_
#define _HMM_H_

#include "types.h"

/// HMM handle
typedef struct 
{
    const flt32_t *a;   ///< Log transition probability matrix
    const flt32_t *b;   ///< Log emission probability matrix
    const flt32_t *pi;  ///< Log initial probabilities
    int32_t N;          ///< Transition matrix dimension
    int32_t M;          ///< Emission matrix dimension
} hmm_t;

void hmm_init(hmm_t *hmm, const flt32_t *a, const flt32_t *b, const flt32_t *pi, int32_t N, int32_t M);
flt32_t hmm_viterbi(hmm_t *hmm, flt32_t *delta, int32_t *o, int32_t T);

#endif

/**
 * @}
 */
