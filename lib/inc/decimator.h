
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   API for singal downsampling (FIR filtering + decimation)
 *
 **********************************************************************/

/**
 * @addtogroup Downsampler Downsampling
 * @{
 */

#ifndef _DECIMATOR_H_
#define _DECIMATOR_H_

#include "types.h"

/// Downsampler callback
typedef void (*decim_proc_t)(flt32_t value);

/// Downsampler handle
typedef struct
{
    flt32_t *buffer;      ///< Circular buffer for storing input samples
    flt32_t *c;           ///< Array of FIR filter coefficients
    int32_t index;        ///< Circular buffer index
    int32_t m;            ///< Decimation factor
    int32_t n;            ///< Length of \p c and \p buffer
    int32_t cnt;          ///< Input sample counter
    decim_proc_t proc;    ///< Downsampling callback
} decim_t;

void decim_init(decim_t *hnd, flt32_t *buffer, const flt32_t *c, int32_t n, int32_t m, decim_proc_t proc);
void decim_calc(decim_t *hnd, flt32_t val);
void decim_reset(decim_t *hnd);
void decim_calc_buffer(flt32_t *x, flt32_t *y, const flt32_t *c, int32_t nc, int32_t n, int32_t m);

/// Downsampler callback
typedef void (*decim16_proc_t)(int16_t value);

/// Downsampler handle
typedef struct
{
    int16_t *buffer;      ///< Circular buffer for storing input samples
    int16_t *c;           ///< Array of FIR filter coefficients
    int32_t index;        ///< Circular buffer index
    int32_t m;            ///< Decimation factor
    int32_t n;            ///< Length of \p c and \p buffer
    int32_t cnt;          ///< Input sample counter
    decim16_proc_t proc;  ///< Downsampling callback
} decim16_t;

void decim16_init(decim16_t *hnd, int16_t *buffer, const int16_t *c, int32_t n, int32_t m, decim16_proc_t proc);
void decim16_calc(decim16_t *hnd, int16_t val);
void decim16_reset(decim16_t *hnd);
void decim16_calc_buffer(int16_t *x, int16_t *y, const int16_t *c, int32_t nc, int32_t n, int32_t m);

#endif

/**
 * @}
 */
