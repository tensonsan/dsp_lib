
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Hilbert Transform API
 *
 **********************************************************************/

/**
 * @addtogroup HT Hilbert Transform
 * @{
 */

#ifndef _HILBERT_H_
#define _HILBERT_H_

#include "types.h"

void hilbert(flt32_t *x, flt32_t *y, int32_t n);

#endif

/**
 * @}
 */
