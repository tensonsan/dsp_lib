
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   LMS Filter API
 *
 **********************************************************************/

/**
 * @addtogroup LMS Least Mean Square Filter
 * @{
 */

#ifndef _LMS_FILTER_H_
#define _LMS_FILTER_H_

#include "types.h"
#include "fir.h"

/// LMS filter handle
typedef struct
{
    fir_t fir;      ///< FIR filter instance
    flt32_t k;      ///< LMS filter learning rate
    flt32_t err;    ///< LMS filter error value
} lms_t;

void lms_init(lms_t *hnd, flt32_t *buffer, const flt32_t *c, int32_t n, flt32_t k);
flt32_t lms_calc(lms_t *hnd, flt32_t x, flt32_t d);

#endif

/**
 * @}
 */
