
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Pre-emphasis Filter API
 *
 **********************************************************************/

/**
 * @addtogroup Preemph Preemphasis Filter
 * @{
 */

#ifndef _PREEMPH_H_
#define _PREEMPH_H_

#include "types.h"

void preemph_calc(flt32_t *x, int32_t n, flt32_t k);
void preemph_calc_dynamic(flt32_t *x, int32_t n);

#endif

/**
 * @}
 */
