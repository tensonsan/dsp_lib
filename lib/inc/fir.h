
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   FIR filter API
 *
 **********************************************************************/

/**
 * @addtogroup FIR FIR Filter
 * @{
 */

#ifndef _FIR_H_
#define _FIR_H_

#include "types.h"

/// FIR handle
typedef struct
{
    flt32_t *buffer;    ///< Circular buffer for storing input samples
    flt32_t *c;         ///< Array of filter coefficients
    int32_t index;      ///< Circular buffer index
    int32_t n;          ///< Length of \p buffer and \p c
} fir_t;

void fir_init(fir_t *hnd, flt32_t *buffer, const flt32_t *c, int32_t n);
flt32_t fir_calc(fir_t *hnd, flt32_t x);
void fir_calc_buffer(flt32_t *x, flt32_t *y, const flt32_t *c, int32_t nc, int32_t n);

/// FIR handle
typedef struct
{
    int16_t *buffer;    ///< Circular buffer for storing input samples
    const int16_t *c;   ///< Array of filter coefficients
    int32_t index;      ///< Circular buffer index
    int32_t n;          ///< Length of \p buffer and \p c
} fir16_t;

void fir16_init(fir16_t *hnd, int16_t *buffer, const int16_t *c, int32_t n);
int16_t fir16_calc(fir16_t *hnd, int16_t x);
void fir16_calc_buffer(int16_t *x, int16_t *y, const int16_t *c, int32_t nc, int32_t n);

#endif

/**
 * @}
 */
