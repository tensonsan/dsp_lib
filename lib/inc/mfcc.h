
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   MFxC Coefficient Calculation API
 *
 **********************************************************************/

/**
 * @addtogroup MFxC Mel Frequency Cepstral/Spectral Coefficients
 * @{
 */

#ifndef _MFCC_H_
#define _MFCC_H_

#include "types.h"

void mfxc_init(int32_t *centers, int32_t n_centers, int32_t n, int32_t fs);
void mfcc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y);
void mfsc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y);

#endif

/**
 * @}
 */

