
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Playback control API
 *
 **********************************************************************/

/**
 * @addtogroup Playback Playback control
 * @{
 */

#ifndef _PLAY_H_
#define _PLAY_H_

#include "types.h"

/// Playback of the entire data buffer
#define PLAY_LENGTH_ALL 0xffffffff

/// Playback handle
typedef struct
{
    int16_t *data;    ///< Playback buffer
    int32_t index;    ///< Playback read index
    int32_t capacity; ///< \p data length in samples 
    int32_t length;   ///< Playback length in samples
    int32_t active;   ///< Playback active flag: 1 = active, 0 = idle 
} play_t;

void play_process(play_t *play, int16_t *value);
int32_t play_init(play_t *play, int16_t *data, int32_t length);
int32_t play_control(play_t *play, int32_t offset, int32_t length, int32_t active);
int32_t play_active(play_t *play);

#endif

/**
 * @}
 */
