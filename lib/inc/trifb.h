
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Triangular filterbank API
 *
 **********************************************************************/

/**
 * @addtogroup TRIFB Triangular Filterbank
 * @{
 */

#ifndef _TRIFB_H_
#define _TRIFB_H_

#include "types.h"

void trifb_linear(int32_t *centers, int32_t n_centers, int32_t n);
void trifb_mel(int32_t *centers, int32_t n_centers, int32_t n, int32_t fs);
void trifb_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y);
void trifb_coeffs(int32_t *centers, int32_t n_centers, int32_t n, flt32_t *coeff);

#endif

/**
 * @}
 */
