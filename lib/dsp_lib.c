
#include <stdio.h>
#include <string.h>
#include "statistics.h"
#include "fft.h"
#include "acorr.h"
#include "lpc.h"
#include "trifb.h"
#include "ann.h"
#include "mlp_lut_test.h"
#include "decision_tree.h"
#include "dt_lut_test.h"
#include "svm.h"
#include "svm_lut_test_lin.h"
#include "svm_lut_test_rbf.h"
#include "gmm.h"
#include "gmm_lut_test.h"
#include "fir.h"
#include "biquad.h"
#include "biquad_lut_test.h"
#include "cepstrum.h"
#include "window.h"
#include "decimator.h"
#include "fir_lut_decimator_test.h"
#include "fir_lut_decimator_16_test.h"
#include "dct.h"
#include "dct_lut_test.h"
#include "lms_filter.h"
#include "hilbert.h"
#include "spline.h"
#include "peak_find.h"
#include "wavelet_lut_test_db8.h"
#include "dwt.h"
#include "dtw.h"
#include "hmm.h"
#include "hmm_lut_test.h"
#include "preemph.h"
#include "lfcc.h"
#include "mfcc.h"
#include "spectral_subtraction.h"
#include "edge_detector.h"
#include "keras.h"
#include "gru_lut_0_test.h"
#include "gru_lut_1_test.h"
#include "dense_lut_2_test.h"
#include "activation.h"
#include "goertzel.h"

#if 1
void dump_flt(const char *name, float *x, int n)
{
    int i;

    printf("%s:\n", name);
    for (i=0; i < n; ++i)
    {
        printf("%f\n", x[i]);
    }
    printf("\n");
}

void dump_flt_file(const char *name, float *x, int n)
{
    int i;

    FILE *fp = fopen(name, "wt");
    if (NULL != fp)
    {
        for (i=0; i < n; ++i)
        {
            fprintf(fp, "%f\n", x[i]);
        }
    }
    fclose(fp);
}

void dump_int(const char *name, int *x, int n)
{
    int i;

    printf("%s:\n", name);
    for (i=0; i < n; ++i)
    {
        printf("%d\n", x[i]);
    }
    printf("\n");
}
#endif

float dsp_lib_mean(float *x, int n)
{
    return stat_mean(x, n);
}

float dsp_lib_median(float *x, int n)
{
    return stat_median(x, n);
}

float dsp_lib_mean_abs(float *x, int n)
{
    return stat_mean_abs(x, n);
}

float dsp_lib_rms(float *x, int n)
{
    return stat_rms(x, n);
}

float dsp_lib_relvar(float *x, int n)
{
    return stat_relvar(x, n);
}

float dsp_lib_mad(float *x, int n)
{
    return stat_mad(x, n);
}

float dsp_lib_entropy(float *x, int n)
{
    return stat_entropy(x, n);
}

void dsp_lib_min(float *x, int n, float *v_min, int *i_min)
{
    stat_min(x, n, i_min);

    *v_min = x[*i_min];
}

void dsp_lib_max(float *x, int n, float *v_max, int *i_max)
{
    stat_max(x, n, i_max);

    *v_max = x[*i_max];
}

void dsp_lib_ma(float *x, float *y, int n, int m)
{
    int i;
    stat_ma_t ma;
    float buffer[m];

    ma_init(&ma, buffer, m);

    for (i=0; i < n; ++i)
    {
        y[i] = ma_calc(&ma, x[i]);
    }
}

void dsp_lib_linreg(int x1, int x2, float *data, int *x, float *y, int n)
{
    int i;
    stat_linreg_t linreg;

    stat_linreg_fit(x1, x2, data, &linreg);

    for (i=0; i < n; ++i)
    {
        y[i] = stat_linreg_calc(x[i], &linreg);
    }
}

void dsp_lib_ema(float *x, float *y, int n, float k)
{
    int i;
    stat_ema_t ema;

    ema_init(&ema, k);

    for (i=0; i < n; ++i)
    {
        y[i] = ema_calc(&ema, x[i]);	
    }
}

void dsp_lib_rfft(float *x, float *y_re, float *y_im, int n)
{
    int32_t i, m = n/2 + 1;
    cplx_t y[m];

    fft_real(x, y, n);

    for (i=0; i < m; ++i)
    {
        y_re[i] = y[i].re;
        y_im[i] = y[i].im;
    }
}

void dsp_lib_irfft(float *x_re, float *x_im, float *y, int n)
{
    int32_t i, m = n/2 + 1;
    cplx_t x[m];

    for (i=0; i < m; ++i)
    {
        x[i].re = x_re[i],
        x[i].im = x_im[i];
    }

    ifft_real(x, y, n);
}

int dsp_lib_bin2freq(int bin, int n, int fs)
{
    return fft_bin2freq(bin, n, fs);
}

int dsp_lib_freq2bin(int f, int n, int fs)
{
    return fft_freq2bin(f, n, fs);
}

void dsp_lib_acorr_direct(float *x, float *r, int n, int m)
{
    acorr_norm(x, r, n, m);
}

void dsp_lib_acorr_fft(float *x, float *r, int n)
{
    acorr_fft(x, r, n);
}

void dsp_lib_lpc(float *x, float *a, float *e, int n, int m)
{
    float r[m + 1];

    lpc_synthesis(x, r, a, e, n, m);
}

void dsp_lib_lpc_freq(float *x, float *y, int n, int m)
{
    float r[m + 1];
    float a[m + 1];
    float e = 0.0f;
    
    lpc_synthesis(x, r, a, &e, n, m);

    lpc_freq_response(x, y, e, a, n, m);
}

void dsp_lib_lpc_prediction(float *x, float *y, int n, int m)
{
    float r[m + 1];
    float a[m + 1];
    float e = 0.0f;

    lpc_synthesis(x, r, a, &e, n, m);

    lpc_prediction(x, y, a, e, n, m);
}

void dsp_lib_lpcc(float *a, float *c, float g, int m)
{
    lpc2cep(a, c, g, m);
}

void dsp_lib_trifb_lin(int m, int n, float *c)
{
    int centers[m];

    trifb_linear(centers, m, n);

    trifb_coeffs(centers, m, n, c);
}

void dsp_lib_trifb_mel(int m, int n, int fs, float *c)
{
    int centers[m];

    trifb_mel(centers, m, n, fs);

    trifb_coeffs(centers, m, n, c);
}

extern const flt32_t mlp_lut_test_w0[MLP_LUT_TEST_INPUTS0 * MLP_LUT_TEST_PERCEPTRONS0];
extern const flt32_t mlp_lut_test_b0[MLP_LUT_TEST_PERCEPTRONS0];
extern const flt32_t mlp_lut_test_w1[MLP_LUT_TEST_INPUTS1 * MLP_LUT_TEST_PERCEPTRONS1];
extern const flt32_t mlp_lut_test_b1[MLP_LUT_TEST_PERCEPTRONS1];

float dsp_lib_mlp(float *x)
{
    float tmp[MLP_LUT_TEST_PERCEPTRONS0], y = 0.0f;

    ann_feedforward(mlp_lut_test_w0, mlp_lut_test_b0, x, tmp, MLP_LUT_TEST_INPUTS0, MLP_LUT_TEST_PERCEPTRONS0, act_logsig);

    ann_feedforward(mlp_lut_test_w1, mlp_lut_test_b1, tmp, &y, MLP_LUT_TEST_INPUTS1, MLP_LUT_TEST_PERCEPTRONS1, act_logsig);

    return y;
}

extern const flt32_t dt_lut_test_cut_point[DT_LUT_TEST_CAPACITY];
extern const int32_t dt_lut_test_cut_var[DT_LUT_TEST_CAPACITY];
extern const dt_node_t dt_lut_test_children[DT_LUT_TEST_CAPACITY];
extern const int32_t dt_lut_test_class[DT_LUT_TEST_CAPACITY];

int dsp_lib_decision_tree(float *x)
{
    dt_handle_t hnd;

    dt_init(&hnd, dt_lut_test_cut_var, dt_lut_test_cut_point, dt_lut_test_children, dt_lut_test_class, DT_LUT_TEST_CAPACITY);

    return dt_process(&hnd, x);
}

extern const flt32_t svm_lut_test_lin_sv[SVM_LUT_TEST_LIN_INPUTS * SVM_LUT_TEST_LIN_VECTORS];
extern const flt32_t svm_lut_test_lin_alpha[SVM_LUT_TEST_LIN_VECTORS];
extern const flt32_t svm_lut_test_lin_bias;
extern const flt32_t svm_lut_test_lin_kernel_arg[];

float dsp_lib_svm_lin(float *x)
{
    svm_t hnd;

    svm_init(&hnd, svm_lut_test_lin_alpha, svm_lut_test_lin_sv, svm_lut_test_lin_bias, SVM_LUT_TEST_LIN_INPUTS, SVM_LUT_TEST_LIN_VECTORS, svm_lin, svm_lut_test_lin_kernel_arg);

    return svm_classify(&hnd, x);
}

extern const flt32_t svm_lut_test_rbf_sv[SVM_LUT_TEST_RBF_INPUTS * SVM_LUT_TEST_RBF_VECTORS];
extern const flt32_t svm_lut_test_rbf_alpha[SVM_LUT_TEST_RBF_VECTORS];
extern const flt32_t svm_lut_test_rbf_bias;
extern const flt32_t svm_lut_test_rbf_kernel_arg[];

float dsp_lib_svm_rbf(float *x)
{
    svm_t hnd;
    
    svm_init(&hnd, svm_lut_test_rbf_alpha, svm_lut_test_rbf_sv, svm_lut_test_rbf_bias, SVM_LUT_TEST_RBF_INPUTS, SVM_LUT_TEST_RBF_VECTORS, svm_rbf, svm_lut_test_rbf_kernel_arg);

    return svm_classify(&hnd, x);
}

extern const flt32_t gmm_lut_test_covariances[GMM_LUT_TEST_INPUTS * GMM_LUT_TEST_MIXTURES];
extern const flt32_t gmm_lut_test_means[GMM_LUT_TEST_INPUTS * GMM_LUT_TEST_MIXTURES];
extern const flt32_t gmm_lut_test_weights[GMM_LUT_TEST_MIXTURES];

float dsp_lib_gmm(float *x)
{
    gmm_t hnd;
    float logw[GMM_LUT_TEST_MIXTURES], c[GMM_LUT_TEST_MIXTURES], post[GMM_LUT_TEST_MIXTURES];

    gmm_init(&hnd, gmm_lut_test_means, gmm_lut_test_covariances, gmm_lut_test_weights, logw, c, GMM_LUT_TEST_INPUTS, GMM_LUT_TEST_MIXTURES);

    return gmm_calc(&hnd, x, post);
}

extern const flt32_t fir_lut_decimator_test[FIR_LUT_DECIMATOR_TEST_LEN];

void dsp_lib_fir_stream(float *x, float *y, int n)
{
    fir_t hnd;
    float buffer[FIR_LUT_DECIMATOR_TEST_LEN];
    int i;
    
    fir_init(&hnd, buffer, fir_lut_decimator_test, FIR_LUT_DECIMATOR_TEST_LEN);
    
    for (i=0; i < n; ++i)
    {
        y[i] = fir_calc(&hnd, x[i]);
    }
}

void dsp_lib_fir_buffer(float *x, float *y, int n)
{
    fir_calc_buffer(x, y, fir_lut_decimator_test, FIR_LUT_DECIMATOR_TEST_LEN, n);
}

extern const int16_t fir_lut_decimator_16_test[FIR_LUT_DECIMATOR_16_TEST_LEN];

void dsp_lib_fir_stream_fix16(int16_t *x, int16_t *y, int n)
{
    fir16_t hnd;
    int16_t buffer[FIR_LUT_DECIMATOR_16_TEST_LEN];
    int i;

    fir16_init(&hnd, buffer, fir_lut_decimator_16_test, FIR_LUT_DECIMATOR_16_TEST_LEN);

    for (i=0; i < n; ++i)
    {
        y[i] = fir16_calc(&hnd, x[i]);
    }
}

void dsp_lib_fir_buffer_fix16(int16_t *x, int16_t *y, int n)
{
    fir16_calc_buffer(x, y, fir_lut_decimator_16_test, FIR_LUT_DECIMATOR_16_TEST_LEN, n);
}

extern const flt32_t biquad_lut_test[BIQUAD_LUT_TEST_COEFFS * BIQUAD_LUT_TEST_STAGES];

void dsp_lib_biquad(float *x, float *y, int n)
{
    biquad_t hnd;
    float s[BIQUAD_STATES * BIQUAD_LUT_TEST_STAGES];
    int i;

    biquad_init(&hnd, biquad_lut_test, s, BIQUAD_LUT_TEST_COEFFS, BIQUAD_LUT_TEST_STAGES);

    for (i=0; i < n; ++i)
    {
        y[i] = biquad_calc(&hnd, x[i]);
    }
}

void dsp_lib_goertzel(float *x, float *y, int f, int fs, int n)
{
    cplx_t xc;

    goertzl(x, &xc, f, fs, n);

    fft_magnitude(&xc, y, n, 4.0f);
}

void dsp_lib_cepstrum(float *x, float *y, int n)
{
    cepstrum(x, y, n);
}

void dsp_lib_window_hann(float *x, int n)
{
    win_hann(x, n);
}

void dsp_lib_window_hamming(float *x, int n)
{
    win_hamming(x, n);
}

void dsp_lib_window_bartlett(float *x, int n)
{
    win_bartlett(x, n);
}

static float *decim_ptr = NULL;

static void decim_proc(flt32_t value)
{
    *decim_ptr++ = value;
}

void dsp_lib_decimator1(float *x, float *y, int n)
{
    int i;
    decim_t hnd;
    float buffer[FIR_LUT_DECIMATOR_TEST_LEN];

    decim_ptr = y;

    decim_init(&hnd, buffer, fir_lut_decimator_test, FIR_LUT_DECIMATOR_TEST_LEN, 4, decim_proc);

    for (i=0; i < n; ++i)
    {
        decim_calc(&hnd, x[i]);
    }
}

static int16_t *decim16_ptr = NULL;

static void decim16_proc(int16_t value)
{
    *decim16_ptr++ = value;
}

void dsp_lib_decimator2(int16_t *x, int16_t *y, int n)
{
    int i;
    decim16_t hnd;
    int16_t buffer[FIR_LUT_DECIMATOR_16_TEST_LEN];

    decim16_ptr = y;

    decim16_init(&hnd, buffer, fir_lut_decimator_16_test, FIR_LUT_DECIMATOR_16_TEST_LEN, 4, decim16_proc);

    for (i=0; i < n; ++i)
    {
        decim16_calc(&hnd, x[i]);
    }
}

void dsp_lib_decimator3(float *x, float *y, int n)
{
    decim_calc_buffer(x, y, fir_lut_decimator_test, FIR_LUT_DECIMATOR_TEST_LEN, n, 4);
}

void dsp_lib_decimator4(int16_t *x, int16_t *y, int n)
{
    decim16_calc_buffer(x, y, fir_lut_decimator_16_test, FIR_LUT_DECIMATOR_16_TEST_LEN, n, 4);
}

void dsp_lib_dct_fft(float *x, float *y, int n)
{
    dct_fft(x, y, n);
}

void dsp_lib_dct_direct(float *x, float *y, int n)
{
    dct_norm(x, y, n);
}

extern const flt32_t dct_lut_test[DCT_LUT_TEST_LEN * DCT_LUT_TEST_LEN];

void dsp_lib_dct_fast(float *x, float *y)
{
    dct_fast(x, y, DCT_LUT_TEST_K1, DCT_LUT_TEST_K2, dct_lut_test, DCT_LUT_TEST_LEN);
}

void dsp_lib_lms(float *x, float *y, float *d, float *c, int n, int m, float k)
{
    int i;
    lms_t hnd;
    float buffer[m];

    lms_init(&hnd, buffer, c, m, k);

    for (i=0; i < n; ++i)
    {
        y[i] = lms_calc(&hnd, x[i], d[i]);
    }
}

void dsp_lib_hilbert(float *x, float *y, int n)
{
    hilbert(x, y, n);
}

void dsp_lib_spline_fit(float *x, float *a, float *b, float *c, float *d, int n)
{
    spline_t hnd;
    flt32_t h[n+1];
    flt32_t mu[n+1];
    flt32_t z[n+1];

    spline_init(&hnd, h, mu, z, b, c, d);

    spline_fit(&hnd, x, a, n);
}

void dsp_lib_spline_interpolate(float *xt, float *yt, int nt, float *x, float *y, int n)
{
    spline_t hnd;
    flt32_t h[nt+1];
    flt32_t mu[nt+1];
    flt32_t z[nt+1];
    flt32_t b[nt+1];
    flt32_t c[nt+1];
    flt32_t d[nt+1];

    spline_init(&hnd, h, mu, z, b, c, d);

    spline_fit(&hnd, xt, yt, nt);

    spline_interpolate(&hnd, x, y, n);
}

void dsp_lib_peak_find_all(float *y, int *xmin, int *nmin, int *xmax, int *nmax, int n)
{
    peak_find_all(y, xmin, nmin, xmax, nmax, n);
}

void dsp_lib_peak_find(float *x, int n, int m, int bw, int *px, float *py)
{
    int32_t i;
    peak_t pk[m];

    peak_find_n(x, n, m, bw, pk);

    for (i=0; i < m; ++i)
    {
        px[i] = pk[i].pos;
        py[i] = pk[i].val;
    }
}

extern const flt32_t wavelet_lut_test_db8_dec_lo[WAVELET_LUT_TEST_DB8_LEN];
extern const flt32_t wavelet_lut_test_db8_dec_hi[WAVELET_LUT_TEST_DB8_LEN];

void dsp_lib_dwt_periodic(float *x, float *y, int n)
{
    dwt_periodic(x, y, n, wavelet_lut_test_db8_dec_lo, wavelet_lut_test_db8_dec_hi, WAVELET_LUT_TEST_DB8_LEN);
}

extern const flt32_t wavelet_lut_test_db8_rec_lo_inv[WAVELET_LUT_TEST_DB8_LEN];
extern const flt32_t wavelet_lut_test_db8_rec_hi_inv[WAVELET_LUT_TEST_DB8_LEN];

void dsp_lib_idwt_periodic(float *x, float *y, int n)
{
    idwt_periodic(x, y, n, wavelet_lut_test_db8_rec_lo_inv, wavelet_lut_test_db8_rec_hi_inv, WAVELET_LUT_TEST_DB8_LEN);
}

float dsp_lib_dtw(float *x, int nx, float *y, int ny)
{
    flt32_t dtw[nx * ny];

    dtw_calc(x, y, nx, ny, dtw);

    return dtw_backtrace(x, y, nx, ny, dtw);
}

const flt32_t hmm_lut_test_a[HMM_LUT_TEST_N * HMM_LUT_TEST_N];
const flt32_t hmm_lut_test_b[HMM_LUT_TEST_N * HMM_LUT_TEST_M];
const flt32_t hmm_lut_test_pi[HMM_LUT_TEST_N];

float dsp_lib_hmm_viterbi(int *o, int T)
{
    hmm_t hmm;
    flt32_t delta[T * HMM_LUT_TEST_N];

    hmm_init(&hmm, hmm_lut_test_a, hmm_lut_test_b, hmm_lut_test_pi, HMM_LUT_TEST_N, HMM_LUT_TEST_M);

    return hmm_viterbi(&hmm, delta, o, T);
}

void dsp_lib_preemph(float *x, int n, float k)
{
    preemph_calc(x, n, k);
}

void dsp_lib_preemph_dynamic(float *x, int n)
{
    preemph_calc_dynamic(x, n);
}

void dsp_lib_lfsc(float *x, float *y, int n, int m)
{
    m += 2;

    int32_t centers[m];

    lfxc_init(centers, m, n);

    lfsc_calc(centers, m, x, y);
}

void dsp_lib_lfcc(float *x, float *y, int n, int m)
{
    m += 2;

    int32_t centers[m];

    lfxc_init(centers, m, n);

    lfcc_calc(centers, m, x, y);
}

void dsp_lib_mfsc(float *x, float *y, int n, int m, int fs)
{
    m += 2;

    int32_t centers[m];

    mfxc_init(centers, m, n, fs);

    mfsc_calc(centers, m, x, y);
}

void dsp_lib_mfcc(float *x, float *y, int n, int m, int fs)
{
    m += 2;

    int32_t centers[m];

    mfxc_init(centers, m, n, fs);

    mfcc_calc(centers, m, x, y);
}

void dsp_lib_ssub(float *noise, int n_noise, float *signal, int n_signal, int nfft, float k, float alpha, float beta, int onset, float threshold)
{
    int32_t i;
    flt32_t buffer[nfft];
    ssub_t hnd;

    ssub_init(&hnd, buffer, nfft, alpha, beta, k, onset, threshold);

    for (i=0; i < n_noise; i += nfft)
    {
        ssub_noise_update(&hnd, &noise[i]);
    }

    for (i=0; i < n_signal; i += nfft)
    {
        ssub_process(&hnd, &signal[i]);
    }
}

void dsp_lib_edge_detector(float *x, float *y, int n, float threshold, int onset, int n_skip)
{
    int32_t i, j = 0, state = 0;
    edge_detector_t hnd;

    ed_init(&hnd, threshold, onset);
    ed_rearm(&hnd);

    for (i=0; i < n; ++i)
    {
        ed_process(&hnd, x[i], 1);

        y[i] = 0.0f;

        switch (state)
        {
            case 0:
                if (ed_done == ed_state(&hnd))
                {
                    state = 1;
                }
                break;

            case 1:
                y[i] = 1.0f;
                j = 0;
                state = 2;
                break;

            case 2:
                j++;
                if (j >= n_skip)
                {
                    ed_rearm(&hnd);
                    state = 0;
                }
                break;
        }
    }
}

extern const flt32_t gru_lut_0_test_wz[GRU_LUT_0_TEST_INPUTS * GRU_LUT_0_TEST_UNITS];
extern const flt32_t gru_lut_0_test_wr[GRU_LUT_0_TEST_INPUTS * GRU_LUT_0_TEST_UNITS];
extern const flt32_t gru_lut_0_test_wh[GRU_LUT_0_TEST_INPUTS * GRU_LUT_0_TEST_UNITS];

extern const flt32_t gru_lut_0_test_uz[GRU_LUT_0_TEST_UNITS * GRU_LUT_0_TEST_UNITS];
extern const flt32_t gru_lut_0_test_ur[GRU_LUT_0_TEST_UNITS * GRU_LUT_0_TEST_UNITS];
extern const flt32_t gru_lut_0_test_uh[GRU_LUT_0_TEST_UNITS * GRU_LUT_0_TEST_UNITS];

extern const flt32_t gru_lut_0_test_bz[GRU_LUT_0_TEST_UNITS];
extern const flt32_t gru_lut_0_test_br[GRU_LUT_0_TEST_UNITS];
extern const flt32_t gru_lut_0_test_bh[GRU_LUT_0_TEST_UNITS];

extern const flt32_t gru_lut_1_test_wz[GRU_LUT_1_TEST_INPUTS * GRU_LUT_1_TEST_UNITS];
extern const flt32_t gru_lut_1_test_wr[GRU_LUT_1_TEST_INPUTS * GRU_LUT_1_TEST_UNITS];
extern const flt32_t gru_lut_1_test_wh[GRU_LUT_1_TEST_INPUTS * GRU_LUT_1_TEST_UNITS];

extern const flt32_t gru_lut_1_test_uz[GRU_LUT_1_TEST_UNITS * GRU_LUT_1_TEST_UNITS];
extern const flt32_t gru_lut_1_test_ur[GRU_LUT_1_TEST_UNITS * GRU_LUT_1_TEST_UNITS];
extern const flt32_t gru_lut_1_test_uh[GRU_LUT_1_TEST_UNITS * GRU_LUT_1_TEST_UNITS];

extern const flt32_t gru_lut_1_test_bz[GRU_LUT_1_TEST_UNITS];
extern const flt32_t gru_lut_1_test_br[GRU_LUT_1_TEST_UNITS];
extern const flt32_t gru_lut_1_test_bh[GRU_LUT_1_TEST_UNITS];

extern const flt32_t dense_lut_2_test_w[DENSE_LUT_2_TEST_INPUTS * DENSE_LUT_2_TEST_UNITS];
extern const flt32_t dense_lut_2_test_b[DENSE_LUT_2_TEST_UNITS];

void dsp_lib_keras_test(float *x, float *y, int n, int m)
{
    flt32_t h[GRU_LUT_0_TEST_UNITS] = {0.0f};
    flt32_t h1[GRU_LUT_1_TEST_UNITS] = {0.0f};

    for (int32_t i=0; i < n; ++i)       // number of batches
    {
        for (int32_t j=0; j < m; ++j)   // batch 
        {
            keras_gru(&x[j + i * m], h,
                gru_lut_0_test_wz,
                gru_lut_0_test_uz,
                gru_lut_0_test_bz,
                gru_lut_0_test_wr,
                gru_lut_0_test_ur,
                gru_lut_0_test_br,
                gru_lut_0_test_wh,
                gru_lut_0_test_uh,
                gru_lut_0_test_bh,
                GRU_LUT_0_TEST_INPUTS,
                GRU_LUT_0_TEST_UNITS);

            keras_gru(h, h1,
                gru_lut_1_test_wz,
                gru_lut_1_test_uz,
                gru_lut_1_test_bz,
                gru_lut_1_test_wr,
                gru_lut_1_test_ur,
                gru_lut_1_test_br,
                gru_lut_1_test_wh,
                gru_lut_1_test_uh,
                gru_lut_1_test_bh,
                GRU_LUT_1_TEST_INPUTS,
                GRU_LUT_1_TEST_UNITS);

            keras_dense(h1, &y[i],
                dense_lut_2_test_w,
                dense_lut_2_test_b,
                DENSE_LUT_2_TEST_INPUTS,
                DENSE_LUT_2_TEST_UNITS);
        }

        keras_gru_reset(h, GRU_LUT_0_TEST_UNITS);
        keras_gru_reset(h1, GRU_LUT_1_TEST_UNITS);
    }
}
