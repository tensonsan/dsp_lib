
/**********************************************************************
 *
 * @file    
 * @author  G.K.
 * @version 1.0
 * @brief   Linear Predictive Coding API
 *
 **********************************************************************/
 
/**
 * @addtogroup LPC Linear Predictive Coding
 * @{
 */
 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "lpc.h"
#include "fft.h"
#include "acorr.h"
#include "window.h"
#include "fir.h"

/**
 * @brief Calculates LPC coefficients using the Levinson-Durbin recursion
 *
 * @param[in] r Array of autocorrelation coefficients (capacity \p m + 1)
 * @param[out] a Array of LPC coefficients (capacity \p m + 1)
 * @param[out] e LPC Residual error (\f$ e = G^2 \f$)
 * @param[in] m Length of \p a
 */
void lpc_levinson_durbin(flt32_t *r, flt32_t *a, flt32_t *e, int32_t m)
{
    int32_t i, j, n;
    flt32_t et, k, tmp;

    // Initialize LPC coefficients
    memset(a, 0, (m + 1) * sizeof(flt32_t));
    a[0] = 1.0f;

    // Initialize error value
    et = r[0];

    // Levinson-Durbin recursion
    for (i=0; i < m; ++i)
    {
        // Calculate k
        k = 0.0f;
        for (j=0; j <= i; ++j)
        {
            k -= a[j] * r[i + 1 - j];
        }
        k /= et;
        
        // Update LPC coefficients
        n = (i + 1) / 2;

        for (j=0; j <= n; ++j)
        {
            tmp = a[i + 1 - j] + k * a[j];
            a[j] += k * a[i + 1 - j];
            a[i + 1 - j] = tmp;
        }

        // Update error value
        et *= 1.0f - k * k;
    }
    *e = et;
}

/**
 * @brief Calculates the frequency response \f$ y \f$ of the LPC filter using the FFT
 *
 * \f$
 * y = \left|
 *  \frac{ \sqrt{e}} { 1 - \sum\limits_{k=1}^{m}{a_k \exp (-j \omega k) }}
 * \right|
 * \f$
 *
 * @param[in] x Input signal (capacity \p n + 2)
 * @param[out] y Magnitude (capacity \p n + 2)
 * @param[in] e LPC residual error
 * @param[in] a Array of LPC coefficients (capacity \p m + 1)
 * @param[in] n Length of \p x and \p y (must be power of 2) 
 * @param[in] m Length of \p a
 */
void lpc_freq_response(flt32_t *x, flt32_t *y, flt32_t e, flt32_t *a, int32_t n, int32_t m)
{
    int32_t i;
    cplx_t *yt = (cplx_t *)y;

    // Store coefficients to the FFT input buffer and add trailing zeros
    memset(x, 0, n * sizeof(flt32_t));
    memcpy(x, a, (m + 1) * sizeof(flt32_t));

    // Real FFT
    fft_real(x, yt, n);

    // Magnitude of the real FFT
    fft_magnitude(yt, x, n, 1.0f);

    // Frequency response of LPC filter
    e = sqrtf(e);   // e = G^2

    for (i=0; i < n; ++i)
    {
        y[i] = e / x[i];
    }
}

/**
 * @brief Calculates LPC coefficients from the input signal
 *
 * @param[in] x Input signal
 * @param[out] r Array of autocorrelation coefficients (capacity \p m + 1)
 * @param[out] a Array of LPC coefficients (capacity \p m + 1)
 * @param[out] e LPC residual error
 * @param[in] n Length of \p x in samples
 * @param[in] m Length of \p r and \p a
 */
void lpc_synthesis(flt32_t *x, flt32_t *r, flt32_t *a, flt32_t *e, int32_t n, int32_t m)
{
    flt32_t et;

    // Autocorrelation
    acorr_norm(x, r, n, m);

    // LPC coefficients
    lpc_levinson_durbin(r, a, &et, m);

    // Normalize LPC residual error
    *e = et / (flt32_t)n;
}

/**
 * @brief Calculates the linear prediction of the input signal \p x
 *
 * @param[in] x Input signal (capacity \p n)
 * @param[out] y Output signal (capacity \p n)
 * @param[in] a LPC coefficients (capacity \p m + 1)
 * @param[out] e LPC residual error
 * @param[in] n Length of \p x and \p y in samples
 * @param[in] m Number of LPC coefficients
 */
void lpc_prediction(flt32_t *x, flt32_t *y, flt32_t *a, flt32_t e, int32_t n, int32_t m)
{
    int32_t i;

    a[0] = e;
    for (i=1; i < m; ++i)
    {
        a[i] = -a[i];
    }

    fir_calc_buffer(x, y, a, m, n);
}

/**
 * @brief Converts LPC coefficients to LPC cepstral coefficients
 *
 * @param[in] a Array of LPC coefficients (capacity \p m + 1)
 * @param[out] c Array of LPC cepstral coefficients (capacity \p m + 1)
 * @param[in] e LPC residual error
 * @param[in] m Number of LPC coefficients
 */
void lpc2cep(flt32_t *a, flt32_t *c, flt32_t e, int32_t m)
{
    int32_t i, j;
    flt32_t sum;

    c[0] = logf(e);

    for (i=1; i <= m; ++i)
    {
        flt32_t c1 = 0.0f, y, t;
        sum = 0.0f;

        for (j=1; j < i; ++j)
        {
            // sum += (j - i) * a[j] * c[i - j];
            y = (j - i) * a[j] * c[i - j] - c1;
            t = sum + y;
            c1 = (t - sum) - y;
            sum = t;
        }

        c[i] = sum / (flt32_t)i - a[i];
    }
}

/**
 * @}
 */
