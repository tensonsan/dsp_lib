/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Natural Cubic Spline Interpolation API
 *
 **********************************************************************/

/**
 * @addtogroup Spline Natural Cubic Spline Interpolation
 * @{
 */

#include "spline.h"

/**
 * @brief Initializes natural cubic spline \f$ y = a + bx + cx^2 + dx^3 \f$
 * 
 * @param[in] hnd Natural cubic spline handle
 * @param[in] h Difference array (capacity \p n)
 * @param[in] mu Superdiagonal array (capacity \p n)
 * @param[in] z Subdiagonal array (capacity \p n)
 * 
 * @param[in] b Spline coefficients (capacity \p n)
 * @param[in] c Spline coefficients (capacity \p n)
 * @param[in] d Spline coefficients (capacity \p n)
 */
void spline_init(spline_t *hnd, flt32_t *h, flt32_t *mu, flt32_t *z, flt32_t *b, flt32_t *c, flt32_t *d)
{
    hnd->h = h;
    hnd->mu = mu;
    hnd->z = z;

    hnd->b = b;
    hnd->c = c;
    hnd->d = d;
}

/**
 * @brief Calculates natural cubic spline parameters
 * 
 * @param[in] hnd Natural cubic spline handle
 * @param[in] x Spline X points (capacity \p n)
 * @param[in] y Spline Y points (capacity \p n)
 * @param[in] n Number of spline points - 1
 */
void spline_fit(spline_t *hnd, flt32_t *x, flt32_t *y, int32_t n)
{
    int32_t i;
    flt32_t l, alpha;

    hnd->x = x;
    hnd->a = y;
    hnd->n = n;

    for (i=0; i < hnd->n; ++i)
    {
        hnd->h[i] = x[i+1] - x[i];
    }

    hnd->mu[0] = 0.0f;
    hnd->z[0] = 0.0f;

    for (i=1; i < hnd->n; ++i)
    {
        alpha = 3.0f * (y[i+1] - y[i]) / hnd->h[i] - 3.0f * (y[i] - y[i-1]) / hnd->h[i-1];
        l = 2.0f * (x[i+1] - x[i-1]) - hnd->h[i-1] * hnd->mu[i-1];

        hnd->mu[i] = hnd->h[i] / l;
        hnd->z[i] = (alpha - hnd->h[i-1] * hnd->z[i-1]) / l;
    }

    hnd->z[hnd->n] = 0.0f;
    hnd->c[hnd->n] = 0.0f;

    for (i=hnd->n - 1; i >= 0; --i)
    {
        hnd->c[i] = hnd->z[i] - hnd->mu[i] * hnd->c[i+1];
        hnd->b[i] = (hnd->a[i+1] - hnd->a[i]) / hnd->h[i] - hnd->h[i] * (hnd->c[i+1] + 2.0f * hnd->c[i]) / 3.0f;
        hnd->d[i] = (hnd->c[i+1] - hnd->c[i]) / (3.0f * hnd->h[i]);
    }
}

/**
 * @brief Interpolate using natural cubic spline
 * 
 * @param[in] hnd Natural cubic spline handle
 * @param[in] x X values
 * @param[out] y interpolated values
 * @param[in] n Length of \p x and \p y in samples
 */
void spline_interpolate(spline_t *hnd, flt32_t *x, flt32_t *y, int32_t n)
{
    int32_t i, j, n1 = hnd->n - 1;
    flt32_t xt;

    for (i=0; i < n; ++i)
    {
        if (x[i] < hnd->x[0])
        {
            // Beginning of the x interval
            xt = x[i] - hnd->x[0];
            y[i] = hnd->a[0] + hnd->b[0] * xt + hnd->c[0] * xt * xt + hnd->d[0] * xt * xt * xt;
        }
        else if (x[i] > hnd->x[n1])
        {
            // End of the x interval
            xt = x[i] - hnd->x[n1];
            y[i] = hnd->a[n1] + hnd->b[n1] * xt + hnd->c[n1] * xt * xt + hnd->d[n1] * xt * xt * xt;
        }
        else
        {
            for (j=0; j < hnd->n; ++j)
            {
                if ((x[i] >= hnd->x[j]) && (x[i] <= hnd->x[j+1]))
                {
                    flt32_t xt = x[i] - hnd->x[j];
                    y[i] = hnd->a[j] + hnd->b[j] * xt + hnd->c[j] * xt * xt + hnd->d[j] * xt * xt * xt;
                    break;
                }
            }
        }
    }
}

/**
 * @}
 */
