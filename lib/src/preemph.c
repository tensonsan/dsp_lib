
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Pre-emphasis Filter API
 *
 **********************************************************************/
 
/**
 * @addtogroup Preemph Preemphasis Filter
 * @{
 */

#include "preemph.h"
#include "acorr.h"

/**
 * @brief Performs in-place pre-emphasis filtering \f$ y_n = x_n - kx_{n-1} \f$
 *
 * @param[in,out] x Input signal, filtered signal
 * @param[in] n Length of \p x in samples
 * @param[in] k Pre-emphasis coefficient in range [0, 1]
 */
void preemph_calc(flt32_t *x, int32_t n, flt32_t k)
{
    int32_t i;
    flt32_t y, xp = 0.0f;

    // Pre-emphasis filtering
    for (i=0; i < n; ++i)
    {
        y = x[i] - k * xp;
        xp = x[i];
        x[i] = y;
    }
}

/**
 * @brief Performs in-place dynamic pre-emphasis filtering \f$ y_n = x_n - kx_{n-1} \f$
 * 
 * \note Coefficient \f$ k \f$ is a quotient of the first two autocorrelation coefficients \f$ k = \frac{r_1}{r_0} \f$
 *
 * @param[in,out] x Input signal, filtered signal
 * @param[in] n Length of \p x in samples
 */
void preemph_calc_dynamic(flt32_t *x, int32_t n)
{
    flt32_t k, r[2];

    // Optimal pre-emphasis filter coefficient
    acorr_norm(x, r, n, 1);

    k = r[1] / r[0];

    preemph_calc(x, n, k);
}

/**
 * @}
 */
