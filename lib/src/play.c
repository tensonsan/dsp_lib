
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Playback control API
 *
 **********************************************************************/

/**
 * @addtogroup Playback Playback control
 * @{
 */
#include <stdlib.h>
#include "play.h"

/**
 * @brief Audio playback processing function
 *
 * @param[in] play Playback handle
 * @param[out] value playback sample value
 */
void play_process(play_t *play, int16_t *value)
{
    if (play->active == 1)
    {
        // Stop processing if the buffer not assigned
        if ((NULL == play->data) || (0 == play->length))
        {
            play->active = 0;
            return;
        }

        // Play until the end of the audio stream
        *value = play->data[play->index++];
        if (play->index >= play->length)
        {
            // End of the steam reached, disable the playback
            play->active = 0;
        }
    }
}

/**
 * @brief Audio playback initialization function
 *
 * @note The playback must not be active when calling this function
 *
 * @param[in] play Playback handle
 * @param[in] data Playback sample buffer
 * @param[in] length Playback sample buffer length in samples
 *
 * @retval 0 playback not initialized (playback in progress)
 * @retval 1 playback initialized
 */
int32_t play_init(play_t *play, int16_t *data, int32_t length)
{
    play->data = data;
    play->capacity = length;
    play->length = 0;
    play->index = 0;
    play->active = 0;

    return 1;
}

/**
 * @brief Controls the audio playback
 *
 * @param[in] play Playback handle
 * @param[in] offset Playback data offset
 * @param[in] length Playback data length
 * @param[in] active 1 = playback active, 0 = playback not active
 *
 * @retval 0 playback control failed
 * @retval 1 playback control successful
 */
int32_t play_control(play_t *play, int32_t offset, int32_t length, int32_t active)
{
    int32_t rv = 0;

    // Play an entire playback buffer?
    if (PLAY_LENGTH_ALL == length)
    {
        // Playback segment within the playback buffer?
        if (offset < play->capacity)
        {
            play->active = active;
            play->index = offset;
            play->length = play->capacity;
            
            rv = 1;
        }
    }
    else 
    {
        // Playback segment within the playback buffer?
        if ((offset + length) < play->capacity)
        {
            play->active = active;
            play->index = offset;
            play->length = length;
        
            rv = 1;
        }
    }
    
    return rv;
}

/**
 * @brief Function returns the audio playback state
 *
 * @param[in] play Playback handle
 *
 * @retval 0 playback not active (idle)
 * @retval 1 playback active
 */
int32_t play_active(play_t *play)
{
    return play->active;
}

/**
 * @}
 */
