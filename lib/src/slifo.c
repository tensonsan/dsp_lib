
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Sample LIFO API
 *
 **********************************************************************/

 /**
  * @addtogroup SLIFO Sample LIFO
  * @{
  */

#include "slifo.h"
#include "utils.h"

/**
 * @brief Clears the sample LIFO
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 *
 */
void slifo_flush(slifo_t *hnd)
{
    hnd->index_copy = 0;
    hnd->index = 0;
    hnd->full = 0;
}

/**
 * @brief Initializes the LIFO
 *
 * @param[in] hnd Pointer to the LIFO handle
 * @param[in] buffer Sample buffer
 * @param[in] length Sample bufer length
 *
 */
void slifo_init(slifo_t *hnd, flt32_t *buffer, int32_t length)
{
    hnd->buffer = buffer;
    hnd->length = length;

    slifo_flush(hnd);
}

/**
 * @brief Writes a sample to the sample LIFO
 *
 * @param[in] hnd Pointer to sample LIFO handle
 * @param[in] val Sample value
 *
 */
void slifo_write(slifo_t *hnd, flt32_t val)
{
    hnd->buffer[hnd->index++] = val;
    if (hnd->index >= hnd->length)
    {
        hnd->index = 0;
        hnd->full = 1; 
    }
}

/**
 * @brief Copies the current LIFO index into a steady copy
 *
 * @note Must be called before any sfifo_read
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 *
 */
void slifo_freeze(slifo_t *hnd)
{
    hnd->index_copy = hnd->index;
}

/**
 * @brief Reads the sample values from the sample LIFO into array
 *
 * @note sfifo_freeze must be called before calling this function!
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 * @param[out] buffer Output array
 * @param[in] length Length of output array
 * @param[in] offset Offset of the newest sample
 *
 * @retval 1 Samples read
 * @retval 0 Reading samples failed
 *
 */
int32_t slifo_read(slifo_t *hnd, flt32_t *buffer, int32_t length, int32_t offset)
{
    int32_t i, idx;

    // Ensure that we don't read too much data from the sample buffer
    if (length > hnd->length)
    {
        return 0;
    }

    // Dequeue newest data from the circular buffer
    idx = hnd->index_copy - offset;
    for (i=length-1; i >= 0; --i)
    {
        if (--idx < 0)
        {
            idx += hnd->length;
        }
        buffer[i] = hnd->buffer[idx];
    }
    return 1;
}

/**
 * @brief Checks if the sample LIFO full
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 *
 * @retval 1 sample LIFO full
 * @retval 0 sample LIFO not full
 *
 */
uint8_t slifo_full(slifo_t *hnd)
{
    return hnd->full;
}

/**
 * @brief Clears the sample LIFO
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 *
 */
void slifo16_flush(slifo16_t *hnd)
{
    hnd->index_copy = 0;
    hnd->index = 0;
    hnd->full = 0;
}

/**
 * @brief Initializes the LIFO
 *
 * @param[in] hnd Pointer to the LIFO handle
 * @param[in] buffer Sample buffer
 * @param[in] length Sample bufer length
 *
 */
void slifo16_init(slifo16_t *hnd, int16_t *buffer, int32_t length)
{
    hnd->buffer = buffer;
    hnd->length = length;

    slifo16_flush(hnd);
}

/**
 * @brief Writes a sample to the sample LIFO
 *
 * @param[in] hnd Pointer to sample LIFO handle
 * @param[in] val Sample value
 *
 */
void slifo16_write(slifo16_t *hnd, int16_t val)
{
    hnd->buffer[hnd->index++] = val;
    if (hnd->index >= hnd->length)
    {
        hnd->index = 0;
        hnd->full = 1; 
    }
}

/**
 * @brief Copies the current LIFO index into a steady copy
 *
 * @note Must be called before any sfifo_read
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 *
 */
void slifo16_freeze(slifo16_t *hnd)
{
    hnd->index_copy = hnd->index;
}

/**
 * @brief Reads the sample values from the sample LIFO into array
 *
 * @note sfifo_freeze must be called before calling this function!
 * 
 * @param[in] hnd Pointer to the sample LIFO handle
 * @param[out] buffer Output array
 * @param[in] length Length of buffer array in samples
 * @param[in] offset Offset of the newest sample
 *
 * @retval 1 Samples read
 * @retval 0 Reading samples failed
 *
 */
int32_t slifo16_read(slifo16_t *hnd, flt32_t *buffer, int32_t length, int32_t offset)
{
    int32_t i, idx;

    // Ensure that we don't read too much data from the sample buffer
    if (length > hnd->length)
    {
        return 0;
    }

    // Dequeue newest data from the circular buffer
    idx = hnd->index_copy - offset;
    for (i=length-1; i >= 0; --i)
    {
        if (--idx < 0)
        {
            idx += hnd->length;
        }
        buffer[i] = AD2FLT32(hnd->buffer[idx]);
    }
    return 1;
}

/**
 * @brief Checks if the sample LIFO full
 *
 * @param[in] hnd Pointer to the sample LIFO handle
 *
 * @retval 1 sample LIFO full
 * @retval 0 sample LIFO not full
 *
 */
uint8_t slifo16_full(slifo16_t *hnd)
{
    return hnd->full;
}

/**
 * @}
 */
