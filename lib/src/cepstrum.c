
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Cepstrum Transform API
 *
 **********************************************************************/

/**
 * @addtogroup Cepstrum Cepstrum Transform
 * @{
 */

#include <math.h>
#include <string.h>
#include "cepstrum.h"
#include "window.h"
#include "fft.h"

/**
 * @brief Converts the Cepstrum bin index into the quefrency 
 *
 * @param[in] bin Cepstrum bin index
 * @param[in] n Cepstrum output buffer length
 * @param[in] fs Input signal sampling frequency in Hz
 *
 * @return Quefrency in seconds
 */
flt32_t cepst_bin2quef(int32_t bin, int32_t n, int32_t fs)
{
    flt32_t q = (flt32_t)bin / (flt32_t)((n - 1) * fs);

    return q;
}

/**
 * @brief Calculates the real Cepstrum 
 *
 * \f$
 * y = \mathcal{F}^{-1} \left\{
 *  \log \left(
 *      \left| \mathcal{F}(x) \right|
 *  \right)
 * \right\}
 * \f$
 *
 * @param[in] x Array of real input values (capacity \p n + 2 samples)
 * @param[out] y Array of real output values (capacity \p n + 2 samples, only \p n/4 output samples are valid)
 * @param[in] n Length of \p x and \p y in samples (must be power of 2)
 */
void cepstrum(flt32_t *x, flt32_t *y, int32_t n)
{
    int32_t i, nh = n/2;
    cplx_t *yt = (cplx_t *)y;
    cplx_t *xt = (cplx_t *)x;
    flt32_t mag;

    fft_real(x, yt, n);

    // Logarithm of magnitude (a factor 0.5 below is because of the square root)
    for (i=0; i < nh + 1; ++i)
    {
        mag = yt[i].re * yt[i].re + yt[i].im * yt[i].im;

        xt[i].re = 0.5f * logf(mag);
        xt[i].im = 0.0f;
    }  

    ifft_real(xt, y, n);
}

/**
 * @}
 */
