
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Statistics API
 *
 **********************************************************************/

 /**
 * @addtogroup Stat Statistics
 * @{
 */

#include <math.h>
#include <string.h>
#include "statistics.h"
#include "utils.h"

/** 
 * @brief Calculates the mean
 *
 * \f$
 * y = \frac{1}{n}\sum{x}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 *
 * @return Mean value
 */
flt32_t stat_mean(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;

    flt32_t c = 0.0f, y, t;

    for (i=0; i < n; ++i)
    {
        // sum += x[i];
        y = x[i] - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }    

    sum /= (flt32_t)n;

    return sum;
}

/**
 * @brief Calculates the median
 *
 * \f$
 * y = \left \{
 *  \begin{array}{l}
 *      x_{\frac{n+1}{2}} & n \textrm{ odd} \\
 *      \frac{1}{2}\left(x_{\frac{n}{2}} + x_{\frac{n}{2} + 1}\right) & n \textrm{ even}
 *  \end{array}
 * \right.
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 *
 * @return Median value
 */
flt32_t stat_median(flt32_t *x, int32_t n)
{
    int32_t nh = n / 2;
    flt32_t rv = 0.0f;

    shellsort(x, n);

    // get the middle data point
    if ((n % 2) == 0)
    {
        rv = 0.5f * (x[nh] + x[nh - 1]);
    }
    else
    {
        rv = x[nh];
    }

    return rv;
}

/** 
 * @brief Calculates the mean of absolute values
 *
 * \f$
 * y = \frac{1}{n}\sum{\left| x \right|}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 *
 * @return Mean absolute value
 */
flt32_t stat_mean_abs(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;

    flt32_t c = 0.0f, y, t;

    for (i=0; i < n; ++i)
    {
        // sum += fabsf(x[i]);
        y = fabsf(x[i]) - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    sum /= (flt32_t)n;

    return sum;
}

/**
 * @brief Calculates the root mean square (RMS)
 *
 * \f$
 * y = \sqrt{\frac{1}{n}\sum{x^2}}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 *
 * @return RMS value
 */
flt32_t stat_rms(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;

    flt32_t c = 0.0f, y, t;

    for (i=0; i < n; ++i)
    {
        // sum += x[i] * x[i];
        y = x[i] * x[i] - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    sum = sqrtf(sum / (flt32_t)n);

    return sum;
}

/**
 * @brief Calculates the mean absolute difference (MAD)
 *
 * \f$
 * y = \frac{1}{n}\sum{\left| x - \bar{x} \right|}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 *
 * @return MAD value
 */
flt32_t stat_mad(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;
    flt32_t xm;

    flt32_t c = 0.0f, y, t;

    // Calculate the mean value
    xm = stat_mean(x, n);

    // Calculate the mean absolute difference
    for (i=0; i < n; ++i)
    {
        // sum += fabsf(x[i] - xm);
        y = fabsf(x[i] - xm) - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    sum /= (flt32_t)n;

    return sum;
}

/**
 * @brief Finds the index of the minimal value in an array
 *
 * \f$
 * y = \arg{\min(x)}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 * @param[out] imin Index of minimum in \p x
 */
void stat_min(flt32_t *x, int32_t n, int32_t *imin)
{
    int32_t i;
    flt32_t min = FLT32_MAX;

    *imin = -1;

    for (i=0; i < n; ++i)
    {
        if (x[i] < min)
        {
            min = x[i];
            *imin = i;
        }
    }
}

/**
 * @brief Finds the index of the maximal value in an array
 *
 * \f$
 * y = \arg{\max(x)}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 * @param[out] imax Index of maximum in \p x
 */
void stat_max(flt32_t *x, int32_t n, int32_t *imax)
{
    int32_t i;
    flt32_t max = -FLT32_MAX;

    *imax = -1;
    
    for (i=0; i < n; ++i)
    {
        if (x[i] > max)
        {
            max = x[i];
            *imax = i;
        }
    }
}

/**
 * @brief Calculates the Shannon entropy
 *
 * \f$
 * y = -\sum{x \log(x)}
 * \f$
 *
 * @param[in] x Input array (probabilities)
 * @param[in] n Length of \p x in samples
 *
 * @return Shannon entropy value
 */
flt32_t stat_entropy(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;

    flt32_t c = 0.0f, y, t;
    
    for (i=0; i < n; ++i)
    {
        // s += x[i] * logf(x[i]);
        y = x[i] * logf(x[i]) - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    return (-sum);
}

/**
 * @brief Calculates the relative variance
 *
 * \f$
 * y = \frac{1}{n-1}\frac{\sum{ \left( x - \bar{x} \right)^2 }}{\bar{x}^2}
 * \f$
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 *
 * @return Relative variance value
 *
 */
flt32_t stat_relvar(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;
    flt32_t xm;

    flt32_t c = 0.0f, y, t;

    // Calculate mean value
    xm = stat_mean(x, n);

    // Calculate relative variance
    for (i=0; i < n; ++i)
    {
        // sum += (x[i] - xm) * (x[i] - xm);
        y = (x[i] - xm) * (x[i] - xm) - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    sum = (sum / (xm * xm)) / (flt32_t)(n - 1);

    return sum;
}

/**
 * @brief Calculates the linear regression
 *
 * \f$
 * y = k(x - \bar{x}) + \bar{y}
 * \f$
 *
 * @note Call @ref stat_linreg_fit prior this function to obtain \p arg
 * 
 * @param[in] x Regression line X-value
 * @param[in] arg Regression line parameters
 *
 * @return Regression line Y-value
 *
 */
flt32_t stat_linreg_calc(int32_t x, stat_linreg_t *arg)
{
    flt32_t y;

    y = (flt32_t)(x - arg->xm) * arg->k + arg->ym;

    return y;
}

/**
 * @brief Calculates the linear regression parameters (data fitting)
 *
 * \f$
 * \bar{x} = \frac{x2 - x1}{2}
 * \f$
 * \f$
 * \bar{y} = \frac{1}{x2 - x1}\sum\limits_{i=x1}^{x2}{y_i}
 * \f$
 * \f$
 * k = \sum\limits_{i=x1}^{x2}{\frac{\left( x_i - \bar{x} \right) \left( y_i - \bar{y} \right)}{ \left( x_i - \bar{x} \right)^2}}
 * \f$
 *
 * @param[in] x1 Start index in \p y array for data under fitting
 * @param[in] x2 End index in \p y array for data under fitting
 * @param[in] y Array of \p y values
 * @param[out] arg Regression line parameters
 *
 */
void stat_linreg_fit(int32_t x1, int32_t x2, flt32_t *y, stat_linreg_t *arg)
{
    int32_t x;
    flt32_t sum1 = 0.0f, sum2 = 0.0f;

    // Calculate X mean
    arg->xm = (x1 + x2) / 2;

    // Calculate Y mean	
    arg->ym = stat_mean(&y[x1], x2 - x1 + 1);

    flt32_t c1 = 0.0f, y1, t1;
    flt32_t c2 = 0.0f, y2, t2;

    // Calculate regression line slope
    for (x=x1; x <= x2; ++x)
    {
        // xs1 += (x - arg->xm) * (y[x] - arg->ym);
        y1 = (x - arg->xm) * (y[x] - arg->ym) - c1;
        t1 = sum1 + y1;
        c1 = (t1 - sum1) - y1;
        sum1 = t1;

        // xs2 += (flt64_t)(x - arg->xm) * (flt64_t)(x - arg->xm);
        y2 = (x - arg->xm) * (x - arg->xm) - c2;
        t2 = sum2 + y2;
        c2 = (t2 - sum2) - y2;
        sum2 = t2;
    }

    arg->k = sum1 / sum2;
}

/**
 * @brief Initializes the moving average
 *
 * @param[in] hnd Moving average handle
 * @param[in] buffer Sample buffer
 * @param[in] n \p buffer length in samples
 *
 */
void ma_init(stat_ma_t *hnd, flt32_t *buffer, int32_t n)
{
    // Initialize circular buffer
    hnd->buffer = buffer;
    hnd->length = n;

    memset(hnd->buffer, 0, n * sizeof(flt32_t));

    // Initialize indices
    hnd->sum = 0.0;
    hnd->index = 0;
}

/**
 * @brief Calculates the moving average
 *
 * \f$
 * y = \frac{1}{n}\sum{x}
 * \f$
 *
 * @param[in] hnd Moving average handle
 * @param[in] x Input sample
 *
 * @return New running average value
 *
 */
flt32_t ma_calc(stat_ma_t *hnd, flt32_t x)
{   
    // Buffer full, calculate recursive running moving
    hnd->sum = hnd->sum + (flt64_t)(x - hnd->buffer[hnd->index]) / (flt64_t)hnd->length;

    // Write new sample into circular buffer
    hnd->buffer[hnd->index++] = x;
    if (hnd->index >= hnd->length)
    {
        hnd->index = 0;
    }
	
    return (flt32_t)hnd->sum;
}

/**
 * @brief Initializes the exponential moving average
 *
 * @param[in] hnd Moving average handle
 * @param[in] k Coefficient, range [0, 1]
 *
 */
void ema_init(stat_ema_t *hnd, flt32_t k)
{
    hnd->k = k;
    hnd->y = 0.0f;
}

/**
 * @brief Calculates the exponential moving average
 *
 * \f$
 * y_n = kx_n + (1 - k)y_{n-1} 
 * \f$
 *
 * @param[in] hnd Exponential moving average handle
 * @param[in] x Input sample
 *
 * @return New exponential running average value
 *
 */
flt32_t ema_calc(stat_ema_t *hnd, flt32_t x)
{
    hnd->y = x * hnd->k + hnd->y * (1.0f - hnd->k);

    return hnd->y;
}

/**
 * @}
 */
