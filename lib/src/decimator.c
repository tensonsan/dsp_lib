
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   API for signal downsampling (FIR filtering + decimation)
 *
 **********************************************************************/
 
/**
 * @addtogroup Downsampler Downsampling
 * @{
 */
  
#include "decimator.h"

/**
 * @brief Initializes the downsampling
 *
 * @param[in] hnd Downsampler handle
 * @param[in] buffer Downsampler input sample buffer
 * @param[in] c Aliasing filter coefficients (direct FIR structure)
 * @param[in] n Length of \p c and \p buffer
 * @param[in] m Decimation factor
 * @param[in] proc Callback for processing of downsampled samples
 */
void decim_init(decim_t *hnd, flt32_t *buffer, const flt32_t *c, int32_t n, int32_t m, decim_proc_t proc)
{
    hnd->buffer = buffer;
    hnd->c = (flt32_t *)c;
    hnd->n = n;
    hnd->m = m;
    hnd->proc = proc;
    
    decim_reset(hnd);
}

/**
 * @brief Resets the downsampling
 *
 * @param[in] hnd Downsampler handle
 */
void decim_reset(decim_t *hnd)
{
    int32_t i;
    
    hnd->cnt = hnd->m;
    hnd->index = 0;
    
    for (i=0; i < hnd->n; ++i)
    {
        hnd->buffer[i] = 0.0f;
    }
}

/**
 * @brief Performs downsampling
 *
 * @note The function calls the proc callback for each downsampled value
 *
 * @param[in] hnd Downsampling handle 
 * @param[in] val Sample value
 */
void decim_calc(decim_t *hnd, flt32_t val)
{
    int32_t i, idx;
    flt32_t y = 0.0f;

    // Write a value into the circular buffer
    hnd->buffer[hnd->index++] = val;
    if (hnd->index >= hnd->n)
    {
        hnd->index = 0;
    }

    // Wait until buffer filled for decimation
    if (hnd->cnt >= hnd->m)
    {
        hnd->cnt = 0;

        idx = hnd->index;
        // Perform the FIR filtering
        for (i=0; i < hnd->n; ++i)
        {
            // FIR convolution
            y += hnd->buffer[idx++] * hnd->c[i];
            if (idx >= hnd->n)
            {
                idx = 0;
            }
        }
        hnd->proc(y);
    }
    hnd->cnt++;
}

/**
 * @brief Performs downsampling
 *
 * @param[in] x Input buffer (capacity \p n samples)
 * @param[out] y Output buffer (capacity \p n / \p m samples)
 * @param[in] c Filter coefficients 
 * @param[in] nc Length of \p c in samples 
 * @param[in] n Length of \p x in samples
 * @param[in] m Decimation factor
 */
void decim_calc_buffer(flt32_t *x, flt32_t *y, const flt32_t *c, int32_t nc, int32_t n, int32_t m)
{
    int32_t i, j, k = 0, idx;
    flt32_t tmp;

    for (i=0; i < n; i += m)
    {
        tmp = 0.0f;
        for (j=0; j < nc; ++j)
        {
            idx = i - j;
            if (idx >= 0)
            {
                tmp += x[idx] * c[j];
            }
        }
        y[k++] = tmp;
    }
}

/**
 * @brief Initializes the downsampling
 *
 * @param[in] hnd Downsampler handle
 * @param[in] buffer Downsampler input sample buffer
 * @param[in] c Aliasing filter coefficients (direct FIR structure)
 * @param[in] n Length of \p c and \p buffer in samples
 * @param[in] m Decimation factor
 * @param[in] proc Callback for processing of downsampled samples
 */
void decim16_init(decim16_t *hnd, int16_t *buffer, const int16_t *c, int32_t n, int32_t m, decim16_proc_t proc)
{
    hnd->buffer = buffer;
    hnd->c = (int16_t *)c;
    hnd->n = n;
    hnd->m = m;
    hnd->proc = proc;
    
    decim16_reset(hnd);
}

/**
 * @brief Resets the downsampling
 *
 * @param[in] hnd Downsampler handle
 */
void decim16_reset(decim16_t *hnd)
{
    int32_t i;
    
    hnd->cnt = hnd->m;
    hnd->index = 0;
    
    for (i=0; i < hnd->n; ++i)
    {
        hnd->buffer[i] = 0;
    }
}

/**
 * @brief Performs downsampling
 *
 * @note The function calls the proc callback for each downsampled value
 *
 * @param[in] hnd Downsampling handle 
 * @param[in] val Sample value
 */
void decim16_calc(decim16_t *hnd, int16_t val)
{
    int32_t i, idx;
    int32_t y = 0;

    // Write a value into the circular buffer
    hnd->buffer[hnd->index++] = val;
    if (hnd->index >= hnd->n)
    {
        hnd->index = 0;
    }

    // Wait until buffer filled for decimation
    if (hnd->cnt >= hnd->m)
    {
        hnd->cnt = 0;

        idx = hnd->index;
        // Perform the FIR filtering
        for (i=0; i < hnd->n; ++i)
        {
            // FIR convolution
            y += (int32_t)(hnd->buffer[idx++] * hnd->c[i]);
            if (idx >= hnd->n)
            {
                idx = 0;
            }
        }
        hnd->proc((int16_t)(y >> 15));
    }
    hnd->cnt++;
}

/**
 * @brief Performs downsampling
 *
 * @param[in] x Input buffer (capacity \p n samples)
 * @param[out] y Output buffer (capacity \p n / \p m samples)
 * @param[in] c Filter coefficients 
 * @param[in] nc Length of \p c in samples 
 * @param[in] n Length of buffer \p y
 * @param[in] m Decimation factor
 */
void decim16_calc_buffer(int16_t *x, int16_t *y, const int16_t *c, int32_t nc, int32_t n, int32_t m)
{
    int32_t i, j, k=0, idx, tmp;

    for (i=0; i < n; i += m)
    {
        tmp = 0;
        for (j=0; j < nc; ++j)
        {
            idx = i - j;
            if (idx >= 0)
            {
                tmp += (int32_t)(x[idx] * c[j]);
            }
        }
        y[k++] = (int16_t)(tmp >> 15);
    }
}

/**
 * @}
 */
