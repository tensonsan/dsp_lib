
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Spectral Subtraction API
 *
 **********************************************************************/
 
/**
 * @addtogroup SSUB Spectral Subtraction
 * @{
 */
 
#include <stdio.h>
#include <math.h>
#include "spectral_subtraction.h"
#include "statistics.h"

/**
 * @brief Resets the spectral subtraction calibration
 *
 * @param[in] hnd Spectral subtraction handle
 */
void ssub_reset(ssub_t *hnd)
{
    int32_t i;
    
    for (i=0; i < hnd->noise_length; ++i)
    {
        hnd->noise_mean[i] = 0.0f;
    }

    hnd->noise_count = 0;
}

/**
 * @brief Initializes the spectral subtraction
 *
 * @param[in] hnd Spectral subtraction handle
 * @param[in] buffer Buffer for storing mean noise spectrum
 * @param[in] length Length of \p buffer in samples
 * @param[in] alpha Over-subtraction factor
 * @param[in] beta Mean noise scale factor
 * @param[in] k Exponential moving average coefficient for noise frame updates
 * @param[in] noise_onset Noise onset in number of frames
 * @param[in] noise_threshold Noise threshold in dB
 */
void ssub_init(ssub_t *hnd, flt32_t *buffer, int32_t length, flt32_t alpha, flt32_t beta, flt32_t k, int32_t noise_onset, flt32_t noise_threshold)
{
    hnd->noise_onset = noise_onset;
	hnd->noise_threshold = noise_threshold;
    hnd->noise_mean = buffer;
    hnd->noise_length = length;
    hnd->alpha = alpha;
    hnd->beta = beta;
    hnd->k = k;
    
    ssub_reset(hnd);
}

/**
 * @brief Performs the noise averaging via the exponential moving average
 *
 * @note This fuction should be called on the noise frames (calibration)
 *
 * @param[in] hnd Spectral subtraction handle
 * @param[in] x Noise power spectrum buffer
 */
void ssub_noise_update(ssub_t *hnd, flt32_t *x)
{
    int32_t i;
    flt32_t cs;
    
    for (i=0; i < hnd->noise_length; ++i)
    {
        cs = hnd->noise_mean[i];

        cs = cs * hnd->k + x[i] * (1.0f - hnd->k);

        hnd->noise_mean[i] = cs;
    }
}

/**
 * @brief Voice activity detector (VAD)
 *
 * @param[in] hnd Spectral subtraction handle
 * @param[in] x Power spectrum buffer
 *
 * @retval 1 signal detected
 * @retval 0 noise detected
 */
int32_t ssub_vad(ssub_t *hnd, flt32_t *x)
{
    int32_t i;
    flt32_t xm = 0.0f;
    flt32_t snr;

    flt32_t c = 0.0f, y, t;

    // Calculate the mean SNR
    for (i=0; i < hnd->noise_length; ++i)
    {
        snr = 10.0f * log10f(x[i] / hnd->noise_mean[i]);
        if (snr < 0.0f)
        {
            snr = 0.0f;
        }

        // xm += snr;
        y = snr - c;
        t = xm + y;
        c = (t - xm) - y;
        xm = t;
    }
    xm = xm / (flt32_t)(hnd->noise_length);
    
    // Check if input signal above the noise
    if (xm < hnd->noise_threshold)
    {
        hnd->noise_count++;
    }
    else
    {
        hnd->noise_count = 0;
    }
    
    // Check if the onset count reached 
    if (hnd->noise_count > hnd->noise_onset)
    {
        return 0;
    }
       
    return 1;
}

/**
 * @brief Performs the spectral subtraction and the mean noise update
 *
 * @note This fuction should be for each captured frame
 *
 * @param[in] hnd Spectral subtraction handle
 * @param[in,out] x Power spectrum input, output buffer
 *
 * @retval 1 frame is signal
 * @retval 0 frame is noise
 */
int32_t ssub_process(ssub_t *hnd, flt32_t *x)
{
    int32_t i, signal;
    flt32_t xm = 0.0f, nm = 0.0f;
    
    signal = ssub_vad(hnd, x);
    if (0 == signal)
    {
        // Update the mean noise
        ssub_noise_update(hnd, x);
        
        for (i=0; i < hnd->noise_length; ++i)
        {
            x[i] = x[i] * hnd->beta;
        }
    }
    else
    {
        // Perform the spectral subtraction
        for (i=0; i < hnd->noise_length; ++i)
        {
            xm = x[i] - hnd->alpha * hnd->noise_mean[i];
            nm = hnd->beta * hnd->noise_mean[i];

            if (xm < nm)
            {
                    xm = nm;
            }
            x[i] = xm;
        }
    }

    return signal;
}

/**
 * @}
 */
