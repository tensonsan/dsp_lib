
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   FIR Filtering API
 *
 **********************************************************************/

/**
 * @addtogroup FIR FIR Filter
 * @{
 */

#include "fir.h" 

/**
 * @brief Initializes the FIR
 *
 * \f$
 * H(z) = \sum\limits_{i=0}^{n-1}{c_i z^{-i}}
 * \f$
 *
 * @param[in] hnd FIR handle
 * @param[in] buffer Internal buffer
 * @param[in] c filter coefficients (direct FIR structure)
 * @param[in] n Length of \p c and \p buffer
 */
void fir_init(fir_t *hnd, flt32_t *buffer, const flt32_t *c, int32_t n)
{
    int32_t i;

    hnd->buffer = buffer;
    hnd->c = (flt32_t *)c;
    hnd->n = n;
    hnd->index = 0;

    for (i=0; i < n; ++i)
    {
        buffer[i] = 0.0f;
    }
}

/**
 * @brief Performs FIR filtering
 *
 * \f$
 * y = \sum\limits_{i=0}^{n-1}{c_i x_{n-i-1}}
 * \f$
 *
 * @param[in] hnd FIR handle 
 * @param[in] x Sample value
 *
 * @return Filtered sample
 */
flt32_t fir_calc(fir_t *hnd, flt32_t x)
{
    int32_t i, idx;
    flt32_t y = 0.0f;

    // Write a value into the circular buffer
    hnd->buffer[hnd->index++] = x;
    if (hnd->index >= hnd->n)
    {
        hnd->index = 0;
    }

    idx = hnd->index;

    // Perform the FIR filtering
    for (i=0; i < hnd->n; ++i)
    {
        // FIR convolution
        y += hnd->buffer[idx++] * hnd->c[i];
        if (idx >= hnd->n)
        {
            idx = 0;
        }
    }
    return y;
}

/**
 * @brief Performs FIR filtering
 *
 * \f$
 * y = \sum\limits_{i=0}^{n-1}{c_i x_{n-i-1}}
 * \f$
 *
 * @param[in] x Input buffer
 * @param[out] y Output buffer
 * @param[in] c Filter coefficients 
 * @param[in] nc Length of \p c 
 * @param[in] n Length of \p x and \p y in samples
 */
void fir_calc_buffer(flt32_t *x, flt32_t *y, const flt32_t *c, int32_t nc, int32_t n)
{
    int32_t i, j, idx;

    for (i=0; i < n; ++i)
    {
        y[i] = 0.0f;
        for (j=0; j < nc; ++j)
        {
            idx = i - j;
            if (idx >= 0)
            {
                y[i] += x[idx] * c[j];
            }
        }
    }
}

/**
 * @brief Initializes the FIR (fixed point)
 *
 * \f$
 * H(z) = \sum\limits_{i=0}^{n-1}{c_i z^{-i}}
 * \f$
 *
 * @param[in] hnd FIR handle
 * @param[in] buffer Internal buffer
 * @param[in] c filter coefficients (direct FIR structure)
 * @param[in] n Length of \p c and \p buffer in samples
 */
void fir16_init(fir16_t *hnd, int16_t *buffer, const int16_t *c, int32_t n)
{
    int32_t i;

    hnd->buffer = buffer;
    hnd->c = (int16_t *)c;
    hnd->n = n;
    hnd->index = 0;

    for (i=0; i < n; ++i)
    {
        buffer[i] = 0;
    }
}

/**
 * @brief Performs FIR filtering (fixed point)
 *
 * \f$
 * y = \sum\limits_{i=0}^{n-1}{c_i x_{n-i-1}}
 * \f$
 *
 * @param[in] hnd FIR handle 
 * @param[in] x Sample value
 *
 * @return Filtered sample
 */
int16_t fir16_calc(fir16_t *hnd, int16_t x)
{
    int32_t i, idx, y = 0;

    // Write a value into the circular buffer
    hnd->buffer[hnd->index++] = x;
    if (hnd->index >= hnd->n)
    {
        hnd->index = 0;
    }

    idx = hnd->index;

    // Perform the FIR filtering
    for (i=0; i < hnd->n; ++i)
    {
        // FIR convolution
        y += (int32_t)hnd->buffer[idx++] * (int32_t)hnd->c[i];
        if (idx >= hnd->n)
        {
            idx = 0;
        }
    }
    return (int16_t)(y >> 15);
}

/**
 * @brief Performs FIR filtering
 *
 * \f$
 * y = \sum\limits_{i=0}^{n-1}{c_i x_{n-i-1}}
 * \f$
 *
 * @param[in] x Input buffer
 * @param[out] y Output buffer
 * @param[in] c Filter coefficients 
 * @param[in] nc Length of \p c 
 * @param[in] n Length of \p x and \p y in samples
 */
void fir16_calc_buffer(int16_t *x, int16_t *y, const int16_t *c, int32_t nc, int32_t n)
{
    int32_t i, j, idx, tmp;

    for (i=0; i < n; ++i)
    {
        tmp = 0.0f;
        for (j=0; j < nc; ++j)
        {
            idx = i - j;
            if (idx >= 0)
            {
                tmp += x[idx] * c[j];
            }
        }
        y[i] = (int16_t)(tmp >> 15);
    }
}

/**
 * @}
 */
