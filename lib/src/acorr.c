
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Autocorrelation API
 *
 **********************************************************************/

/**
 * @addtogroup ACORR Autocorrelation API
 * @{
 */
 
#include <stdio.h>
#include <string.h>
#include "acorr.h"
#include "fft.h"

/**
 * @brief Calculates the autocorrelation \f$ r \f$
 * 
 * \f$ 
 * r_i = \sum\limits_{j=0}^{n-i}{x_j x_{i+j}}
 * \f$ 
 *
 * @param[in] x Input signal
 * @param[out] r Array of autocorrelation coefficients (capacity \p m + 1 coefficients)
 * @param[in] n Length of \p x in samples
 * @param[in] m Number of autocorrelation coefficients
 */
void acorr_norm(flt32_t *x, flt32_t *r, int32_t n, int32_t m)
{
    int32_t i, j;
    flt32_t sum;

    flt32_t c = 0.0f, y, t;

    n -= 1;

    for (i=0; i <= m; ++i)
    {
        sum = 0.0;
        for (j=0; j <= n - i; ++j)
        {
            // sum += x[j] * x[j + i];
            y = x[j] * x[j + i] - c;
            t = sum + y;
            c = (t - sum) - y;
            sum = t;
        }
        r[i] = sum;
    }
}

/**
 * @brief Calculates the autocorrelation \f$ y \f$ using the FFT
 *
 * \f$
 * y = \mathcal{F}^{-1} \left\{
 *  \left| \mathcal{F}(x) \right|^2
 * \right\}
 * \f$
 *
 * @param[in] x Input signal (capacity \p n + 2 samples, although only the first \p n/2 samples are used)
 * @param[out] y Array of autocorrelation coefficients (capacity \p n + 2 coefficients)
 * @param[in] n Length of \p x and \p y in samples
 */
void acorr_fft(flt32_t *x, flt32_t *y, int32_t n)
{
    int32_t i, nh = n / 2;
    cplx_t *yt, *xt;

    // Zero pad the second half of the input data
    memset(&x[nh], 0,  nh * sizeof(flt32_t));

    yt = (cplx_t *)y;
    xt = (cplx_t *)x;

    // Forward FFT
    fft_real(x, yt, n);

    // Power
    for (i=0; i < nh + 1; ++i)
    {
        xt[i].re = yt[i].re * yt[i].re + yt[i].im * yt[i].im;
        xt[i].im = 0.0f;
    }

    // Inverse FFT
    ifft_real(xt, y, n);
}

/**
 * @}
 */
