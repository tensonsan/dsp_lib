
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Activation Functions API
 *
 **********************************************************************/

/**
 * @addtogroup Activation Activation functions
 * @{
 */

#include <math.h>
#include "activation.h"
#include "utils.h"

/**
 * @brief Leaky ReLU function
 * 
 * @note Set a = 0.0f to get ReLU
 *
 * \f$
 * y = \left\{
 *      \begin{array}{l}
 *          ax & \mbox{for} & x < 0 \\
 *          x & \mbox{for} & x \ge 0
 *      \end{array} \right.
 * \f$
 *
 * @param[in] x Input value
 * @param[in] a Leaking factor
 * @return Output value \f$ y \f$
 */
flt32_t act_leaky_relu(flt32_t x, flt32_t a)
{
    flt32_t rv;

    if (x > 0.0f)
    {
        rv = x;
    }
    else
    {
        rv = x * a;
    }

    return rv;
}

/**
 * @brief Saturated linear function
 *
 * \f$
 * y = \left\{
 *  \begin{array}{l}
 *      -1 & \mbox{for} & x < -1 \\
 *      1 & \mbox{for} & x > 1 \\
 *      x & \mbox{otherwise}
 *  \end{array} \right.
 * \f$
 *
 * @param[in] x Input value
 * @return Output value \f$ y \f$
 */
flt32_t act_satlins(flt32_t x)
{
    flt32_t rv;

    if (x < -1.0f)
    {
        rv = -1.0f;
    }
    else if (x > 1.0f)
    {
        rv = 1.0f;
    }
    else
    {
        rv = x;
    }

    return rv;
}

/**
 * @brief Sigmoid function
 *
 * \f$
 * y = \frac{1}{1 + \exp(-x)}
 * \f$
 *
 * @param[in] x Input value
 * @return Output value \f$ y \f$
 */
flt32_t act_logsig(flt32_t x)
{
    flt32_t rv;

    rv = 1.0f / (1.0f + expf(-x));

    return rv;
}

/**
 * @brief tanh function
 *
 * \f$
 * y = \tanh(x)
 * \f$
 *
 * @param[in] x Input value
 * @return Output value \f$ y \f$
 */
flt32_t act_tanh(flt32_t x)
{
    flt32_t rv;

    rv = tanhf(x);

    return rv;
}

/**
 * @brief Linear function
 *
 * \f$
 * y = x
 * \f$
 *
 * @param[in] x Input value
 * @return Output value \f$ y \f$
 */
flt32_t act_linear(flt32_t x)
{
    return x;
}

/**
 * @brief In-place softmax function
 *
 * \f$
 * y = \frac{\exp(x)}{\sum{\exp(x)}}
 * \f$
 *
 * @param[in,out] x Sample vector
 * @param[in] n Length of \p x in samples
 */
void act_softmax(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t sum = 0.0f;

    flt32_t c = 0.0f, t, y;

    for (i=0; i < n; ++i)
    {
        // sum += expf(x[i]);
        y = expf(x[i]) - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    for (i=0; i < n; ++i)
    {
        x[i] = expf(x[i]) / sum;
    }
}

/**
 * @brief Hard sigmoid function
 *
 * \f$
 * y = \left\{
 *  \begin{array}{l}
 *      0 & \mbox{for} & x < -2.5 \\
 *      1 & \mbox{for} & x > 2.5 \\
 *      0.2 x + 0.5 & \mbox{otherwise}
 *  \end{array} \right.
 * \f$
 *
 * @param[in] x Input value
 * @return Output value \f$ y \f$
 */
flt32_t act_hard_sigmoid(flt32_t x)
{
    flt32_t rv;

    rv = MAX(0.0f, MIN(1.0f, (x * 0.2f) + 0.5f));

    return rv;
}

/**
 * @}
 */
