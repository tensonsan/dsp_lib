/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Hidden Markov Model API
 *
 **********************************************************************/

/**
 * @addtogroup HMM Hidden Markov Model
 * @{
 */

#include "hmm.h"
#include "utils.h"

/**
 * @brief Initializes HMM
 *
 * @param[in] hmm HMM handle
 * @param[in] a Log transition probability matrix (capacity \p N * \p N)
 * @param[in] b Log emission probability matrix (capacity \p N * \p M)
 * @param[in] pi Initial log probabilities (capacity \p N)
 * @param[in] N Transition matrix dimension
 * @param[in] M Emission matrix dimension
 */
void hmm_init(hmm_t *hmm, const flt32_t *a, const flt32_t *b, const flt32_t *pi, int32_t N, int32_t M)
{
    hmm->a = a;
    hmm->b = b;
    hmm->pi = pi;
    hmm->N = N;
    hmm->M = M;
}

/**
 * @brief Calculates the log likelihood of the best sequence of hidden states (Viterbi decoding)
 *
 * @param[in] hmm HMM handle
 * @param[out] delta Maximal log probability of state sequences (capacity \p T * \p N)
 * @param[in] o Observations (capacity \p T)
 * @param[in] T maximal time
 * 
 * @return log likelihood
 */
flt32_t hmm_viterbi(hmm_t *hmm, flt32_t *delta, int32_t *o, int32_t T)
{
    int32_t i, j, t;
    flt32_t p, p_best;

    // Initialization
    for (i=0; i < hmm->N; ++i)
    {
        delta[IDX(0, i, hmm->N)] = hmm->pi[i] + hmm->b[IDX(i, o[0], hmm->M)];
    }

    // Iterations
    for (t=1; t < T; ++t)
    {
        for (i=0; i < hmm->N; ++i)
        {
            p_best = -FLT32_MAX;

            for (j = 0; j < hmm->N; ++j)
            {
                p = delta[IDX(t-1, j, hmm->N)] + hmm->a[IDX(j, i, hmm->N)];
                if (p > p_best)
                {
                    p_best = p;
                }
            }

            delta[IDX(t, i, hmm->N)] = p_best + hmm->b[IDX(i, o[t], hmm->M)];
        }
    }

    // Backtracking
    p_best = -FLT32_MAX;

    for (j=0; j < hmm->N; ++j)
    {
        p = delta[IDX(T-1, j, hmm->N)];
        if (p > p_best)
        {
            p_best = p;
        }
    }

    return p_best;
}

/**
 * @}
 */
