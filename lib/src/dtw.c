
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Dynamic Time Warping API
 *
 **********************************************************************/

/**
 * @addtogroup DTW Dynamic Time Warping
 * @{
 */

#include <stdlib.h>
#include <math.h>
#include "dtw.h"
#include "statistics.h"
#include "utils.h"

/**
 * @brief Calculates the euclidean distance between \p x and \p y
 *
 * \f$
 * y = \sqrt{\left( x - y \right)^2}
 * \f$
 *
 * @param[in] x Value 1
 * @param[in] y Value 2
 *
 * @return Distance between x and y
 */
static flt32_t dtw_dist(flt32_t x, flt32_t y)
{
    flt32_t d = x - y;

    return sqrtf(d * d);
}

/**
 * @brief Calcuates the dtw matrix
 *
 * @param[in] x Sample vector 1
 * @param[in] y Sample vector 2
 * @param[in] nx Length of \p x in samples
 * @param[in] ny Length of \p y in samples
 * @param[out] dtw DTW matrix (capacity \p nx * \p ny)
 */
void dtw_calc(flt32_t *x, flt32_t *y, int32_t nx, int32_t ny, flt32_t *dtw)
{
    int32_t i, j;
    flt32_t c, tmp[3];
    int32_t imin;

    dtw[0] = 0.0f;

    for (i=1; i < nx; ++i)
    {
        dtw[IDX(0, i, nx)] = dtw[IDX(0, i - 1, nx)] + dtw_dist(x[i], y[0]);
    }

    for (i=1; i < ny; ++i)
    {
        dtw[IDX(i, 0, nx)] = dtw[IDX(i - 1, 0, nx)] + dtw_dist(x[0], y[i]);
    }

    for (i=1; i < nx; ++i)
    {
        for (j=1; j < ny; ++j)
        {
            tmp[0] = dtw[IDX(j, i - 1, nx)];
            tmp[1] = dtw[IDX(j - 1, i, nx)];
            tmp[2] = dtw[IDX(j - 1, i - 1, nx)];

            stat_min(tmp, 3, &imin);

            c = dtw_dist(x[i], y[j]);

            dtw[IDX(j, i, nx)] = c + tmp[imin];
        }
    }
}

/**
 * @brief Performs the optimal DTW path backtracing
 *
 * @param[in] x Sample vector 1
 * @param[in] y Sample vector 2
 * @param[in] nx Length of \p x in samples
 * @param[in] ny Length of \p y in samples
 * @param[in] dtw DTW matrix (capacity \p nx * \p ny)
 * 
 * @return Cumulative optimal path distance
 */
flt32_t dtw_backtrace(flt32_t *x, flt32_t *y, int32_t nx, int32_t ny, flt32_t *dtw)
{
        flt32_t tmp[3], d;
        int32_t i = nx-1, j = ny-1;
        int32_t imin;

        d = dtw_dist(x[i], y[j]);

        while ((i > 0) || (j > 0))
        {
                if (0 == i)
                {
                        j--;
                }
                else if (0 == j)
                {
                        i--;
                }
                else
                {
                        tmp[0] = dtw[IDX(j, i - 1, nx)];
                        tmp[1] = dtw[IDX(j - 1, i, nx)];
                        tmp[2] = dtw[IDX(j - 1, i - 1, nx)];

                        stat_min(tmp, 3, &imin);

                        switch(imin)
                        {
                        case 0:
                                i--;
                                break;

                        case 1:
                                j--;
                                break;

                        case 2:
                                i--;
                                j--;
                                break;
                        }   
                }
                d += dtw_dist(x[i], y[j]);
        }
        return d;
}

/**
 * @}
 */
