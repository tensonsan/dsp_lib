
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Utility functions
 *
 **********************************************************************/

 /**
  * @addtogroup utils Utility functions
  * @{
  */

#include <stdlib.h>
#include "utils.h"

/**
 * @brief Calculates a random number, which lies within an interval [\p min, \p max]
 *
 * @note: 1 + \p max - \p min must be less than RAND_MAX
 *
 * @param[in] min Lower random number bound
 * @param[in] max Upper random number bound
 *
 * @return Random number
 */
int32_t rand_interval(int32_t min, int32_t max)
{
    int32_t r;
    int32_t range = 1 + max - min;        // random number range
    int32_t buckets = RAND_MAX / range;   // number of buckets
    int32_t limit = buckets * range;      // last bucket limit

    // hit one bucket or try again
    do
    {
        r = rand();
    } while (r >= limit);

    return min + (r / buckets);
}

/**
 * @brief Performs in-place sorting of values in \p x using shellsort algorithm
 *
 * @note Values are ascending with rising indices of \p x
 *
 * @param[in,out] x On entry unsorted array and sorted array on exit
 * @param[in] n Length of \p x
 */
void shellsort(flt32_t *x, int32_t n)
{
    int32_t i, j, k, nh = n / 2;
    flt32_t tmp;

    for (i = nh; i > 0; i = i / 2)
    {
        for (j = i; j < n; j++)
        {
            for(k = j - i; k >= 0; k = k - i)
            {
                if (x[k + i] < x[k])
                {
                    SWAP(x[k], x[k + i], tmp);
                }
                else
                {
                    break;
                }
            }
        }
    }
}

/**
 * @}
 */
