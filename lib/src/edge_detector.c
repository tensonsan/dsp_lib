
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Edge detector API
 *
 **********************************************************************/

/**
 * @addtogroup EdgeDetector Edge detector
 * @{
 */

#include "edge_detector.h"

/**
 * @brief Initializes the edge detection
 *
 * @note To start edge detection call \ref ed_rearm
 *
 * @param[in] hnd Edge detector handle
 * @param[in] threshold Percentage of the maximal signal value
 * @param[in] onset Threshold onset
 */
void ed_init(edge_detector_t *hnd, flt32_t threshold, int32_t onset)
{
    hnd->state = ed_idle;
    hnd->count = 0;
    hnd->onset = onset;
    hnd->threshold = threshold;
}

/**
 * @brief Performs the edge detection
 *
 * @note Function tracks the short term maximal signal value, producing the global maximal value.
 *       In parallel it checks if the short term maximal signal value falls below the predefined 
 *       percentage of the global maximal value. Once the edge was detected the detector needs to 
 *       be restarted with the \ref ed_rearm. The Edge detector state can be read with the \ref ed_state 
 *       function.
 *
 * @param[in] hnd Edge detector handle 
 * @param[in] x Sample value
 * @param[in] active Signal below minimal threshold (optional pre-processing VAD output), set to 1 if no VAD is used
 *
 */
void ed_process(edge_detector_t *hnd, flt32_t x, int32_t active)
{
    flt32_t threshold;
    
    switch (hnd->state)
    {
        case ed_idle:
            // Waiting for the start condition
            if (hnd->start)
            {
                hnd->start = 0;
                hnd->max = 0.0f;
                hnd->count = 0;
                hnd->state = ed_waiting;
            }
            break;
            
        case ed_waiting:
            if (hnd->start)
            {
                hnd->state = ed_idle;
                break;
            }

            if (0 == active)
            {
                break;
            }
            
            // Track the maximal signal value
            if (x > hnd->max)
            {
                hnd->max = x;
            }
            
            // Calculate the low signal threshold
            threshold = hnd->max * hnd->threshold;
            if (x < threshold)
            {
                hnd->count++;
            }
            else
            {
                hnd->count = 0;
            }

            // Edge detected, wait until restart
            if (hnd->count >= hnd->onset)
            {
                hnd->state = ed_done;
            }
            break;
            
        case ed_done:            
            // Edge detector finished
            if (hnd->start)
            {
                hnd->state = ed_idle;
            }
            break;
    }
}

/**
 * @brief Returns edge detector peak value
 *
 * @param[in] hnd Edge detector handle 
 *
 * @return Edge detector peak value
 */
flt32_t ed_peak_get(edge_detector_t *hnd)
{
    return hnd->max;
}

/**
 * @brief Returns the current edge detection state
 *
 * @param[in] hnd Edge detector handle 
 *
 * @return ed_state_t Edge detection state
 */
ed_state_t ed_state(edge_detector_t *hnd)
{
    return hnd->state;
}

/**
 * @brief Restarts the edge detector
 *
 * @param[in] hnd Edge detector handle 
 */
void ed_rearm(edge_detector_t *hnd)
{
    hnd->start = 1;
}

/**
 * @}
 */
