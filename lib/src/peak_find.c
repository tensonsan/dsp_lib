
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Peak search API
 *
 **********************************************************************/

 /**
 * @addtogroup PeakSearch Peak Search API
 * @{
 */

#include <math.h>
#include "peak_find.h"

/// Epsilon to decide if a value is near zero
#define PEAK_FIND_EPS   0.0000001f

/**
 * @brief Searches for peak within an array
 *
 * @param[in] x Input array
 * @param[in] imin Lower \p x search index
 * @param[in] imax Upper \p x search index
 *
 * @return peak index
 */
int32_t peak_find(flt32_t *x, int32_t imin, int32_t imax)
{
    int32_t i, idx = -1;
    flt32_t max = -FLT32_MAX;

    for (i=imin; i <= imax; ++i)
    {
        if (x[i] > max)
        {
            max = x[i];
            idx = i;
        }
    }
    return idx;
}

/**
 * @brief Searches for \p m largest peaks in an array
 *
 * @note Function modifies the input array (make a copy prior calling this function to preserve the original)
 *
 * @param[in] x Input array
 * @param[in] n Length of \p x in samples
 * @param[in] m Number of peaks
 * @param[in] bw Peak bandwitdh (number of indices)
 * @param[out] peaks Pointer to array of peaks
 */
void peak_find_n(flt32_t *x, int32_t n, int32_t m, int32_t bw, peak_t *peaks)
{
    int32_t i, j, idx;
    
    // Search for m peaks
    for (i=0; i < m; ++i)
    {
        // Invalidate peak
        peaks[i].val = 0.0f;
        peaks[i].pos = -1;
        
        // Search for the largest peak in an array
        for (j=0; j < n; ++j)
        {
            if (x[j] > peaks[i].val)
            {
                peaks[i].pos = j;
                peaks[i].val = x[j];
            }
        }
        
        // Remove the peak and its surrounding points from an array
        for (j=0; j < bw; ++j)
        {
            idx = j + peaks[i].pos - bw / 2;
            if ((idx > 0) && (idx < n))
            {
                x[idx] = 0.0f;
            }
        }
    }
}

/**
 * @brief Finds local extrema in input array 
 *
 * @note Array boundaries aren't considered as extrema.
 * @note Extremas lie at the mid of saddles (if any).
 *
 * @param[in] y Input array
 * @param[out] xmin Indices of local minima
 * @param[out] nmin Number of minima
 * @param[out] xmax Indices of local maxima
 * @param[out] nmax Number of maxima
 * @param[in] n Length of \p y in samples
 */
void peak_find_all(flt32_t *y, int32_t *xmin, int32_t *nmin, int32_t *xmax, int32_t *nmax, int32_t n)
{
    int32_t j, i, s1 = 0, s2 = 0, kmin = 0, kmax = 0;
    flt32_t h, ht;

    for (i=0; i < n - 1; ++i)
    {
        // derivative
        h = y[i+1] - y[i];

        if (0 == s1)
        {
            // positive slope
            if (h > 0.0f)
            {
                s1 = 1;
            }
        }
        else
        {
            // negtive slope
            if (h < 0.0f)
            {
                // count saddle points if any
                int32_t k = 1;
                for (j=i; j > 0; --j)
                {
                    ht = y[j] - y[j-1];
                    if (PEAK_FIND_EPS < fabsf(ht))
                    { 
                        break;
                    }
                    k++;
                }
                // maxima at the mid of the saddle
                xmax[kmax++] = i - k / 2;
                s1 = 0;
            }
        }

        if (0 == s2)
        {
            // negative slope
            if (h < 0.0f)
            {
                s2 = 1;
            }
        }
        else
        {
            // positive slope
            if (h > 0.0f)
            {
                // count saddle points if any
                int32_t k = 1;
                for (j=i; j > 0; --j)
                {
                    ht = y[j] - y[j-1];
                    if (PEAK_FIND_EPS < fabsf(ht))
                    {
                        break;
                    }
                    k++;
                }
                // minima at the mid of the saddle
                xmin[kmin++] = i - k / 2;
                s2 = 0;
            }
        }
    }

    *nmax = kmax;
    *nmin = kmin;
}


/**
 * @}
 */
