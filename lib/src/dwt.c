
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Discrete Wavelet Transform API
 *
 **********************************************************************/

/**
 * @addtogroup DWT Discrete Wavelet Transform
 * @{
 */

#include "dwt.h"

/**
 * @brief Calculate the discrete wavelet transform
 *
 * @note Approximation and detail coefficients are returned in the 
 *       the first and the second half of the \p y respectively
 *       Periodic boundaries are used.
 * 
 * @param[in] x Input array
 * @param[out] y Output array
 * @param[in] n Length of \p x and \p y in samples (must be power of 2) 
 * @param[in] lo Array of filter coefficients for approximation
 * @param[in] hi Array of filter coefficients for detail
 * @param[in] m Length of \p lo and \p hi
 *
 */
void dwt_periodic(flt32_t *x, flt32_t *y, int32_t n, const flt32_t *lo, const flt32_t *hi, int32_t m)
{
    int32_t i, j, idx1, idx2, nh, mh;

    nh = n / 2;
    mh = m /  2;

    for (i=0; i < nh; ++i)
    {
        y[i] = 0.0f;
        y[i + nh] = 0.0f;

        for (j=0; j < m; ++j)
        {
            // Sample index - decimation by 2
            idx1 = 2*i + j + 1;

            // Coefficient index - convolution
            idx2 = m - j - 1;

            // Periodic boundaries
            if (idx1 < mh)
            {
                idx1 = idx1 + n - mh;
            }
            else if (idx1 > n + mh - 1)
            {
                idx1 = idx1 - n - mh;
            }
            else
            {
                idx1 = idx1 - mh;
            }

            // Approximation
            y[i] += x[idx1] * lo[idx2];

            // Detail
            y[i + nh] += x[idx1] * hi[idx2];
        }
    }
}

/**
 * @brief Calculate the inverse discrete wavelet transform
 *
 * @note Approximation and detail coefficients must be provided in the 
 *       the first and the second half of the \p x respectively.
 *       Periodic boundaries are used.
 *
 * @param[in] x Input array
 * @param[out] y Output array
 * @param[in] n Length of \p x and \p y in samples (must be power of 2) 
 * @param[in] lo Array of filter coefficients for approximation
 * @param[in] hi Array of filter coefficients for detail
 * @param[in] m Length of \p lo and \p hi
 *
 */
void idwt_periodic(flt32_t *x, flt32_t *y, int32_t n, const flt32_t *lo, const flt32_t *hi, int32_t m)
{
    int32_t i, j, idx1, nh, mh;

    nh = n / 2;
    mh = m / 2;

    for (i=0; i < n; ++i)
    {
        y[i] = 0.0f;
    }

    // Convolution
    for (i=0; i < nh; ++i)
    {
        for (j=0; j < m; ++j)
        {
            // Output buffer index 
            idx1 = 2*i + j + 1;
            
            // Periodic boundaries
            if (idx1 < mh)
            {
                idx1 = idx1 + n - mh;
            }
            else if (idx1 > n + mh - 1)
            {
                idx1 = idx1 - n - mh;
            }
            else
            {
                idx1 = idx1 - mh;
            }

            // Combine approximation and detail 
            y[idx1] += x[i] * lo[j] + x[i + nh] * hi[j];
        }
    }
}

/**
 * @}
 */
