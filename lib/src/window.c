
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Signal windowing API
 *
 **********************************************************************/

/**
 * @addtogroup SigWin Signal windowing
 * @{
 */

#include <math.h>
#include "window.h"
#include "lut.h"

/**
 * @brief Performs the in-place Hann windowing of a real signal
 *
 * \f$ w(k) = \frac{1}{2}\left(1 - \cos \left( \frac{2 \pi k}{n} \right) \right) \f$
 *
 * @note The precomputed twiddle factor lookup table must be provided
 *
 * @param[in,out] x Input, output array of real values (capacity \p n)
 * @param[in] n Length of \p x (must be power of 2)
 *
 */
void win_hann(flt32_t *x, int32_t n)
{
    int32_t i, j, offset;
    flt32_t *pcos;

    // Get pointer to cosine LUT
    lut_cos_ptr(&pcos);

    offset = LUT_LEN / n;
    j = 0;

    // Perform windowing
    for (i=0; i < n; ++i)
    {
        x[i] = x[i] * 0.5f * (1.0f - pcos[j]);
        j += offset;
    }
}

/**
 * @brief Performs the in-place Hamming windowing of a real signal
 *
 * \f$ w(k) = 0.54 - 0.46\cos \left( \frac{2 \pi k}{n} \right) \f$
 *
 * @note The precomputed twiddle factor lookup table must be provided
 *
 * @param[in,out] x Input, output array of real values (capacity \p n)
 * @param[in] n Length of \p x (must be power of 2)
 */
void win_hamming(flt32_t *x, int32_t n)
{
    int32_t i, j, offset;
    flt32_t *pcos;

    // Get pointer to cosine LUT
    lut_cos_ptr(&pcos);

    offset = LUT_LEN / n;
    j = 0;

    // Perform windowing
    for (i=0; i < n; ++i)
    {
        x[i] = x[i] * (0.54f - 0.46f * pcos[j]);
        j += offset;
    }
}

/**
 * @brief Performs the in-place Bartlett windowing of a real signal
 *
 * \f$ w(k) = 1 - \left| \frac{k - \frac{n}{2}}{\frac{n}{2}} \right| \f$
 *
 * @param[in,out] x Input, output array of real values (capacity \p n)
 * @param[in] n Length of \p x (must be power of 2)
 */
void win_bartlett(flt32_t *x, int32_t n)
{
    int32_t i;
    flt32_t k = 2.0f / (flt32_t)n;

    for (i=0; i < n; ++i)
    {
        x[i] = x[i] * (1.0f - fabsf((flt32_t)i * k - 1.0f));
    }
}

/**
 * @}
 */
