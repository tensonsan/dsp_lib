
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Biquad Filter API
 * 
 * @note The Biquad filter coefficients are generated using the biquad_export.py script
 *
 **********************************************************************/
 
/**
 * @addtogroup Biquad Biquad Filter
 * @{
 */

#include "biquad.h"

/**
 * @brief Initializes multistage Biquad filtering
 * 
 * \f$
 * H(z) = \prod\limits_{i=0}^{ns-1}\frac{c_{0,i} + c_{1,i} z^{-1} c_{2,i} z^{-2}}{1 + c_{4,i} z^{-1} + c_{5,i} z^{-2}}
 * \f$
 *
 * @param[in] hnd Biquad filter handle
 * @param[in] c Precomputed coefficients of all filter stages (capacity \p nc * \p ns coefficients)
 * @param[in] s States of all filter stages (capacity \ref BIQUAD_STATES * \p ns states)
 * @param[in] nc Number of filter coefficients (single stage)
 * @param[in] ns Number of filter stages
 */
void biquad_init(biquad_t *hnd, const flt32_t *c, flt32_t *s, int32_t nc, int32_t ns)
{
    int32_t i;

    hnd->c = (flt32_t *)c;
    hnd->s = s;
    hnd->nc = nc;
    hnd->ns = ns;

    for (i=0; i < (hnd->ns * BIQUAD_STATES); ++i)
    {
        hnd->s[i] = 0.0f;
    }
}

/**
 * @brief Performs multistage Biquad filtering
 *
 * @note A single biquad filter stage is computed as:
 *
 * \f$
 * s_n = x - c_4 s_{n-1} - c_5 s_{n-2}
 * \f$
 *
 * \f$
 * y_n = c_0 s_n + c_1 s_{n-1} + c_2 s_{n-2}
 * \f$
 *
 * @param[in] hnd Biquad filter handle
 * @param[in] x Input sample
 *
 * @return Filtered value
 */
flt32_t biquad_calc(biquad_t *hnd, flt32_t x)
{	
    int32_t i, c_i, s_i;
    flt32_t y;

    for (i=0; i < hnd->ns; ++i)
    {
        c_i = i * hnd->nc;
        s_i = i * BIQUAD_STATES;

        hnd->s[s_i] = x - hnd->c[c_i + 4] * hnd->s[s_i + 1] - hnd->c[c_i + 5] * hnd->s[s_i + 2];
        y = hnd->c[c_i] * hnd->s[s_i] + hnd->c[c_i + 1] * hnd->s[s_i + 1] + hnd->c[c_i + 2] * hnd->s[s_i + 2];
        
        hnd->s[s_i + 2] = hnd->s[s_i + 1];
        hnd->s[s_i + 1] = hnd->s[s_i];
        
        x = y;
    }

    return y;
}

/**
 * @}
 */
