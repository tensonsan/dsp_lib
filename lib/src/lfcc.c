
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   LFCC/LFSC API
 *
 **********************************************************************/

/**
 * @addtogroup LFxC Linear Frequency Cepstral/Spectral Coefficients
 * @{
 */

#include <math.h>
#include <string.h>
#include "lfcc.h"
#include "dct.h"
#include "trifb.h"

/**
 * @brief Calculates n_centers equidistant filterbank center frequencies 
 *        including the first and the last stop frequency.
 *
 * @note This function must be called once before \ref lfcc_calc or \ref lfsc_calc
 *
 * @param[out] centers Array of filterbank center frequencies including the first start and the last stop frequency
 * @param[in] n_centers Length of centers array
 * @param[in] n Length \p x in samples (must be a power of 2)
 */
void lfxc_init(int32_t *centers, int32_t n_centers, int32_t n)
{
    trifb_linear(centers, n_centers, n);
}

/**
 * @brief Calculates LFCC coefficients from the FFT magnitude spectrum
 *
 * @note LFCC coefficients are provided in the first \p n_centers - 2 elements of array \p y
 *
 * @param[in] centers Array of filterbank center frequencies including first start and last stop frequency
 * @param[in] n_centers Length of centers array
 * @param[in] x Input magnitude spectrum (capacity \p n samples)
 * @param[out] y LFCC coefficients (capacity \p n samples)
 */
void lfcc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y)
{
    int32_t i;

    // Filter-bank
    trifb_calc(centers, n_centers, x, y);

    n_centers -= 2;

    // Logarithm
    for (i=0; i < n_centers; ++i)
    {
        x[i] = logf(y[i]);
    }

    // Discrete cosine transform
    dct_norm(x, y, n_centers);
}

/**
 * @brief Calculates LFSC coefficients from the FFT magnitude spectrum
 *
 * @note LFSC coefficients are provided in first \p n_centers - 2 elements of array \p y
 *
 * @param[in] centers Array of filterbank center frequencies including first start and last stop frequency
 * @param[in] n_centers Length of centers array
 * @param[in] x Input magnitude spectrum (capacity \p n samples)
 * @param[out] y LFSC coefficients (capacity \p n samples)
 *
 */
void lfsc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y)
{
    int32_t i;

    // Filter-bank
    trifb_calc(centers, n_centers, x, y);

    n_centers -= 2;

    // Logarithm
    for (i=0; i < n_centers; ++i)
    {
        y[i] = logf(y[i]);
    }
}

/**
 * @}
 */
