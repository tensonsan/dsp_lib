
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Fast Fourier Transform API
 *
 **********************************************************************/

/**
 * @addtogroup FFT Fast Fourier Transform
 * @{
 */
 
#include <string.h>
#include <math.h>
#include "utils.h"
#include "fft.h"
#include "lut.h"

/**
 * @brief Calculates the binary logarithm
 *
 * \f$
 * y = log_2(n)
 * \f$
 *
 * @param[in] n Input value (must be a power of 2)
 *
 * @return binary logarithm of \p n
 */
static int32_t fft_log2(int32_t n)
{
    int32_t res;

    for (res = 0; n >= 2; ++res)
    {
        n = n >> 1;
    }

    return res;
}

/**
 * @brief Performs an in-place bit reversal of an array
 *
 * @param[in,out] x Array of complex values (capacity \p n)
 * @param[in] n Length of \p x (must be a power of 2)
 */
static void fft_bitrev(cplx_t *x, int32_t n)
{
    int32_t n1, i, j, k;
    cplx_t xt;

    n1 = n - 1;
    j = 0;

    for (i=0; i < n1; ++i)
    {
        if (i < j)
        {
            SWAP(x[i], x[j], xt);
        }
        k = n / 2;
        
        while (k < j + 1)
        {
            j -= k;
            k /= 2;
        }
        j += k;
    }
}

/**
 * @brief Calculates the in-place radix 2 DIF complex FFT
 *
 * @note A precomputed twiddle factor lookup table must be provided
 *
 * @param[in,out] x Array of complex values (capacity \p n)
 * @param[in] type 1 for inverse FFT, 0 for forward FFT
 * @param[in] n Length of \p x (must be a power of 2)
 */
void fft(cplx_t *x, int32_t type, int32_t n)
{ 
    int32_t m, i, j, k, l, n1, n2, n3, ie, ia, offset;
    cplx_t xt, wt;
    flt32_t *psin, *pcos;

    lut_sin_ptr(&psin);
    lut_cos_ptr(&pcos);

    if (n == 1)
    {
        return;
    }

    m = fft_log2(n);

    if (type)
    {
        // Conjugate the imaginary part for the reverse transform
        for (i=0; i < n; ++i)
        {
            x[i].im = -x[i].im;
        }
    }

    n2 = n;

    // LUT index
    offset = LUT_LEN / n;

    // Iterative in-place Cooley-Tokey algorithm
    for (k=1; k <= m; ++k)
    {
        n1 = n2;
        n2 /= 2;
        
        ie = n / n1;
        ia = 0;

        for (j=0; j < n2; ++j)
        {
            n3 = ia * offset;
            
            wt.re = pcos[n3];
            wt.im = psin[n3];
            
            ia += ie;

            for (i=j; i < n; i += n1)
            {
                l = i + n2;

                xt.re = x[i].re - x[l].re;
                x[i].re += x[l].re;

                xt.im = x[i].im - x[l].im;
                x[i].im += x[l].im;

                x[l].re = wt.re * xt.re + wt.im * xt.im;
                x[l].im = wt.re * xt.im - wt.im * xt.re;
            }
        }
    }

    // In-place bit reversal
    fft_bitrev(x, n);

    if (type)
    {   
        // Scale the complex value and conjugate the imaginary part for the reverse transform
        for (i=0; i < n; ++i)
        {
            x[i].im /= -(flt32_t)n;
            x[i].re /= (flt32_t)n;
        }
    }
}

/**
 * @brief Calculate the spectrum as if using a full-length FFT of a real signal.
 *
 * @note A precomputed twiddle factor lookup table must be provided
 *
 * @param[in] x Input array of complex values (capacity \p n + 1)
 * @param[out] y Output array of complex values (capacity \p n + 1)
 * @param[in] n Length of \p x and \p y (must be a power of 2) 
 */
static void fft_interleave(cplx_t *x, cplx_t *y, int32_t n)
{
    int32_t i, n1, n2, offset;
    cplx_t a, b;
    flt32_t *psin, *pcos;

    lut_sin_ptr(&psin);
    lut_cos_ptr(&pcos);

    // LUT index increment
    offset = LUT_LEN / (n * 2);

    x[n].re = x[0].re;
    x[n].im = x[0].im;

    for (i=0; i < n; ++i)
    {
        n1 = n - i;

        a.re = 0.5f * (x[i].re + x[n1].re);
        a.im = 0.5f * (x[i].im - x[n1].im);

        b.re = 0.5f * (x[i].re - x[n1].re);
        b.im = 0.5f * (x[i].im + x[n1].im);

        n2 = offset * i;
        
        y[i].re = a.re + b.im * pcos[n2] - b.re * psin[n2];
        y[i].im = a.im - b.re * pcos[n2] - b.im * psin[n2];
    }

    y[n].re = x[0].re - x[0].im;
    y[n].im = 0.0f;
}

/**
 * @brief Calculate the real signal as if using a full-length IFFT of a real spectrum.
 *
 * @note A precomputed twiddle factor lookup table must be provided
 *
 * @param[in] x Input array of complex values (capacity \p n + 1)
 * @param[out] y Output array of complex values (capacity \p n)
 * @param[in] n Length of \p x and \p y (must be a power of 2) 
 */
static void fft_deinterleave(cplx_t *x, cplx_t *y, int32_t n)
{
    int32_t i, n1, n2, offset;
    cplx_t a, b;
    flt32_t *psin, *pcos;

    lut_sin_ptr(&psin);
    lut_cos_ptr(&pcos);

    // LUT index increment
    offset = LUT_LEN / (n * 2);

    for (i=0; i < n; ++i)
    {
        n1 = n - i;

        a.re = 0.5f * (x[i].re + x[n1].re);
        a.im = 0.5f * (x[i].im - x[n1].im);

        b.re = 0.5f * (x[i].re - x[n1].re);
        b.im = 0.5f * (x[i].im + x[n1].im);

        n2 = offset * i;
        
        y[i].re = a.re - b.im * pcos[n2] - b.re * psin[n2];
        y[i].im = a.im + b.re * pcos[n2] - b.im * psin[n2];
    }
}

/**
 * @brief Packs the buffer for the real FFT
 *
 * \f$
 * \Re(y) = \left \{ x_0, x_2, \ldots, x_{\frac{n}{2}} \right \}
 * \f$
 *
 * \f$
 * \Im(y) = \left \{ x_1, x_3, \ldots, x_{\frac{n}{2} - 1} \right \}
 * \f$
 *
 * @param[in] x Input array of real values (capacity \p n)
 * @param[out] y Output array of complex values (capacity \p n/2)
 * @param[in] n Length of \p x (must be power of 2)
 */
static void fft_pack(flt32_t *x, cplx_t *y, int32_t n)
{
    int32_t i, nh = n / 2;

    // re are even samples, im are odd samples
    for (i=0; i < nh; ++i)
    {
        y->re = *x++;
        y->im = *x++;
        ++y;
    }
}

/**
 * @brief Unpacks the buffer from the real IFFT
 *
 * \f$
 * y = \left \{
 *      \Re \left( x_0 \right), \Im \left( x_0 \right),
 *      \Re \left( x_1 \right), \Im \left( x_1 \right),
 *      \ldots,
 *      \Re \left( x_{\frac{n}{2}} \right), \Im \left( x_{\frac{n}{2}} \right)
 * \right \}
 * \f$
 *
 * @param[in] x Input array of complex values (capacity \p n/2)
 * @param[out] y Output array of real values (capacity \p n)
 * @param[in] n Length of \p y (must be a power of 2)
 */
static void fft_unpack(cplx_t *x, flt32_t *y, int32_t n)
{
    int32_t i, nh = n / 2;

    // re are even samples, im are odd samples
    for (i=0; i < nh; ++i)
    {
        *y++ = x->re;
        *y++ = x->im;
        ++x;
    }
}

/**
 * @brief Calculates the magnitude of the FFT
 *
 * \f$
 * y =  \frac{k}{2n}\left| x \right|
 * \f$
 *
 * @param[in] x Input array of complex values (capacity \p n)
 * @param[out] y Output array of magnitude values (capacity \p n)
 * @param[in] n Length of \p x and \p y (must be power of 2)
 * @param[in] k Magnitude scaling factor (set to 1.0f for no scaling)
 */
void fft_magnitude(cplx_t *x, flt32_t *y, int32_t n, flt32_t k)
{
    int32_t i;

    k = k / (2.0f * (flt32_t)n);

    for (i=0; i < n; ++i)
    {
        y[i] = k * sqrtf(x[i].re * x[i].re + x[i].im * x[i].im);
    }
}

/**
 * @brief Calculates the power of the FFT
 *
 * \f$
 * y =  \frac{k}{2n}\left| x \right|^2
 * \f$
 *
 * @param[in] x Input array of complex values (capacity \p n)
 * @param[out] y Output array of magnitude values (capacity \p n)
 * @param[in] n Length of \p x and \p y (must be power of 2)
 * @param[in] k Magnitude scaling factor (set to 1.0f for no scaling)
 */
void fft_power(cplx_t *x, flt32_t *y, int32_t n, flt32_t k)
{
    int32_t i;

    k = k / (2.0f * (flt32_t)n);

    for (i=0; i < n; ++i)
    {
        y[i] = k * (x[i].re * x[i].re + x[i].im * x[i].im);
    }
}

/**
 * @brief Calculates the real FFT
 *
 * @note A precomputed twiddle factor lookup table must be provided
 *
 * @param[in] x Array of real input values (capacity \p n + 2)
 * @param[out] y Array of complex output values (capacity \p n + 1)
 * @param[in] n Length of \p x and \p y (must be a power of 2)
 */
void fft_real(flt32_t *x, cplx_t *y, int32_t n)
{
    int32_t nh = n / 2;
    cplx_t *xt = (cplx_t *)x;

    // Pack data for the FFT
    fft_pack(x, y, n);
    
    // Half size complex FFT
    fft(y, 0, nh);

    // Unpack from half size FFT
    fft_interleave(y, xt, nh);

    memcpy(y, x, (n + 2) * sizeof(flt32_t));
}

/**
 * @brief Calculates the real inverse FFT
 *
 * @note A precomputed twiddle factor lookup table must be provided
 *
 * @param[in] x Array of complex input values (capacity \p n + 1)
 * @param[out] y Array of real output values (capacity \p n + 2)
 * @param[in] n Length of \p x and \p y (must be power of 2)
 */
void ifft_real(cplx_t *x, flt32_t *y, int32_t n)
{
    int32_t nh = n / 2;
    cplx_t *yt = (cplx_t *)y;

    // Pack for half size IFFT
    fft_deinterleave(x, yt, nh);

    // Half size complex IFFT calculation
    fft(yt, 1, nh);

    // Unpack data from the IFFT
    fft_unpack(yt, (flt32_t *)x, n);

    memcpy(y, x, n * sizeof(flt32_t));
}

/**
 * @brief Converts the FFT bin index into the frequency
 *
 * @param[in] bin FFT bin index
 * @param[in] n Total FFT buffer length
 * @param[in] fs Sampling frequency in Hz
 *
 * @return Frequency in Hz
 */
int32_t fft_bin2freq(int32_t bin, int32_t n, int32_t fs)
{
    int32_t f = (bin * fs) / (n - 1);

    return f;
}

/**
 * @brief Converts the frequency into the FFT bin index 
 *
 * @param[in] f Frequency in Hz
 * @param[in] n Total FFT buffer length
 * @param[in] fs Sampling frequency in Hz
 *
 * @return FFT bin index
 *
 */
int32_t fft_freq2bin(int32_t f, int32_t n, int32_t fs)
{
    int32_t bin = (f * (n - 1)) / fs;

    return bin;
}

/**
 * @}
 */
