
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Goertzel filter API
 *
 **********************************************************************/

/**
 * @addtogroup Goertzel Goertzel filter
 * @{
 */

#include <math.h>
#include "goertzel.h"
#include "utils.h"

/**
 * @brief Calculates Goertzel algorithm on a real signal
 * 
 * @param[in] x Input array of real values (capacity \p n)
 * @param[out] y Output array complex value
 * @param[in] f Frequency in Hz
 * @param[in] fs Sampling frequency in Hz
 * @param[in] n Length of \p x
 */
void goertzl(flt32_t *x, cplx_t *y, int32_t f, int32_t fs, int32_t n)
{
    int32_t i;
    flt32_t w = (2.0f * M_PI * f) / fs;
    flt32_t s0, s1 = 0.0f, s2 = 0.0f;
    flt32_t rw = 2.0f * cosf(w);
    flt32_t iw = sinf(w);

    for (i=0; i < n; ++i)
    {
        s0 = x[i] + rw * s1 - s2;
        s2 = s1;
        s1 = s0;
    }

    y->re = 0.5f * rw * s1 - s2;
    y->im = iw * s1;
}

/**
 * @}
 */