
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   LMS filter API
 *
 **********************************************************************/
 
/**
 * @addtogroup LMS Least Mean Square Filter
 * @{
 */

#include <math.h>
#include "lms_filter.h"

/**
 * @brief Initializes the LMS filter
 *
 * @param[in] hnd LMS filter handle
 * @param[in] buffer LMS filter input sample buffer
 * @param[in] c LMS filter coefficients (direct FIR structure)
 * @param[in] n Length of \p c and \p buffer
 * @param[in] k LMS filter learning rate
 */
void lms_init(lms_t *hnd, flt32_t *buffer, const flt32_t *c, int32_t n, flt32_t k)
{
    int32_t i;

    fir_init(&(hnd->fir), buffer, c, n);

    // Initialize LMS filter
    hnd->k = 2.0f * k;
    hnd->err = 0.0f;

    // Reset coefficients
    for (i=0; i < hnd->fir.n; ++i)
    {
        hnd->fir.c[i] = 0.0f;
    }
}

/**
 * @brief Performs the LMS filtering
 *
 * @param[in] hnd LMS filter handle
 * @param[in] x Input value
 * @param[in] d Desired value
 *
 * @return Filtered value
 */
flt32_t lms_calc(lms_t *hnd, flt32_t x, flt32_t d)
{
    int32_t i, idx;
    flt32_t tmp, y = 0.0f;

    y = fir_calc(&(hnd->fir), x); 

    // Scaled error
    hnd->err = d - y;
    tmp = hnd->err * hnd->k;

    idx = hnd->fir.index;
    // Coefficient update
    for (i=0; i < hnd->fir.n; ++i)
    {
        hnd->fir.c[i] += hnd->fir.buffer[idx++] * tmp;
        if (idx >= hnd->fir.n)
        {
            idx = 0;
        }
    }

    return y;
}

/**
 * @}
 */
