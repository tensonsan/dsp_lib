

/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Decision Tree Classifier API
 *
 **********************************************************************/

/**
 * @addtogroup DecisionTree Decision Tree
 * @{
 */

#include <stdio.h>
#include "decision_tree.h"

/**
 * @brief Initializes the decision tree
 *
 * @param[in] hnd Decision tree handle
 * @param[in] cut_var Decision tree cut variable indices (DT_NODE_NIL if the decision tree node is a leaf) 
 * @param[in] cut_point Decision tree cut values (0.0f if the decision tree node is a leaf)
 * @param[in] children Decision tree children indices
 * @param[in] classes Decision tree classes (DT_NODE_NIL if the decision tree node isn't a leaf)
 * @param[in] n Maximal depth of a decision tree
 */
void dt_init(dt_handle_t *hnd, const int32_t *cut_var, const flt32_t *cut_point, const dt_node_t *children, const int32_t *classes, int32_t n)
{
    hnd->children = (dt_node_t *)children;
    hnd->classes = (int32_t *)classes;
    hnd->cut_point = (flt32_t *)cut_point;
    hnd->cut_var = (int32_t *)cut_var;
    hnd->n = n;
}

/**
 * @brief Performs the decision tree classification
 * 
 * @param[in] hnd Decision tree handle
 * @param[in] x Input array
 *
 * @return Class index which corresponds to the input value or \ref DT_NODE_NIL
 */
int32_t dt_process(dt_handle_t *hnd, flt32_t *x)
{
    int32_t idx, node = 0;

    while ((DT_NODE_NIL != node) && (node < hnd->n))
    {
        // Return identified class if node is a leaf
        if ((DT_NODE_NIL == hnd->children[node].left) && (DT_NODE_NIL == hnd->children[node].right))
        {
            // TODO: add delay to have constant time execution
            // measure minimal time - needed for equation
            // measure maximal time - needed for check
            // add delay and compare with maximal time measurement
            // (hnd->n - node) * delay
            // printf("\n\rnode: %d", node);
            return hnd->classes[node];
        }

        // Traverse tree based on cut points comparisons with a particular cut variables
        idx = hnd->cut_var[node];        
        if (x[idx] < hnd->cut_point[node])
        {
            node = hnd->children[node].left;
        }
        else
        {
            node = hnd->children[node].right;
        }
    }

    return DT_NODE_NIL;
}

/**
 * @}
 */
