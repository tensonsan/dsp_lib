
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Keras Neural Network API
 *
 **********************************************************************/

/**
 * @addtogroup Keras Keras Neural Network
 * @{
 */

#include <math.h>
#include "keras.h"
#include "utils.h"
#include "activation.h"

/**
 * @brief Calculates a dot product between the matrix \p w and the vector \p x.
 *
 * \f$
 * s = \mathbf{W} x
 * \f$
 *
 * @param[in] w Matrix \f$ \mathbf{W} \f$ (capacity \p m x \p n)
 * @param[in] x Input vector (capacity \p n)
 * @param[out] s Output vector (capacity \p m)
 * @param[in] n Dimension of \p w and \p x
 * @param[in] m Dimension of \p w
 */
static void keras_dot(const flt32_t *w, flt32_t *x, flt32_t *s, int32_t n, int32_t m)
{
    int32_t i, j;

    for (i=0; i < m; ++i)
    {
        flt32_t c = 0.0f, t, y, sum = 0.0f;

        for (j=0; j < n; ++j)
        {
            // s[i] += w[IDX(i, j, n)] * x[j];
            y = w[IDX(i, j, n)] * x[j] - c;
            t = sum + y;
            c = (t - sum) - y;
            sum = t;
        }

        s[i] = sum;
    }
}

/**
 * @brief Applies the activation fuction according to equation below.
 *
 * \f$
 * s_i = f  \left( {x1}_i + {x2}_i + b_i \right)
 * \f$
 *
 * @param[in] x1 Input vector (capacity \p n)
 * @param[in] x2 Input vector (capacity \p n)
 * @param[out] s Output vector (capacit \p n)
 * @param[in] b Bias vector (capacity \p n)
 * @param[in] n Length of \p x1, \p x2, \p b and \p s
 * @param[in] f Activation functon
 */
static void keras_activate2(flt32_t *x1, flt32_t *x2, flt32_t *s, const flt32_t *b, int32_t n, flt32_t (*f)(flt32_t x))
{
    int32_t i;

    for (i=0; i < n; ++i)
    {
        s[i] = f(x1[i] + x2[i] + b[i]);
    }
}

/**
 * @brief Applies the activation fuction according to equation below.
 *
 * \f$
 * s_i = f  \left( x_i + b_i \right)
 * \f$
 *
 * @param[in] x Input vector (capacity \p n)
 * @param[out] s Output vector (capacit \p n)
 * @param[in] b Bias vector (capacity \p n)
 * @param[in] n Length of \p x, \p b and \p s
 * @param[in] f Activation functon
 */
static void keras_activate1(flt32_t *x, flt32_t *s, const flt32_t *b, int32_t n, flt32_t (*f)(flt32_t x))
{
    int32_t i;

    for (i=0; i < n; ++i)
    {
        s[i] = f(x[i] + b[i]);
    }
}

/**
 * @brief Gated Recurrent Unit
 * 
 * \f$
 * z_t = \sigma \left( \mathbf{W}_z x_t + \mathbf{U}_z h_{t-1} + b_z \right)
 * \f$
 * 
 * \f$
 * r_t = \sigma \left( \mathbf{W}_r x_t + \mathbf{U}_r h_{t-1} + b_r \right)
 * \f$
 * 
 * \f$
 * hh_t = \tanh \left( \mathbf{W}_h x_t + \mathbf{U}_h \left(h_{t-1} \odot r_t \right) + b_h \right)
 * \f$
 * 
 * \f$
 * h_t = (1 - z_t) \odot hh_t + z_t \odot h_{t-1}
 * \f$
 * 
 * @note The stack used by this function is at least 16 * \p m bytes
 * 
 * @param[in] x Input vector
 * @param[out] h Output/memory vector
 * @param[in] wz Input update weights (capacity \p n * \p m)
 * @param[in] uz Recurrent update weights (capacity \p m * \p m)
 * @param[in] bz Update bias (capacity \p m)
 * @param[in] wr Input reset weights (capacity \p n * \p m)
 * @param[in] ur Recurrent reset weights (capacity \p m * \p m)
 * @param[in] br Reset bias (capacity \p m)
 * @param[in] wh Input Output weights (capacity \p n * \p m)
 * @param[in] uh Recurrent output weights (capacity \p m * \p m)
 * @param[in] bh Output bias (capacity \p m)
 * @param[in] n Input vector length 
 * @param[in] m Number of units
 */
void keras_gru(flt32_t *x,
        flt32_t *h,
        const flt32_t *wz, 
        const flt32_t *uz, 
        const flt32_t *bz, 
        const flt32_t *wr,
        const flt32_t *ur,
        const flt32_t *br,
        const flt32_t *wh,
        const flt32_t *uh,
        const flt32_t *bh,
        int32_t n,
        int32_t m)
{
    int32_t i;
    flt32_t a[m], b[m];
    flt32_t z[m], r[m];

    // z = logsig(Wz * x + Uz * h + bz) -> update
    keras_dot(wz, x, a, n, m);            // a = Wz * x
    keras_dot(uz, h, b, m, m);            // b = Uz * h
    keras_activate2(a, b, z, bz, m, act_logsig);    // z = sigmoid(a + b + bz)

    // r = logsig(Wr * x + Ur * h + br) -> reset
    keras_dot(wr, x, a, n, m);            // a = Wr * x
    keras_dot(ur, h, b, m, m);            // b = Ur * h
    keras_activate2(a, b, r, br, m, act_logsig);    // r = sigmoid(a + b + br)

    // r = r o h
    for (i=0; i < m; ++i)
    {
        r[i] = r[i] * h[i];
    }

    // r = tanh(Wh * x + Uh * r + bh)
    keras_dot(wh, x, a, n, m);            // a = Wh * x
    keras_dot(uh, r, b, m, m);            // b = Uh * r
    keras_activate2(a, b, r, bh, m, act_tanh);    // r = tanh(a + b + bh)

    // h = (1 - z) o r + z o h
    for (i=0; i < m; ++i)
    {
        h[i] = ((1.0f - z[i]) * r[i]) + (z[i] * h[i]);
    }
}

/**
 * @brief Reset internal recurrent gate states
 * 
 * @param[in] h Recurrent gate states (capacity \p m)
 * @param[in] m Length of \p h
 */
void keras_gru_reset(flt32_t *h, int32_t m)
{
    for (int32_t i=0; i < m; ++i)
    {
        h[i] = 0.0f;
    }
}

/**
 * @brief Dense Unit
 * 
 * \f$
 * y_i = f  \left( \mathbf{W} x_i + b_i \right)
 * \f$
 * 
 * @param[in] x Input vector (capacity \p n)
 * @param[out] y Output vector (capacity \p m)
 * @param[in] w Weights (capacity \p n * \p m)
 * @param[in] b Bias (capacity \p m)
 * @param[in] n Input vector length
 * @param[in] m Number of units
 */
void keras_dense(flt32_t *x,
            flt32_t *y,
            const flt32_t *w,
            const flt32_t *b,
            int32_t n,
            int32_t m)
{
    flt32_t a[m];

    // y = sigmoid(W * x + b)
    keras_dot(w, x, a, n, m);         // a = W * x
    keras_activate1(a, y, b, m, act_logsig);   // y = sigmoid(a + b)
}

/**
 * @}
 */
