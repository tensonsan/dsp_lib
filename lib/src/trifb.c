
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Triangular Filterbank API
 *
 **********************************************************************/

/**
 * @addtogroup TRIFB Triangular Filterbank
 * @{
 */

#include <math.h>
#include "trifb.h"
#include "fft.h"
#include "utils.h"

/**
 * @brief Converts the frequency \f$ f \f$ to the Mel pitch \f$ m \f$
 *
 * \f$
 * m = \log \left( 1 + \frac{f}{700} \right)
 * \f$
 *
 * @param[in] f Frequency in Hz
 *
 * @return Mel pitch which corresponds to frequency
 *
 */
static flt32_t trifb_f2mel(flt32_t f)
{
    flt32_t mel;

    mel = 1127.0f * logf(1.0f + f / 700.0f);

    return mel;
}

/**
 * @brief Converts the Mel pitch \f$ m \f$ to the frequency \f$ f \f$
 *
 * \f$
 * f = 700 \exp \left( \frac{m}{1127} - 1 \right)
 * \f$
 *
 * @param[in] m Mel pitch
 *
 * @return Frequency in Hz which corresponds to mel pitch
 *
 */
static flt32_t trifb_mel2f(flt32_t m)
{
    flt32_t f;

    f = 700.0f * (expf(m / 1127.0f) - 1.0f);

    return f;
}

/**
 * @brief Calculates n_centers equidistant filterbank center frequencies
 *        including the first and the last stop frequency.
 *
 * @param[out] centers Array of filterbank center frequencies
 * @param[in] n_centers Length of \p centers
 * @param[in] n Number of spectrum FFT bins
 *
 */
void trifb_linear(int32_t *centers, int32_t n_centers, int32_t n)
{
    int32_t i;

    for (i=0; i < n_centers; ++i)
    {
        centers[i] = (i * (n - 1)) / (n_centers - 1);
    }
}

/**
 * @brief Calculates n_centers mel spaced filterbank center frequencies
 *        including the first and the last stop frequency.
 *
 * @param[out] centers Array of filterbank center frequencies
 * @param[in] n_centers Length of \p centers
 * @param[in] n Number input spectrum of FFT bins
 * @param[in] fs Sampling frequency in Hz
 *
 */
void trifb_mel(int32_t *centers, int32_t n_centers, int32_t n, int32_t fs)
{
    int32_t i;
    flt32_t m, fc;

    m = trifb_f2mel((flt32_t)fs);

    for (i=0; i < n_centers; ++i)
    {
        fc = trifb_mel2f((i * m) / (flt32_t)(n_centers - 1));

        centers[i] = lroundf((fc * (n - 1)) / (flt32_t)fs);
    }
}

/**
 * @brief Applies the triangular filterbank to the input spectrum
 *
 * @note The pre-computed array of filterbank center frequencies (centers) must be provided
 *
 * @note For \p m triangular filterbanks, the \p n_centers must be \p m + 2 (this must also be the capacity of the \p centers array)
 *
 * @param[in] centers Array of filterbank center frequencies including the first start and the last stop frequency
 * @param[in] n_centers Length of \p centers array
 * @param[in] x Input spectrum
 * @param[out] y Output (filtered) spectrum, only \p n_centers - 2 samples are valid
 *
 */
void trifb_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y)
{
    int32_t i, j, scale, start, center, stop;

    // Process all triangular filters
    for (i=0; i < n_centers - 2; ++i)
    {
        y[i] = 0.0f;

        start = centers[i];
        center = centers[i + 1];
        stop = centers[i + 2];

        scale = center - start;

        // Convolve the spectrum with the rising edge of the triangle filter
        for (j=start; j < center; ++j)
        {
            y[i] += x[j] * (flt32_t)(j - start) / (flt32_t)scale;
        }

        scale = stop - center;

        // Convolve the spectrum with the falling edge of the triangle filter
        for (j=center; j < stop; ++j)
        {
            y[i] += x[j] * (flt32_t)(stop - j) / (flt32_t)scale;
        }
    }
}

/**
 * @brief Calculates an array of triangular filterbank coefficients
 *
 * @note Array coeff is actually a matrix of dimension \p n * (\p n_centers - 2)
 *
 * @note For \p m triangular filterbanks, the \p n_centers must be \p m + 2 (this must also be the capacity of the \p centers array)
 *
 * @param[in] centers Array of filterbank center frequencies including first start and last stop frequency
 * @param[in] n_centers Length of \p centers array
 * @param[in] n Number input spectrum of FFT bins
 * @param[out] coeff Output array of triangular filterbank coefficients (capacity \p n * (\p n_centers - 2))
 *
 */
void trifb_coeffs(int32_t *centers, int32_t n_centers, int32_t n, flt32_t *coeff)
{
    int32_t i, j, scale, start, center, stop;

    // Process all triangle filters
    for (i=0; i < n_centers - 2; ++i)
    {
        start = centers[i];
        center = centers[i + 1];
        stop = centers[i + 2];

        scale = center - start;

        // Rising edge of a triangle filter
        for (j=start; j < center; ++j)
        {
            coeff[IDX(i, j, n)] = (flt32_t)(j - start) / (flt32_t)scale;
        }

        scale = stop - center;

        // Falling edge of a triangle filter
        for (j=center; j < stop; ++j)
        {
            coeff[IDX(i, j, n)] = (flt32_t)(stop - j) / (flt32_t)scale;
        }
    }
}

/**
 * @}
 */
