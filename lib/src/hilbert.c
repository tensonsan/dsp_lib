
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Hilbert Transform API
 *
 **********************************************************************/

/**
 * @addtogroup HT Hilbert Transform
 * @{
 */

#include "hilbert.h"
#include "fft.h"

/**
 * @brief Calculates the real signal Hilbert transform 
 *
 * \f$
 * y = \mathcal{F}^{-1} \left\{ -j \mathcal{F}(x) \right\}
 * \f$
 *
 * @param[in] x Array of real input values (capacity \p n + 2 samples)
 * @param[out] y Array of real output values (capacity \p n + 2 samples) 
 * @param[in] n Length of \p x and \p y (must be a power of 2) 
 */
void hilbert(flt32_t *x, flt32_t *y, int32_t n)
{
    int32_t i, nh = n / 2;
    cplx_t *yt = (cplx_t *)y;
    cplx_t *xt = (cplx_t *)x;

    fft_real(x, yt, n);

    xt[0].re = 0.0f;
    xt[0].im = 0.0f;

    // Multiply complex spectrum with the imaginary unit
    for (i=1; i < nh; ++i)
    {
        xt[i].re = -yt[i].im;
        xt[i].im = yt[i].re;
    }

    xt[nh].re = 0.0f;
    xt[nh].im = 0.0f;

    ifft_real(xt, y, n);
}

/**
 * @}
 */
