
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   MFCC/MFSC API
 *
 **********************************************************************/

/**
 * @addtogroup MFxC Mel Frequency Cepstral/Spectral Coefficients
 * @{
 */

#include <math.h>
#include <string.h>
#include "mfcc.h"
#include "dct.h"
#include "trifb.h"

/**
 * @brief Calculates n_centers Mel filterbank center frequencies 
 *        including the first and the last stop frequency.
 *
 * @note This function must be called once before \ref mfcc_calc or \ref mfsc_calc
 *
 * @param[out] centers Array of filterbank center frequencies including the first start and the last stop frequency
 * @param[in] n_centers Length of \p centers array
 * @param[in] n Length of \p x and \p y in samples (must be a power of 2)
 * @param[in] fs Sampling frequency in Hz
 *
 */
void mfxc_init(int32_t *centers, int32_t n_centers, int32_t n, int32_t fs)
{
    trifb_mel(centers, n_centers, n, fs);
}

/**
 * @brief Calculates MFCC coefficients from the input signal
 *
 * @note MFCC coefficients are provided in first \p n_centers - 2 elements of the array \p y
 *
 * @param[in] centers Array of Mel center frequencies including first start and last stop frequency
 * @param[in] n_centers Length of \p centers
 * @param[in] x Input magnitude spectrum (capacity \p n)
 * @param[out] y MFCC coefficients (capacity \p n)
 */
void mfcc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y)
{
    int32_t i;

    // Apply filter-bank
    trifb_calc(centers, n_centers, x, y);

    n_centers -= 2;

    // Apply logarithm
    for (i=0; i < n_centers; ++i)
    {
        x[i] = logf(y[i]);
    }

    // Calculate DCT
    dct_norm(x, y, n_centers);
}

/**
 * @brief Calculates MFSC coefficients from the input signal
 *
 * @note MFSC coefficients are provided in first \p n_centers - 2 elements of the array \p y
 *
 * @param[in] centers Array of Mel center frequencies including first start and last stop frequency
 * @param[in] n_centers Length of \p centers
 * @param[in] x Input magnitude spectrum (capacity \p n)
 * @param[out] y MFSC coefficients (capacity \p n)
 */
void mfsc_calc(int32_t *centers, int32_t n_centers, flt32_t *x, flt32_t *y)
{
    int32_t i;

    // Apply filter-bank
    trifb_calc(centers, n_centers, x, y);

    n_centers -= 2;

    // Apply logarithm
    for (i=0; i < n_centers; ++i)
    {
        y[i] = logf(y[i]);
    }
}

/**
 * @}
 */
