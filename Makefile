BUILDDIR = bin

LIB_SRC_PATH = lib/src

LIB_SRC = lib/dsp_lib.c \
		$(LIB_SRC_PATH)/utils.c \
		$(LIB_SRC_PATH)/statistics.c \
		$(LIB_SRC_PATH)/fft.c \
		$(LIB_SRC_PATH)/lut.c \
		$(LIB_SRC_PATH)/acorr.c \
		$(LIB_SRC_PATH)/lpc.c \
		$(LIB_SRC_PATH)/trifb.c \
		$(LIB_SRC_PATH)/dct.c \
		$(LIB_SRC_PATH)/ann.c \
		$(LIB_SRC_PATH)/decision_tree.c \
		$(LIB_SRC_PATH)/svm.c \
		$(LIB_SRC_PATH)/gmm.c \
		$(LIB_SRC_PATH)/fir.c \
		$(LIB_SRC_PATH)/biquad.c \
		$(LIB_SRC_PATH)/cepstrum.c \
		$(LIB_SRC_PATH)/window.c \
		$(LIB_SRC_PATH)/decimator.c \
		$(LIB_SRC_PATH)/lms_filter.c \
		$(LIB_SRC_PATH)/hilbert.c \
		$(LIB_SRC_PATH)/spline.c \
		$(LIB_SRC_PATH)/peak_find.c \
		$(LIB_SRC_PATH)/dwt.c \
		$(LIB_SRC_PATH)/dtw.c \
		$(LIB_SRC_PATH)/hmm.c \
		$(LIB_SRC_PATH)/preemph.c \
		$(LIB_SRC_PATH)/lfcc.c \
		$(LIB_SRC_PATH)/mfcc.c \
		$(LIB_SRC_PATH)/spectral_subtraction.c \
		$(LIB_SRC_PATH)/edge_detector.c \
		$(LIB_SRC_PATH)/slifo.c \
		$(LIB_SRC_PATH)/keras.c \
		$(LIB_SRC_PATH)/activation.c \
		$(LIB_SRC_PATH)/goertzel.c

LIB_INC = -Ilib \
	-Ilib/inc

GEN_SRC_PATH = lib/generated

GEN_SRC = $(GEN_SRC_PATH)/biquad_lut_test.c \
	$(GEN_SRC_PATH)/dct_lut_test.c \
	$(GEN_SRC_PATH)/dt_lut_test.c \
	$(GEN_SRC_PATH)/fir_lut_decimator_16_test.c \
	$(GEN_SRC_PATH)/fir_lut_decimator_test.c \
	$(GEN_SRC_PATH)/gmm_lut_test.c \
	$(GEN_SRC_PATH)/hmm_lut_test.c \
	$(GEN_SRC_PATH)/mlp_lut_test.c \
	$(GEN_SRC_PATH)/svm_lut_test_lin.c \
	$(GEN_SRC_PATH)/svm_lut_test_rbf.c \
	$(GEN_SRC_PATH)/wavelet_lut_test_db8.c \
	$(GEN_SRC_PATH)/dense_lut_2_test.c \
	$(GEN_SRC_PATH)/gru_lut_0_test.c \
	$(GEN_SRC_PATH)/gru_lut_1_test.c \

GEN_INC = -Ilib/generated

SOURCES = $(LIB_SRC) \
			$(GEN_SRC)

INCLUDES = $(LIB_INC) \
			$(GEN_INC)

OBJECTS = $(addprefix $(BUILDDIR)/, $(addsuffix .o, $(basename $(SOURCES))))

TARGET = $(BUILDDIR)/dsp_lib.so

CC = gcc
LD = gcc

CFLAGS = -O0 -g -Wall \
	-std=c99 \
	-fPIC -Wl,--no-undefined \
	$(INCLUDES)

LDFLAGS = -shared $(CFLAGS)

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $(OBJECTS) $(LDLIBS) -lm -lc

$(BUILDDIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $< -o $@

doc:
	doxygen Doxyfile

.PHONY: tests
tests:
	cd tests && ./run_tests.sh

.PHONY: luts
luts:
	cd tests && ./generate_luts.sh

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
	rm -rf doc
