
import ctypes
import pytest
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from utils import plot_absolute_error, check_absolute_error
from fastdtw import dtw

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def gen_test_values():
    x1 = np.array([1.1, 4.4, 3.3, 2.2], dtype=ctypes.c_float)
    x2 = np.array([-3.3, -4.4, -1.1, -2.2], dtype=ctypes.c_float)
    x3 = np.array([-3.3, 4.4, -1.1, -2.2], dtype=ctypes.c_float)
    x4 = np.array([3.3, -4.4, 1.1, -2.2], dtype=ctypes.c_float)

    return [x1, x2, x3, x4]

def ma_test_signal(n):
    x = np.zeros(n, dtype=ctypes.c_float)
    
    x[int(n/5):int(2*n/5)] = 1
    x[int(3*n/5):int(4*n/5)] = -1
    
    return x

def linreg_test_signal(n):
    x = np.arange(0, n)
    y = np.sin(2.0 * np.pi * x / (n - 1))

    return x, y

def test_max(dsp_lib):
    values = gen_test_values()

    for val in values:
        v, i = dsp_lib.max(val)
        vr, ir = np.max(val), np.argmax(val) 

        assert(v == vr)
        assert(i == ir)

def test_min(dsp_lib):
    values = gen_test_values()

    for val in values:
        v, i = dsp_lib.min(val)
        vr, ir = np.min(val), np.argmin(val) 

        assert(v == vr)
        assert(i == ir)

def test_mean(dsp_lib):
    values = gen_test_values()

    for val in values:
        v = dsp_lib.mean(val)
        vr = np.mean(val)

        assert(check_absolute_error(v, vr, 1.0e-8))

def test_mad(dsp_lib):
    values = gen_test_values()

    for val in values:
        v = dsp_lib.mad(val)
        vr = np.mean(np.abs(val - np.mean(val)))

        assert(check_absolute_error(v, vr, 2.0e-7))

def test_median(dsp_lib):
    values = gen_test_values()

    for val in values:
        v = dsp_lib.median(val)
        vr = np.median(val)

        assert(v == vr)

def test_rms(dsp_lib):
    values = gen_test_values()

    for val in values:
        v = dsp_lib.rms(val)
        vr = np.sqrt(val.dot(val) / len(val))

        assert(check_absolute_error(v, vr, 3.0e-8))

def test_relvar(dsp_lib):
    values = gen_test_values()

    for val in values:
        v = dsp_lib.relvar(val)
        vr = np.var(val, ddof=1) / (np.mean(val)**2)

        assert(check_absolute_error(v, vr, 4.0e-6))

def test_mean_abs(dsp_lib):
    values = gen_test_values()

    for val in values:
        v = dsp_lib.mean_abs(val)
        vr = np.mean(np.abs(val))

        assert(check_absolute_error(v, vr, 2.0e-6))

def test_entropy(dsp_lib):
    values = gen_test_values()

    for val in values:
        val = np.abs(val) # avoid negative logarithm argument
        v = dsp_lib.entropy(val)
        vr = -np.sum(val * np.log(val))

        assert(check_absolute_error(v, vr, 1.0e-6))

def test_ma(dsp_lib):
    m = 10
    x = ma_test_signal(250)

    y = dsp_lib.ma(x, m)

    yr = np.convolve(x, np.ones(m) / m)
    yr = yr[0:len(x)]

    plot_absolute_error('test_ma.png', y, yr)

    assert(check_absolute_error(y, yr, 3e-8).all())

def test_ema(dsp_lib):
    k = 0.1
    x = ma_test_signal(250)

    y = dsp_lib.ema(x, k)

    yr = np.zeros(len(x))
    for i in range(0, len(yr)):
        yr[i] = (1 - k) * yr[i - 1] + k * x[i]

    plot_absolute_error('test_ema.png', y, yr)

    assert(check_absolute_error(y, yr, 8e-8).all())

def test_linreg(dsp_lib):
    limits = [[0, 25], [25, 50], [50, 75], [75, 100]]
    n = 100

    x, y = linreg_test_signal(n)

    cnt = 0
    for limit in limits:
        yo = dsp_lib.linreg(x, y, limit[0], limit[1]-1)
        yo = yo[limit[0]:limit[1]]

        xi = x[limit[0]:limit[1]]
        A = np.vstack([xi, np.ones(len(xi))]).T
        m, c = np.linalg.lstsq(A, y[xi], rcond=None)[0]
        yr = m * xi + c

        plot_absolute_error('test_linreg{0}.png'.format(cnt), yo, yr)
        cnt = cnt + 1

        assert(check_absolute_error(yo, yr, 2e-7).all())

def test_dtw(dsp_lib):

    x = np.array([1, 2, 2, 3, 2, 0], dtype=ctypes.c_float)
    y = np.array([0, 1, 1, 2, 3, 2, 1], dtype=ctypes.c_float)
    d = dsp_lib.dtw(x, y)

    dr, _ = dtw(x, y)

    assert(check_absolute_error(d, dr, 1e-8))