#!/bin/bash

SCRIPT_PATH='../lib/scripts'
OUT_PATH='../lib/generated/'

# FIR decimator float
python3 $SCRIPT_PATH/fir_export.py --order 29 --fs 32000 --fc 4000 --window hamming --suffix decimator_test --output $OUT_PATH

# FIR decimator int16
python3 $SCRIPT_PATH/fir_export.py --order 29 --fs 32000 --fc 4000 --window hamming --suffix decimator_16_test --fixed --output $OUT_PATH

# Biquad
python3 $SCRIPT_PATH/biquad_export.py --order 15 --fs 32000 --fc 500 --typ lowpass --suffix test --output $OUT_PATH

# MLP (Scikit learn)
python3 $SCRIPT_PATH/mlp_train.py --output $OUT_PATH/mlp.pkl
python3 $SCRIPT_PATH/mlp_export.py --input $OUT_PATH/mlp.pkl --suffix test --output $OUT_PATH

# GMM (Scikit learn)
python3 $SCRIPT_PATH/gmm_train.py --output $OUT_PATH/gmm
python3 $SCRIPT_PATH/gmm_export.py --input $OUT_PATH/gmm1.pkl --suffix test --output $OUT_PATH

# SVM linear kernel (Scikit learn)
python3 $SCRIPT_PATH/svm_train.py --kernel linear --output $OUT_PATH/svm_linear.pkl
python3 $SCRIPT_PATH/svm_export.py --input $OUT_PATH/svm_linear.pkl --suffix test_lin --output $OUT_PATH

# SVM RBF kernel (Scikit learn)
python3 $SCRIPT_PATH/svm_train.py --kernel rbf --output $OUT_PATH/svm_rbf.pkl
python3 $SCRIPT_PATH/svm_export.py --input $OUT_PATH/svm_rbf.pkl --suffix test_rbf --output $OUT_PATH

# DT (Scikit learn)
python3 $SCRIPT_PATH/decision_tree_train.py --output $OUT_PATH/dt.pkl
python3 $SCRIPT_PATH/decision_tree_export.py --input $OUT_PATH/dt.pkl --suffix test --output $OUT_PATH

# HMM (hmmlearn)
python3 $SCRIPT_PATH/hmm_train.py --output $OUT_PATH/hmm.pkl
python3 $SCRIPT_PATH/hmm_export.py --input $OUT_PATH/hmm.pkl --suffix test --output $OUT_PATH

# DWT
python3 $SCRIPT_PATH/wavelet_export.py --suffix test --wavelet db8 --output $OUT_PATH

# DCT
python3 $SCRIPT_PATH/dct_export.py --length 512 --suffix test --output $OUT_PATH

# Keras
python3 $SCRIPT_PATH/keras_train.py --model $OUT_PATH/model.json --weights $OUT_PATH/model.h5
python3 $SCRIPT_PATH/keras_export.py --model $OUT_PATH/model.json --weights $OUT_PATH/model.h5 --suffix test --output $OUT_PATH