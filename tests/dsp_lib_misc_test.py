import pytest
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks, hilbert, filtfilt
from utils import read_audio, check_absolute_error
import operator
from collections import OrderedDict
import os

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def test_peak_find_all(dsp_lib):

    # test signal
    y = np.array([1.0, 2.0, 2.0, 2.0, 1.0, 3.0, 1.0, -1.0, -2.0, -2.0, -2.0, -2.0, 3.0, 2.0, 3.0, 2.0, 3.0])

    # reference_algorithm
    xmax_r, _ = find_peaks(y)
    xmin_r, _ = find_peaks(-1 * y)

    # dsplib
    xmin, xmax = dsp_lib.peak_find_all(y)

    # graphs
    plt.plot(y)
    plt.plot(xmin, y[xmin], 'o')
    plt.plot(xmax, y[xmax], 'o')
    plt.plot(xmin_r, y[xmin_r], '+')
    plt.plot(xmax_r, y[xmax_r], '+')
    plt.legend(['signal', 'dsplib min', 'dsplib max', 'python min', 'python max'])
    plt.grid()

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', 'test_peak_find_all.png'))
    plt.close()

    assert(check_absolute_error(xmin, xmin_r, 1e-9).all())
    assert(check_absolute_error(xmax, xmax_r, 1e-9).all())

def test_peak_find(dsp_lib):

    # test signal
    x = read_audio('audio/audio1.wav',  int(11000), 512, True)
    y = np.abs(np.fft.rfft(x))[:20]

    # reference algorithm
    pi, _ = find_peaks(y)
    du = dict(zip(pi, y[pi]))
    ds = OrderedDict(sorted(du.items(), key=operator.itemgetter(1), reverse=True))

    # dsplib
    px, py = dsp_lib.peak_find(x=y, m=5, bw=4)

    # graphs
    plt.plot(y)
    plt.plot(px, py, 'o')
    plt.plot(pi, y[pi], '+')
    plt.legend(['signal', 'dsplib', 'python'])
    plt.grid()

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', 'test_peak_find.png'))
    plt.close()

    assert(check_absolute_error(px, list(ds.keys()), 1e-9).all())
    assert(check_absolute_error(py, list(ds.values()), 7e-7).all())

def test_edge_detector(dsp_lib):

    # test signal
    x = read_audio('audio/audio2.wav', int(1e5), int(5e5))
    m = 1024
    b = np.ones(m) / m
    e = 2.0 * filtfilt(b, 1, np.abs(hilbert(x)))

    # reference algorithm
    imax, _ = find_peaks(e, prominence=0.1, distance=(1e4))
    ied_r = []
    for i in imax:
        j = i
        while not e[j] < 0.3 * e[i]:
            j = j + 1
        ied_r.append(j)

    # dsplib
    y = dsp_lib.edge_detector(x=e, threshold=0.3, onset=1, n_skip=10000)
    ied = np.where(y > 0.0)[0]
    ied = ied[1:] - 1 # -1 is to compensate for onset latency

    # graphs
    plt.plot(e)
    plt.plot(ied, e[ied], 'o')
    plt.plot(ied_r, e[ied_r], '+')
    plt.legend(['signal', 'dsplib', 'python'])
    plt.grid()

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', 'test_edge_detector.png'))
    plt.close()

    assert(check_absolute_error(ied, ied_r, 1e-9).all())