
import ctypes
import numpy as np
import scipy.io.wavfile as wavfile
import matplotlib.pyplot as plt
import os

def check_absolute_error(got, expected, tol, show=True):

    err = np.abs(got - expected)

    if show:
        print(np.max(err))

    return err < tol

def plot_absolute_error(filename, got, expected, show=False):

    plt.subplot(2, 1, 1)
    plt.plot(got)
    plt.plot(expected, '--')
    plt.grid()
    plt.legend(['dsplib', 'python'])

    plt.title(filename)

    plt.subplot(2, 1, 2)
    plt.plot(got - expected)
    plt.grid()
    plt.legend(['abs error'])

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', filename))

    if show:
        plt.show()

    plt.close()

def read_audio(filename, offset=0, length=-1, window=False):

    _, x = wavfile.read(filename)

    x = np.array(x, dtype=ctypes.c_float)
    x = x - np.mean(x)	        # remove DC component
    x = x / np.max(np.abs(x))   # normalize to [-1, 1] range

    x = x[offset:offset + length]

    if window:
        w = np.hanning(length)
        x = x * w

    return x

def write_c_array_flt32(f, name, data):
    f.write(name + " = \n{\n")

    n = len(data)
    for i in range(0, n):
        if i == n-1:
            f.write("  {}f\n".format(data[i]))
        else:
            f.write("  {}f,\n".format(data[i]))

    f.write("};\n\n")

def write_c_array_int32(f, name, data):
    f.write(name + " = \n{\n")

    n = len(data)
    for i in range(0, n):
        if i == n-1:
            f.write("  {}\n".format(data[i]))
        else:
            f.write("  {},\n".format(data[i]))

    f.write("};\n\n")

def write_c_array_int32_touple(f, name, data1, data2):
    f.write(name + " = \n{\n")

    n = len(data1)
    for i in range(0, n):
        if i == n-1:
            f.write("  {" + "{}".format(data1[i]) + ", {}".format(data2[i]) + "}\n")
        else:
            f.write("  {" + "{}".format(data1[i]) + ", {}".format(data2[i]) + "},\n")

    f.write("};\n\n")

def float_to_int16(x):

    y = np.zeros(len(x))

    idx = np.argwhere(x < 0)
    y[idx] = np.round(x[idx] * 32768)

    idx = np.argwhere(x >= 0)
    y[idx] = np.round(x[idx] * 32767)

    return y.astype(ctypes.c_int16)

def int16_to_float(x):

    x = x.astype(ctypes.c_float)
    y = np.zeros(len(x))

    idx = np.argwhere(x < 0)
    y[idx] = x[idx] / 32768

    idx = np.argwhere(x >= 0)
    y[idx] = x[idx] / 32767

    return y

def trifb_lin(n_fft, n_filters):

    n_centers = n_filters + 2
    centers = np.zeros(n_centers, dtype=int)
    for i in range(0, n_centers):
        centers[i] = (i * (n_fft - 1)) / (n_centers - 1)

    w = np.zeros((n_filters, n_fft))
    for i in range(0, n_filters):
        start = centers[i]
        center = centers[i + 1]
        stop = centers[i + 2]

        scale = center - start

        for j in range(start, center):
            w[i, j] = (j - start) / float(scale)

        scale = stop - center

        for j in range(center, stop):
            w[i, j] = (stop - j) / float(scale)

    return w

def segment_process(x, nseg, noverlap, cb, args):
    n = len(x)

    assert(nseg > noverlap)

    if n < nseg:
        cb(x, args)
        return

    step = nseg - noverlap

    j = 0
    while j + nseg < n:
        cb(x[j:j+nseg], args)
        j = j + step
