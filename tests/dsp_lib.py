
import ctypes
import numpy as np
from utils import float_to_int16, int16_to_float

class DSPLib:
    def __init__(self, path="../bin/dsp_lib.so"):
        self.lib = ctypes.CDLL(path)

    def max(self, x):
        n = len(x)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)
        c_v = ctypes.c_float(0)
        c_i = ctypes.c_int32(-1)

        self.lib.dsp_lib_max(c_x, c_n, ctypes.byref(c_v), ctypes.byref(c_i))

        return c_v.value, c_i.value

    def min(self, x):
        n = len(x)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)
        c_v = ctypes.c_float(0)
        c_i = ctypes.c_int32(-1)

        self.lib.dsp_lib_min(c_x, c_n, ctypes.byref(c_v), ctypes.byref(c_i))

        return c_v.value, c_i.value

    def mean(self, x):
        n = len(x)

        self.lib.dsp_lib_mean.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_mean(c_x, c_n) 

    def mad(self, x):
        n = len(x)

        self.lib.dsp_lib_mad.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_mad(c_x, c_n)

    def median(self, x):
        n = len(x)

        self.lib.dsp_lib_median.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_median(c_x, c_n)

    def rms(self, x):
        n = len(x)

        self.lib.dsp_lib_rms.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_rms(c_x, c_n)

    def relvar(self, x):
        n = len(x)

        self.lib.dsp_lib_relvar.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_relvar(c_x, c_n)

    def mean_abs(self, x):
        n = len(x)

        self.lib.dsp_lib_mean_abs.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_mean_abs(c_x, c_n)

    def entropy(self, x):
        n = len(x)

        self.lib.dsp_lib_entropy.restype = ctypes.c_float
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        return self.lib.dsp_lib_entropy(c_x, c_n)

    def ma(self, x, m):
        n = len(x)

        y = np.zeros(n, dtype=ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)
        c_m = ctypes.c_int32(m)

        self.lib.dsp_lib_ma(c_x, c_y, c_n, c_m)

        return y

    def ema(self, x, k):
        n = len(x)

        y = np.zeros(n, dtype=ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)
        c_k = ctypes.c_float(k)

        self.lib.dsp_lib_ema(c_x, c_y, c_n, c_k)

        return y

    def linreg(self, x, y, x1, x2):
        n = len(x)

        xi = x.astype(ctypes.c_int32)
        yi = y.astype(ctypes.c_float)
        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
        c_y = yi.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_x1 = ctypes.c_int32(x1)
        c_x2 = ctypes.c_int32(x2)
        c_n = ctypes.c_int32(n)

        yo = np.zeros(n, dtype=ctypes.c_float)
        c_yo = yo.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        self.lib.dsp_lib_linreg(c_x1, c_x2, c_y, c_x, c_yo, c_n)

        return yo

    def rfft(self, x):
        n = len(x)
        no = int(n/2) + 1

        xs = np.append(x, np.zeros(2, dtype=ctypes.c_float))
        y_re = np.zeros(n+1, dtype=ctypes.c_float)
        y_im = np.zeros(n+1, dtype=ctypes.c_float)

        c_x = xs.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y_re = y_re.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y_im = y_im.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_rfft(c_x, c_y_re, c_y_im, c_n)

        return y_re[0:no] + y_im[0:no] * 1j

    def irfft(self, x):
        n = len(x)
        ni = 2 * (n - 1)

        y = np.zeros(ni+2, dtype=ctypes.c_float)
        x_re = np.real(x).astype(dtype=ctypes.c_float)
        x_im = np.imag(x).astype(dtype=ctypes.c_float)

        c_x_re = x_re.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_x_im = x_im.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(ni)

        self.lib.dsp_lib_irfft(c_x_re, c_x_im, c_y, c_n)

        return y[0:ni]

    def bin2freq(self, bin, n, fs):

        self.lib.dsp_lib_bin2freq.restype = ctypes.c_int32
        c_bin = ctypes.c_int32(bin)
        c_n = ctypes.c_int32(n)
        c_fs = ctypes.c_int32(fs)

        return self.lib.dsp_lib_bin2freq(c_bin, c_n, c_fs)

    def freq2bin(self, f, n, fs):

        self.lib.dsp_lib_freq2bin.restype = ctypes.c_int32
        c_f = ctypes.c_int32(f)
        c_n = ctypes.c_int32(n)
        c_fs = ctypes.c_int32(fs)

        return self.lib.dsp_lib_freq2bin(c_f, c_n, c_fs)

    def dct_fft(self, x):
        n = len(x)
        
        y = np.zeros(n+2, dtype=ctypes.c_float)
        xs = np.append(x, np.zeros(2, dtype=ctypes.c_float))

        c_x = xs.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_dct_fft(c_x, c_y, c_n)

        return y[0:int(n/2)]

    def dct_direct(self, x):
        n = len(x)

        y = np.zeros(n, dtype=ctypes.c_float)
        xs = np.copy(x)

        c_x = xs.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_dct_direct(c_x, c_y, c_n)
    
        return y[0:int(n/2)]

    def dct_fast(self, x):
        n = len(x)

        y = np.zeros(n, dtype=ctypes.c_float)
        xs = np.copy(x)

        c_x = xs.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        self.lib.dsp_lib_dct_fast(c_x, c_y)

        return y[0:int(n/2)]

    def acorr_fft(self, x, m):
        n = len(x)

        xi = np.append(x, np.zeros(n, dtype=ctypes.c_float))
        y = np.zeros(2*n, dtype=ctypes.c_float)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(2*n)

        self.lib.dsp_lib_acorr_fft(c_x, c_y, c_n)

        return y[0:m]

    def acorr_direct(self, x, m):
        n = len(x)

        y = np.zeros(m+1, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)
        c_m = ctypes.c_int32(m)

        self.lib.dsp_lib_acorr_direct(c_x, c_y, c_n, c_m)

        return y[0:m]

    def cepstrum(self, x):
        n = len(x)

        xi = np.append(x, np.zeros(2, dtype=ctypes.c_float))
        y = np.zeros(n + 2, dtype=ctypes.c_float)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_cepstrum(c_x, c_y, c_n)

        return y[0:int(n/4)]

    def goertzel(self, x, f, fs):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(1, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_f = ctypes.c_int32(f)
        c_fs = ctypes.c_int32(fs)
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_goertzel(c_x, c_y, c_f, c_fs, c_n)

        return y[0]

    def hilbert(self, x):
        n = len(x)

        xi = np.append(x, np.zeros(2, dtype=ctypes.c_float))
        y = np.zeros(n + 2, dtype=ctypes.c_float)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_hilbert(c_x, c_y, c_n)

        return y[0:n]

    def fir_stream(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_fir_stream(c_x, c_y, c_n)
        
        return y

    def fir_buffer(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_fir_buffer(c_x, c_y, c_n)

        return y

    def fir_stream_fix16(self, x):
        n = len(x)

        xi = float_to_int16(x)
        y = np.zeros(n, dtype=ctypes.c_int16)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_fir_stream_fix16(c_x, c_y, c_n)

        return int16_to_float(y)

    def fir_buffer_fix16(self, x):
        n = len(x)

        xi = float_to_int16(x)
        y = np.zeros(n, dtype=ctypes.c_int16)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_fir_buffer_fix16(c_x, c_y, c_n)

        return int16_to_float(y)

    def biquad(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_biquad(c_x, c_y, c_n)

        return y

    def decimator_stream(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_decimator1(c_x, c_y, c_n)

        return y

    def decimator_buffer(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_decimator3(c_x, c_y, c_n)

        return y

    def decimator_stream_fix16(self, x):
        n = len(x)

        xi = float_to_int16(x)
        y = np.zeros(n, dtype=ctypes.c_int16)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_decimator2(c_x, c_y, c_n)

        return int16_to_float(y)

    def decimator_buffer_fix16(self, x):
        n = len(x)

        xi = float_to_int16(x)
        y = np.zeros(n, dtype=ctypes.c_int16)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_int16))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_decimator4(c_x, c_y, c_n)

        return int16_to_float(y)

    def lms(self, x, d, m, k):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)
        d = d.astype(ctypes.c_float)
        c = np.zeros(m, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_d = d.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_c = c.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        c_m = ctypes.c_int32(m)
        c_n = ctypes.c_int32(n)
        c_k = ctypes.c_float(k)

        self.lib.dsp_lib_lms(c_x, c_y, c_d, c_c, c_n, c_m, c_k)

        return y, c

    def spline_fit(self, x, y):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = y.astype(ctypes.c_float)

        b = np.zeros(n, dtype=ctypes.c_float)
        c = np.zeros(n, dtype=ctypes.c_float)
        d = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_b = b.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_c = c.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_d = d.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n-1)

        self.lib.dsp_lib_spline_fit(c_x, c_y, c_b, c_c, c_d, c_n)

        return y[0:n-1], b[0:n-1], c[0:n-1], d[0:n-1]

    def spline_interpolate(self, xt, yt, x):

        nt = len(xt)
        xt = xt.astype(ctypes.c_float)
        yt = yt.astype(ctypes.c_float)

        n = len(x)
        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_xt = xt.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_yt = yt.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_nt = ctypes.c_int32(nt-1)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_spline_interpolate(c_xt, c_yt, c_nt, c_x, c_y, c_n)

        return y

    def gmm(self, x):
        self.lib.dsp_lib_gmm.restype = ctypes.c_float

        x = x.astype(ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        y = self.lib.dsp_lib_gmm(c_x)

        return y

    def mlp(self, x):
        self.lib.dsp_lib_mlp.restype = ctypes.c_float

        x = x.astype(ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        y = self.lib.dsp_lib_mlp(c_x)

        return y

    def svm_lin(self, x):
        self.lib.dsp_lib_svm_lin.restype = ctypes.c_float

        x = x.astype(ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        y = self.lib.dsp_lib_svm_lin(c_x)

        return y

    def svm_rbf(self, x):
        self.lib.dsp_lib_svm_rbf.restype = ctypes.c_float

        x = x.astype(ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        y = self.lib.dsp_lib_svm_rbf(c_x)

        return y

    def decision_tree(self, x):
        self.lib.dsp_lib_decision_tree.restype = ctypes.c_int32

        x = x.astype(ctypes.c_float)
        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        y = self.lib.dsp_lib_decision_tree(c_x)

        return y

    def peak_find_all(self, y):
        n = len(y)

        y = y.astype(ctypes.c_float)

        xmin = np.zeros(n, dtype=ctypes.c_int32)
        nmin = 0
        c_xmin = xmin.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
        c_nmin = ctypes.c_int32(nmin)

        xmax = np.zeros(n, dtype=ctypes.c_int32)
        nmax = 0
        c_xmax = xmax.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
        c_nmax = ctypes.c_int32(nmax)

        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int32(n)

        self.lib.dsp_lib_peak_find_all(c_y, c_xmin, ctypes.byref(c_nmin), c_xmax, ctypes.byref(c_nmax), c_n)

        xmin = xmin[0:int(c_nmin.value)]
        xmax = xmax[0:int(c_nmax.value)]

        return xmin, xmax

    def peak_find(self, x, m, bw):
        n = len(x)

        xi = np.copy(x)
        xi = xi.astype(ctypes.c_float)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)
        c_bw = ctypes.c_int(bw)

        px = np.zeros(m, dtype=ctypes.c_int)
        c_px = px.ctypes.data_as(ctypes.POINTER(ctypes.c_int))

        py = np.zeros(m, dtype=ctypes.c_float)
        c_py = py.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        self.lib.dsp_lib_peak_find(c_x, c_n, c_m, c_bw, c_px, c_py)

        return px, py

    def window_hamming(self, n):
        x = np.ones(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_window_hamming(c_x, c_n)

        return x

    def window_hann(self, n):
        x = np.ones(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_window_hann(c_x, c_n)

        return x

    def window_bartlett(self, n):
        x = np.ones(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_window_bartlett(c_x, c_n)

        return x

    def lpc(self, x, m):
        n = len(x)
        a = np.zeros(m+1, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_a = a.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_e = ctypes.c_float(0)
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_lpc(c_x, c_a, ctypes.byref(c_e), c_n, c_m)

        return a, c_e.value

    def lpc_freq(self, x, m):
        n = len(x)
        xi = np.append(x, np.zeros(2, dtype=ctypes.c_float))
        y = np.zeros(n + 2, dtype=ctypes.c_float)

        c_x = xi.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_lpc_freq(c_x, c_y, c_n, c_m)

        return y[0:int(n/2)]

    def lpc_prediction(self, x, m):
        n = len(x)

        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_lpc_prediction(c_x, c_y, c_n, c_m)

        return y

    def lpcc(self, a, g, m):
        a = a.astype(ctypes.c_float)
        c = np.zeros(m+1, dtype=ctypes.c_float)

        c_a = a.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_c = c.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_g = ctypes.c_float(g)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_lpcc(c_a, c_c, c_g, c_m)

        return c

    def trifb_lin(self, n, m):
        c = np.zeros(n * m, dtype=ctypes.c_float)

        c_c = c.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m + 2)

        self.lib.dsp_lib_trifb_lin(c_m, c_n, c_c)

        c1 = np.resize(c, (m, n))

        return c1

    def trifb_mel(self, n, m, fs):
        c = np.zeros(n * m, dtype=ctypes.c_float)

        c_c = c.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m + 2)
        c_fs = ctypes.c_int(fs)

        self.lib.dsp_lib_trifb_mel(c_m, c_n, c_fs, c_c)

        c1 = np.resize(c, (m, n))

        return c1

    def dwt_periodic(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_dwt_periodic(c_x, c_y, c_n)

        return y

    def idwt_periodic(self, x):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_idwt_periodic(c_x, c_y, c_n)

        return y

    def dtw(self, x, y):
        nx = len(x)
        ny = len(y)

        self.lib.dsp_lib_dtw.restype = ctypes.c_float
        x = x.astype(ctypes.c_float)
        y = y.astype(ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_nx = ctypes.c_int(nx)
        c_ny = ctypes.c_int(ny)

        return self.lib.dsp_lib_dtw(c_x, c_nx, c_y, c_ny)

    def hmm_viterbi(self, o):
        t = len(o)

        self.lib.dsp_lib_hmm_viterbi.restype = ctypes.c_float
        o = o.astype(ctypes.c_int)

        c_o = o.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        c_t = ctypes.c_int(t)

        logl = self.lib.dsp_lib_hmm_viterbi(c_o, c_t)

        return logl

    def preemph(self, x, k):
        n = len(x)

        y = np.copy(x)

        y = y.astype(ctypes.c_float)
        c_x = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_k = ctypes.c_float(k)

        self.lib.dsp_lib_preemph(c_x, c_n, c_k)

        return y

    def preemph_dynamic(self, x):
        n = len(x)

        y = np.copy(x)

        y = y.astype(ctypes.c_float)
        c_x = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)

        self.lib.dsp_lib_preemph_dynamic(c_x, c_n)

        return y

    def lfcc(self, x, m):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_lfcc(c_x, c_y, c_n, c_m)

        return y[0:m]

    def lfsc(self, x, m):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_lfsc(c_x, c_y, c_n, c_m)

        return y[0:m]

    def mfcc(self, x, m, fs):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)
        c_fs = ctypes.c_int(fs)

        self.lib.dsp_lib_mfcc(c_x, c_y, c_n, c_m, c_fs)

        return y[0:m]

    def mfsc(self, x, m, fs):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)
        c_fs = ctypes.c_int(fs)

        self.lib.dsp_lib_mfsc(c_x, c_y, c_n, c_m, c_fs)

        return y[0:m]

    def spectral_subtraction(self, noise, signal, nfft, k, alpha, beta, onset, threshold):

        noise = noise.astype(ctypes.c_float)
        _signal = np.copy(signal)
        _signal = _signal.astype(ctypes.c_float)

        c_signal = _signal.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_noise = noise.ctypes.data_as(ctypes.POINTER(ctypes.c_float))

        c_n_noise = ctypes.c_int(len(noise))
        c_n_signal = ctypes.c_int(len(signal))
        c_nfft = ctypes.c_int(nfft)

        c_alpha = ctypes.c_float(alpha)
        c_beta = ctypes.c_float(beta)
        c_k = ctypes.c_float(k)
        c_onset = ctypes.c_int(onset)
        c_threshold = ctypes.c_float(threshold)

        self.lib.dsp_lib_ssub(c_noise, c_n_noise, c_signal, c_n_signal, c_nfft, c_k, c_alpha, c_beta, c_onset, c_threshold)

        return _signal

    def edge_detector(self, x, threshold, onset, n_skip):
        n = len(x)

        x = x.astype(ctypes.c_float)
        y = np.zeros(n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_onset = ctypes.c_int(onset)
        c_threshold = ctypes.c_float(threshold)
        c_n_skip = ctypes.c_int(n_skip)

        self.lib.dsp_lib_edge_detector(c_x, c_y, c_n, c_threshold, c_onset, c_n_skip)

        return y

    def keras_test(self, x, m, n):
        
        x = x.astype(ctypes.c_float)
        y = np.zeros(1 * n, dtype=ctypes.c_float)

        c_x = x.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_y = y.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
        c_n = ctypes.c_int(n)
        c_m = ctypes.c_int(m)

        self.lib.dsp_lib_keras_test(c_x, c_y, c_n, c_m)

        return y
