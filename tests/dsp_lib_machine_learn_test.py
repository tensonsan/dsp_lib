
import pytest
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from sklearn import mixture
from sklearn import tree
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
import joblib
from hmmlearn import hmm
from sklearn import datasets
from utils import plot_absolute_error, check_absolute_error

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

@pytest.fixture
def dataset(scope="session"):

    bc = datasets.load_breast_cancer()

    x = bc.data
    y = bc.target

    # normalization
    x = (x - np.mean(x)) / np.std(x)

    return x, y

def test_mlp(dsp_lib, dataset):
    clf = joblib.load("../lib/generated/mlp.pkl")

    x = dataset[0]
    y = dataset[1]
    n = len(y)

    yr = clf.predict_proba(x[0:n,:])
    yr = yr[:,1]

    y = np.zeros(n)
    for i in range(0, n):
        y[i] = dsp_lib.mlp(x[i, :])

    plot_absolute_error('mlp.png', y, yr)

    assert(check_absolute_error(y, yr, 2e-7).all())

def test_gmm(dsp_lib, dataset):
    clf = joblib.load("../lib/generated/gmm1.pkl")

    x = dataset[0]
    y = dataset[1]
    n = len(y)

    yr = clf.score_samples(x[0:n,:])

    y = np.zeros(n)
    for i in range(0, n):
        y[i] = dsp_lib.gmm(x[i, :])

    plot_absolute_error('gmm.png', y, yr)

    assert(check_absolute_error(y, yr, 9e-5).all())

def test_svm_lin(dsp_lib, dataset):
    clf = joblib.load("../lib/generated/svm_linear.pkl")

    x = dataset[0]
    y = dataset[1]
    n = len(y)

    yr = clf.predict(x)

    y = np.zeros(n)
    for i in range(0, n):
        y[i] = dsp_lib.svm_lin(x[i, :])

    plot_absolute_error('svm_lin.png', y, yr)

    assert(check_absolute_error(y, yr, 1e-8).all())

def test_svm_rbf(dsp_lib, dataset):
    clf = joblib.load("../lib/generated/svm_rbf.pkl")

    x = dataset[0]
    y = dataset[1]
    n = len(y)

    yr = clf.predict(x)

    y = np.zeros(n)
    for i in range(0, n):
        y[i] = dsp_lib.svm_rbf(x[i, :])

    plot_absolute_error('svm_rbf.png', y, yr)

    assert(check_absolute_error(y, yr, 1e-8).all())

def test_decision_tree(dsp_lib, dataset):
    clf = joblib.load("../lib/generated/dt.pkl")

    x = dataset[0]
    y = dataset[1]
    n = len(y)

    yr = clf.predict(x[0:n,:])

    y = np.zeros(n)
    for i in range(0, n):
        y[i] = dsp_lib.decision_tree(x[i, :])

    plot_absolute_error('decision_tree.png', y, yr)

    assert(check_absolute_error(y, yr, 1e-8).all())

def test_hmm(dsp_lib):
    clf = joblib.load("../lib/generated/hmm.pkl")

    o = np.array([[0, 2, 1, 1, 2, 0]]).T

    llr, _ = clf.decode(o, algorithm="viterbi")

    ll = dsp_lib.hmm_viterbi(o)

    assert(check_absolute_error(ll, llr, 8e-7))
