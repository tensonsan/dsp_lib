
import pytest
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import lfilter, firwin, welch, butter, sosfilt, decimate, freqz
import utils
from utils import read_audio, plot_absolute_error, check_absolute_error
import librosa
import os

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def fir_test_signal(n):
    np.random.seed(2) # always the same noise signal
    x = np.random.normal(loc=0.0, scale=0.1, size=n)
    x = x / np.abs(np.max(x))
    return x

def lms_filter(u, d, M, step):
    N = len(u) - M + 1

    y = np.zeros(N)  # Filter output
    e = np.zeros(N)  # Error signal
    w = np.zeros(M)  # Initial filter coeffs

    # Perform filtering
    for n in range(N):
        x = np.flipud(u[n:n+M])
        y[n] = np.dot(x, w)
        e[n] = d[n+M-1] - y[n]

        w = w + step * x * e[n]
        y[n] = np.dot(x, w)

    return y, e, w

def fir_filter(nt, fc, fs, x):
    b = firwin(numtaps=nt, cutoff=fc, window='hamming', fs=fs)
    y = lfilter(b, 1.0, x)
    return y

def fir_filter_coeff(nt, fc, fs):
    b = firwin(numtaps=nt, cutoff=fc, window='hamming', fs=fs)
    return b

def biquad_filter(nt, fc, fs, x):
    sos = butter(N=nt, Wn=2 * fc / fs, btype='lowpass', output='sos')
    return sosfilt(sos, x)

def preemphasis_filter(k, x):
    b = [1.0, -k]
    return lfilter(b, 1.0, x)

def fir_frequency_response(b):
    w, h = freqz(b, 1.0, worN=512)
    return 20.0 * np.log10(abs(h))

def test_fir_stream(dsp_lib):
    x = fir_test_signal(1000)
        
    y = dsp_lib.fir_stream(x)

    yr = fir_filter(nt=30, fc=4e3, fs=32e3, x=x)

    plot_absolute_error('test_fir_stream.png', y, yr)

    assert(check_absolute_error(y, yr, 2e-7).all())

def test_fir_buffer(dsp_lib):
    x = fir_test_signal(1000)

    y = dsp_lib.fir_buffer(x)

    yr = fir_filter(nt=30, fc=4e3, fs=32e3, x=x)

    plot_absolute_error('test_fir_buffer', y, yr)

    assert(check_absolute_error(y, yr, 2e-7).all())

def test_fir_stream_fix16(dsp_lib):
    x = fir_test_signal(1000)
        
    y = dsp_lib.fir_stream_fix16(x)

    yr = fir_filter(nt=30, fc=4e3, fs=32e3, x=x)

    plot_absolute_error('test_fir_stream_fix16.png', y, yr)

    assert(check_absolute_error(y, yr, 9e-5).all())

def test_fir_buffer_fix16(dsp_lib):
    x = fir_test_signal(1000)

    y = dsp_lib.fir_buffer_fix16(x)

    yr = fir_filter(nt=30, fc=4e3, fs=32e3, x=x)

    plot_absolute_error('test_fir_buffer_fix16', y, yr)

    assert(check_absolute_error(y, yr, 9e-5).all())

def test_biquad(dsp_lib):
    x = fir_test_signal(1000)

    y = dsp_lib.biquad(x)

    yr = biquad_filter(nt=16, fc=500, fs=32e3, x=x)

    plot_absolute_error('test_biquad.png', y, yr)

    assert(check_absolute_error(y, yr, 3e-6).all())

def test_decimator_stream(dsp_lib):

    x = fir_test_signal(1024)
    q = 4

    y = dsp_lib.decimator_stream(x)
    y = y[0:int(len(x)/q)]

    yr = decimate(x, q=q, n=29, ftype='fir', zero_phase=False)

    plot_absolute_error('test_decimator_stream.png', y, yr)

    assert(check_absolute_error(y, yr, 8e-8).all())

def test_decimator_buffer(dsp_lib):

    x = fir_test_signal(1024)
    q = 4

    y = dsp_lib.decimator_buffer(x)
    y = y[0:int(len(x)/q)]

    yr = decimate(x, q=q, n=29, ftype='fir', zero_phase=False)

    plot_absolute_error('test_decimator_buffer.png', y, yr)

    assert(check_absolute_error(y, yr, 5e-8).all())

def test_decimator_stream_fix16(dsp_lib):

    x = fir_test_signal(1024)
    q = 4

    y = dsp_lib.decimator_stream_fix16(x)
    y = y[0:int(len(x)/q)]

    yr = decimate(x, q=q, n=29, ftype='fir', zero_phase=False)

    plot_absolute_error('test_decimator_stream_fix16.png', y, yr)

    assert(check_absolute_error(y, yr, 6e-5).all())

def test_decimator_buffer_fix16(dsp_lib):

    x = fir_test_signal(1024)
    q = 4

    y = dsp_lib.decimator_buffer_fix16(x)
    y = y[0:int(len(x)/q)]

    yr = decimate(x, q=q, n=29, ftype='fir', zero_phase=False)

    plot_absolute_error('test_decimator_buffer_fix16.png', y, yr)

    assert(check_absolute_error(y, yr, 6e-5).all())

def test_lms(dsp_lib):

    step = 0.01
    m = 256
    nconverged = 9000

    x = fir_test_signal(10*1024)

    d = fir_filter(nt=16, fc=4e3, fs=32e3, x=x)
    br = fir_filter_coeff(nt=16, fc=4e3, fs=32e3)

    d = d / np.max(np.abs(d))

    y, b = dsp_lib.lms(x, d, m, step)
    y = y[m:-1]

    yr, _, br = lms_filter(x, d, m, 2.0 * step)
    yr = yr[1:-1]

    plot_absolute_error('test_lms.png', y[nconverged:-1], yr[nconverged:-1])
    assert(check_absolute_error(y[nconverged:-1], yr[nconverged:-1], 7e-6).all())

    Yr = fir_frequency_response(br)
    Y = fir_frequency_response(b)

    plot_absolute_error('test_lms_freq.png', Y, Yr)
    assert(check_absolute_error(Y, Yr, 0.5).all())

def test_trifb_lin(dsp_lib):
    m = 16
    n = 256

    plt.title('trifb_lin.png')
    cr = utils.trifb_lin(n_fft=n, n_filters=m)
    plt.subplot(3,1,1)
    plt.plot(cr.T)

    plt.subplot(3,1,2)
    c = dsp_lib.trifb_lin(n, m)
    plt.plot(c.T)

    plt.subplot(3,1,3)
    plt.plot(cr.T - c.T)
    assert(check_absolute_error(c.ravel(), cr.ravel(), 3e-8).all())

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', 'trifb_lin.png'))
    plt.close()

def test_trifb_mel(dsp_lib):
    m = 16
    n = 256
    fs = 16000

    plt.title('trifb_mel.png')
    cr = librosa.filters.mel(sr=fs, n_fft=2*n-1, n_mels=m, htk=True, norm=None)
    plt.subplot(3,1,1)
    plt.plot(cr.T)

    c = dsp_lib.trifb_mel(n, m, int(fs/2))
    plt.subplot(3,1,2)
    plt.plot(c.T)

    plt.subplot(3,1,3)
    plt.plot(cr.T - c.T)
    assert(check_absolute_error(c.ravel(), cr.ravel(), 0.2).all())

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', 'trifb_mel.png'))
    plt.close()

def test_preemphasis(dsp_lib):
    k = 0.7
    x = fir_test_signal(512)

    y = dsp_lib.preemph(x, k)
    yr = preemphasis_filter(k, x)

    plot_absolute_error('test_preemph.png', y, yr)
    assert(check_absolute_error(y, yr, 7e-8).all())

def test_preemphasis_dynamic(dsp_lib):
    x = fir_test_signal(512)

    # calculate pre-emphasis dynamically
    r = np.correlate(x, x, "full")
    r = r[len(x)-1:len(x)+1]
    k = r[1] / r[0]

    y = dsp_lib.preemph_dynamic(x)
    yr = preemphasis_filter(k, x)

    plot_absolute_error('test_preemph_dynamic.png', y, yr)
    assert(check_absolute_error(y, yr, 5e-8).all())

def test_goertzel(dsp_lib):
    n = 2048
    fs = 16000
    f = np.array([1000, 1500, 3000, 3200, 5500, 7200]) # Frequencies
    ar = np.array([0.5, 0.3, 0.1, 0.7, 0.2, 0.5]) # Amplitudes
    
    # generate a signal composed of discrete tones
    x = np.zeros(n)
    w = 2.0 * np.pi * np.arange(0, n) / fs
    for i in range(len(f)):
        x = x + ar[i] * np.sin(w * f[i])

    a = []
    for fx in f:
        y = dsp_lib.goertzel(x, fx, fs)
        a.append(float(y))

    assert(check_absolute_error(a, ar, 3e-3).all())