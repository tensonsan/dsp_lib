
import ctypes
import pytest
from dsp_lib import DSPLib
import numpy as np
from scipy.fftpack import dct, hilbert
from scipy.signal import hann, find_peaks
from utils import read_audio, plot_absolute_error, check_absolute_error
import pywt

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def test_rfft(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.rfft(x)
    yr = np.fft.rfft(x)

    plot_absolute_error('test_rfft_real.png', np.real(y), np.real(yr))
    plot_absolute_error('test_rfft_imag.png', np.imag(y), np.imag(yr))

    assert(check_absolute_error(y, yr, 4e-6).all())

def test_irfft(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 512)
    X = np.fft.rfft(x)

    y = dsp_lib.irfft(X)
    yr = np.fft.irfft(X)

    plot_absolute_error('test_irfft.png', y, yr)

    assert(check_absolute_error(y, yr, 8e-8).all())

def test_fft_bin2freq_freq2bin(dsp_lib):
    n = 1024
    fs = 16000
    f = np.array([1000, 1500, 3000, 3200, 5500, 7200]) # Frequencies
    a = np.array([0.5, 0.3, 0.1, 0.7, 0.2, 0.5]) # Amplitudes
    
    # generate a signal composed of discrete tones
    x = np.zeros(n)
    w = 2.0 * np.pi * np.arange(0, n) / fs
    for i in range(len(f)):
        x = x + a[i] * np.sin(w * f[i])

    # amplitude spectrum
    X = np.abs(np.fft.rfft(x))
    X = X[0:int(n/2)] * (4.0 / n)

    # find peaks in the spectrum
    peaks, _ = find_peaks(X, height=0.05)
    
    # verify library routines
    f_peaks = []
    b_peaks = []

    for xp in peaks:
        fp = dsp_lib.bin2freq(xp, n, fs)
        f_peaks.append(fp)

        bp = dsp_lib.freq2bin(fp, n, fs)
        b_peaks.append(bp)

    assert(check_absolute_error(f, f_peaks, int(fs/n)).all())
    assert(check_absolute_error(peaks, b_peaks, 1).all)


def test_dct_fft(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.dct_fft(x)

    yr = dct(x, type=2, norm='ortho')
    yr = yr[0:int(len(x)/2)]

    plot_absolute_error('test_dct_fft.png', y, yr)

    assert(check_absolute_error(y, yr, 1e-4).all())

def test_dct_direct(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.dct_direct(x)

    yr = dct(x, type=2, norm='ortho')
    yr = yr[0:int(len(x)/2)]

    plot_absolute_error('test_dct_direct.png', y, yr)

    assert(check_absolute_error(y, yr, 2e-6).all())

def test_dct_fast(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.dct_fast(x)

    yr = dct(x, type=2, norm='ortho')
    yr = yr[0:int(len(x)/2)]

    plot_absolute_error('test_dct_fast.png', y, yr)

    assert(check_absolute_error(y, yr, 3e-8).all())

def test_acorr_fft(dsp_lib):
    m = 32
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.acorr_fft(x, m)

    n = len(x)
    yr = np.correlate(x, x, "full")
    yr = yr[n-1:n+m-1]

    plot_absolute_error('test_acorr_fft.png', y, yr)

    assert(check_absolute_error(y, yr, 3e-7).all())

def test_acorr_direct(dsp_lib):
    m = 32
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.acorr_direct(x, m)

    n = len(x)
    yr = np.correlate(x, x, "full")
    yr = yr[n-1:n+m-1]

    plot_absolute_error('test_acorr_direct.png', y, yr)

    assert(check_absolute_error(y, yr, 6e-8).all())

def test_cepstrum(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 1024)

    y = dsp_lib.cepstrum(x)

    n = len(x)
    yr = np.fft.irfft(np.log(np.abs(np.fft.rfft(x))))
    yr = yr[0:int(n/4)]

    plot_absolute_error('test_cepstrum.png', y, yr)

    assert(check_absolute_error(y, yr, 4e-7).all())

def test_hilbert(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 512)

    y = dsp_lib.hilbert(x)

    yr = hilbert(x)

    plot_absolute_error('test_hilbert.png', y, yr)

    assert(check_absolute_error(y, yr, 2e-7).all())

def test_dwt_periodic(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 1024)

    y = dsp_lib.dwt_periodic(x)

    (cA, cD) = pywt.dwt(data=x, wavelet='db8', mode='periodization')
    yr = np.concatenate((cA, cD))

    plot_absolute_error('test_dwt_periodic.png', y, yr)

    assert(check_absolute_error(y, yr, 3e-8).all())

def test_idwt_periodic(dsp_lib):
    x = read_audio('audio/audio1.wav', 0, 1024)

    (cA, cD) = pywt.dwt(data=x, wavelet='db8', mode='periodization')
    xi = np.concatenate((cA, cD))

    y = dsp_lib.idwt_periodic(xi)
    yr = pywt.idwt(cA=cA, cD=cD, wavelet='db8', mode='periodization')

    plot_absolute_error('test_idwt_periodic.png', y, yr)

    assert(check_absolute_error(y, yr, 2e-8).all())