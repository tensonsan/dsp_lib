

import pytest
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from utils import plot_absolute_error, check_absolute_error

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def test_spline_fit(dsp_lib):
    x = np.arange(11)
    y = np.sin(x)

    a, b, c, d = dsp_lib.spline_fit(x, y)

    cs = CubicSpline(x=x, y=y, bc_type='natural')

    assert(check_absolute_error(d, cs.c[0], 1e-7).all())
    assert(check_absolute_error(c, cs.c[1], 1e-7).all())
    assert(check_absolute_error(b, cs.c[2], 1e-7).all())
    assert(check_absolute_error(a, cs.c[3], 1e-7).all())

def test_spline_interpolate(dsp_lib):
    xt = np.arange(11)
    yt = np.sin(xt)

    xs = np.arange(-0.5, 11.5, 0.1)

    y = dsp_lib.spline_interpolate(xt, yt, xs)

    cs = CubicSpline(x=xt, y=yt, bc_type='natural')
    yr = cs(xs)

    assert(check_absolute_error(y, yr, 5e-7).all())
