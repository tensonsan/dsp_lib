
import pytest
from keras.datasets import imdb
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential, model_from_json
from utils import plot_absolute_error, check_absolute_error
from dsp_lib import DSPLib
import numpy as np

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def test_keras(dsp_lib):

    # Load dataset
    num_words = 2000
    maxlen = 80

    (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words = num_words)

    # pad the sequences with zeros 
    x_train = pad_sequences(x_train, maxlen = maxlen, padding = 'post')
    x_test = pad_sequences(x_test, maxlen = maxlen, padding = 'post')

    x_train = x_train.reshape(x_train.shape + (1,)).astype(dtype=float)
    x_test = x_test.reshape(x_test.shape + (1,)).astype(dtype=float)

    n_samples = 1000
    x_train = x_train[:n_samples,:]
    x_test = x_test[:n_samples,:]

    # Load model
    json_file = open('../lib/generated/model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights("../lib/generated/model.h5")

    # DSP Lib
    xi = x_test.flatten()
    y = dsp_lib.keras_test(xi, maxlen, n_samples)

    # Keras
    testPredict = model.predict(x_test)
    yr = testPredict[:,0]

    plot_absolute_error('keras.png', y, yr)
    assert(check_absolute_error(y, yr, 3e-7).all())
