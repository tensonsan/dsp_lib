#!/bin/bash
rm -rf images
#OPT="-p no:sugar -s -v"
OPT="-s"
python3 -m pytest $OPT dsp_lib_statistics_test.py
python3 -m pytest $OPT dsp_lib_transform_test.py
python3 -m pytest $OPT dsp_lib_filter_test.py
python3 -m pytest $OPT dsp_lib_window_test.py
python3 -m pytest $OPT dsp_lib_machine_learn_test.py
python3 -m pytest $OPT dsp_lib_interpolation_test.py
python3 -m pytest $OPT dsp_lib_model_test.py
python3 -m pytest $OPT dsp_lib_spectral_subtraction_test.py
python3 -m pytest $OPT dsp_lib_misc_test.py
python3 -m pytest $OPT dsp_lib_keras_test.py