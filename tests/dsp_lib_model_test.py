
import pytest
from dsp_lib import DSPLib
import ctypes
import numpy as np
import matplotlib.pyplot as mp
import scipy
import scipy.io.wavfile as wf
import copy
#from scikits.talkbox import lpc
from audiolazy import lpc
from scipy.signal import freqz
import utils
from utils import read_audio, plot_absolute_error, check_absolute_error
import librosa

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def lpc_freq(x, m):
    n = len(x)

    filt = lpc(x, m)

    a = filt.numerator
    e = filt.error / n

    _, h = freqz(np.sqrt(e), a, int(n/2))
    y = abs(h) * (n * 2)

    return y[0:int(n/2)]

def filters_lin(n_filters, n_fft, fmin, fmax, sr):
    # Initialize the weights
    n_filters = int(n_filters)
    weights = np.zeros((n_filters, int(1 + n_fft/2)))

    # Center freqs of each FFT bin
    fftfreqs = np.linspace(0, float(sr)/2, int(1 + n_fft/2), endpoint=True)

    # 'Center freqs' of mel bands - uniformly spaced between limits
    fb_f = np.linspace(fmin, fmax, n_filters)

    fdiff = np.diff(fb_f)
    ramps = np.subtract.outer(fb_f, fftfreqs)

    for i in range(n_filters):
        # lower and upper slopes for all bins
        lower = -ramps[i] / fdiff[i]
        upper = ramps[i+2] / fdiff[i+1]

        # .. then intersect them with each other and zero
        weights[i] = np.maximum(0, np.minimum(lower, upper))

    return weights

def lpcc(a, g, m):
    c = np.zeros(m+1)

    c[0] = np.log(g)

    for i in range(1, m+1):
        sum = 0.0
        for j in range(1, i):
            sum += (j - i) * a[j] * c[i - j]
            
        c[i] = sum / float(i) - a[i]

    return c

def test_lpc(dsp_lib):

    m = 64
    n = 512

    x = read_audio('audio/audio.wav', int(4e5), n, True)
    x = x.astype(ctypes.c_float)

    a, g = dsp_lib.lpc(x, m)
    filt = lpc(x, m)

    ar = filt.numerator
    er = filt.error / n

    plot_absolute_error('test_lpc.png', a, ar)

    assert(check_absolute_error(a, ar, 2e-7).all())
    assert(check_absolute_error(g, er, 1e-9))

def test_lpc_freq(dsp_lib):

    m = 64
    x = read_audio('audio/audio.wav', int(4e5), 512, True)
    x = x.astype(ctypes.c_float)

    y = dsp_lib.lpc_freq(x, m)
    yr = lpc_freq(x, m)

    plot_absolute_error('test_lpc_freq.png', y, yr)

    assert(check_absolute_error(y, yr, 3e-5).all())

def test_lpc_prediction(dsp_lib):

    m = 32
    x = read_audio('audio/audio2.wav', int(2e5), 512, True)
    x = x.astype(ctypes.c_float)

    y = dsp_lib.lpc_prediction(x, m)

    plot_absolute_error('test_lpc_prediction.png', y, x)

    assert(check_absolute_error(y, x, 0.04).all())


def test_lpcc(dsp_lib):

    m = 32
    x = read_audio('audio/audio.wav', int(4e5), 512, True)
    x = x.astype(ctypes.c_float)

    filt = lpc(x, m+1)

    a = np.array(filt.numerator)

    y = dsp_lib.lpcc(a, 1.0, m)
    yr = lpcc(a, 1.0, m)

    plot_absolute_error('test_lpcc.png', y, yr)

    assert(check_absolute_error(y, yr, 2e-8).all())

def test_mfcc(dsp_lib):

    m = 32
    n = 512
    fs = 16000
    x = read_audio('audio/audio.wav', int(4e5), n, True)
    x = x.astype(ctypes.c_float)

    S = np.abs(librosa.core.stft(y=x, n_fft=n-1, center=False))**2
    S = S[:,0]
    mfb = librosa.filters.mel(sr=fs, n_fft=n-1, n_mels=m, htk=True, norm=None)
    cr = scipy.fftpack.dct(np.log(np.dot(mfb, S)), type=2, norm='ortho')

    c = dsp_lib.mfcc(S, m, int(fs/2))

    plot_absolute_error('test_mfcc.png', c, cr)

    assert(check_absolute_error(c, cr, 0.22).all())

def test_mfsc(dsp_lib):

    m = 32
    n = 512
    fs = 16000
    x = read_audio('audio/audio.wav', int(4e5), n, True)
    x = x.astype(ctypes.c_float)

    S = np.abs(librosa.core.stft(y=x, n_fft=n-1, center=False))**2
    S = S[:,0]
    mfb = librosa.filters.mel(sr=fs, n_fft=n-1, n_mels=m, htk=True, norm=None)
    cr = np.log(np.dot(mfb, S))

    c = dsp_lib.mfsc(S, m, int(fs/2))

    plot_absolute_error('test_mfsc.png', c, cr)

    assert(check_absolute_error(c, cr, 0.52).all())

def test_lfcc(dsp_lib):

    m = 32
    n = 512
    x = read_audio('audio/audio.wav', int(4e5), n, True)
    x = x.astype(ctypes.c_float)

    S = np.abs(librosa.core.stft(y=x, n_fft=n-1, center=False))**2
    S = S[:,0]
    fb = utils.trifb_lin(int(n/2), m)
    cr = scipy.fftpack.dct(np.log(np.dot(fb, S)), type=2, norm='ortho')

    c = dsp_lib.lfcc(S, m)

    plot_absolute_error('test_lfcc.png', c, cr)

    assert(check_absolute_error(c, cr, 1e-5).all())

def test_lfsc(dsp_lib):

    m = 32
    n = 512
    x = read_audio('audio/audio.wav', int(4e5), n, True)
    x = x.astype(ctypes.c_float)

    S = np.abs(librosa.core.stft(y=x, n_fft=n-1, center=False))**2
    S = S[:,0]
    fb = utils.trifb_lin(int(n/2), m)
    cr = np.log(np.dot(fb, S))

    c = dsp_lib.lfsc(S, m)

    plot_absolute_error('test_lfcc.png', c, cr)

    assert(check_absolute_error(c, cr, 4e-7).all())