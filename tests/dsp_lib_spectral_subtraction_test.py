import ctypes
import pytest
import scipy
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from utils import read_audio, plot_absolute_error, check_absolute_error
import librosa
import librosa.display
import os

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def spectral_subraction(noise, signal, nfft, k, alpha, beta, onset, threshold):

    y = []
    noise_mean = np.zeros(nfft)

    # mean noise calculation
    for noise_seg in noise.T:
        noise_mean = noise_seg * (1.0 - k) + noise_mean * k

    # signal processing
    count = 0
    for signal_seg in signal.T:
        # VAD
        snr = 10.0 * np.log10(signal_seg / noise_mean)
        snr[snr < 0] = 0.0

        snr_mean = np.average(snr)
        if snr_mean < threshold:
            count = count + 1
        else:
            count = 0

        # subtraction
        if count > onset:
            noise_mean = signal_seg * (1.0 - k) + noise_mean * k
            y.append(noise_mean * beta)
        else:
            s = signal_seg - noise_mean * alpha
            idx = s < noise_mean * beta
            s[idx] = noise_mean[idx] * beta
            y.append(s)

    return np.matrix(y)

def test_subtraction(dsp_lib):

    n = 512
    m_noise = 4096
    m_signal = 2048
    alpha = 1.0
    beta = 0.03
    k = 0.99
    threshold = 2.0
    onset = 3

    noise = read_audio('audio/audio.wav', int(4e5), m_noise * n, False)
    noise = scipy.signal.decimate(noise, q=4)
    noise = noise.astype(ctypes.c_float)

    signal = read_audio('audio/audio.wav', int(87e5), m_signal * n, False)
    signal = scipy.signal.decimate(signal, q=4)
    signal = signal.astype(ctypes.c_float)

    S_noise = librosa.core.stft(noise, n_fft=n-1, hop_length=int(n/2), win_length=None, window='hann', center=False, dtype=ctypes.c_float)
    S_noise = np.abs(S_noise)
    S_noise_in = np.ravel(S_noise.T)

    S_signal = librosa.core.stft(signal, n_fft=n-1, hop_length=int(n/2), win_length=None, window='hann', center=False, dtype=ctypes.c_float)
    S_signal = np.abs(S_signal)
    S_signal_in = np.ravel(S_signal.T)

    yr = spectral_subraction(noise=S_noise, signal=S_signal, nfft=int(n/2), k=k, alpha=alpha, beta=beta, onset=onset, threshold=threshold)
    yr = librosa.amplitude_to_db(yr.T)

    y = dsp_lib.spectral_subtraction(noise=S_noise_in, signal=S_signal_in, nfft=int(n/2), k=k, alpha=alpha, beta=beta, onset=onset, threshold=threshold)
    y = librosa.amplitude_to_db(y.reshape(1023, int(n/2)).T)

    plt.subplot(5, 1, 1)
    plt.contourf(librosa.amplitude_to_db(S_noise), cmap=cm.jet)
    plt.legend(['noise'])

    ax2 = plt.subplot(5, 1, 2)
    plt.contourf(librosa.amplitude_to_db(S_signal), cmap=cm.jet)
    plt.legend(['signal'])

    ax3 = plt.subplot(5, 1, 3, sharex=ax2)
    plt.contourf(y, cmap=cm.jet)
    plt.legend(['dsplib'])

    plt.subplot(5, 1, 4, sharex=ax2)
    plt.contourf(yr, cmap=cm.jet)
    plt.legend(['python'])

    plt.subplot(5, 1, 5, sharex=ax3)
    plt.contourf(y - yr, cmap=cm.jet)
    plt.legend(['abs error'])

    assert(check_absolute_error(y, yr, 26).all())

    if not os.path.exists('images'):
        os.mkdir('images')

    plt.savefig(os.path.join('images', 'spectral_subtraction.png'))
    plt.close()