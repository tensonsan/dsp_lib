
import ctypes
import pytest
from dsp_lib import DSPLib
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from utils import plot_absolute_error, check_absolute_error

@pytest.fixture
def dsp_lib(scope="module"):
    dsp_lib = DSPLib()
    return dsp_lib

def test_hamming(dsp_lib):

    n = 512
    w = dsp_lib.window_hamming(n)
    wr = signal.hamming(M=n, sym=False)

    plot_absolute_error("test_hamming", w, wr)

    assert(check_absolute_error(w, wr, 3e-7).all())

def test_hann(dsp_lib):

    n = 512
    w = dsp_lib.window_hann(n)
    wr = signal.hann(M=n, sym=False)

    plot_absolute_error("test_hann", w, wr)

    assert(check_absolute_error(w, wr, 3e-7).all())

def test_bartlett(dsp_lib):

    n = 512
    w = dsp_lib.window_bartlett(n)
    wr = signal.bartlett(M=n, sym=False)

    plot_absolute_error("test_bartlett", w, wr)

    assert(check_absolute_error(w, wr, 1e-8).all())